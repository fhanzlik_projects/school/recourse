{
	pkgs,
	channel,
	version
}: let
	toolchain = pkgs.rust-bin.${channel}.${version}.default.override {
		extensions = [ "rust-src" "rustfmt" "rust-analyzer" ];
		targets = [
			(pkgs.rust.toRustTarget pkgs.stdenv.buildPlatform)
			(pkgs.rust.toRustTarget pkgs.stdenv.hostPlatform)
			"wasm32-unknown-unknown"
		];
	};

	project = pkgs.rustBuilder.makePackageSet {
		rustToolchain = toolchain;
		packageFun = import ../Cargo.nix;
	};

	platform = pkgs.makeRustPlatform {
		cargo = toolchain;
		rustc = toolchain;
	};
in {
	inherit toolchain project platform;
}
