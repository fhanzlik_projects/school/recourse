{
	pkgs
}: let
	port_postgres = 5432;
	port_minio_api = 9000;
	port_minio_console = 9001;
	port_ory_kratos_public = 4433;
	port_ory_kratos_admin = 4434;
in {
	inherit pkgs;
	config = { pkgs, ... }: {
		project.name = "recourse";

		docker-compose.raw = {
			volumes = {
				"postgres-data" = {};
				"minio-data" = {};
			};
		};
		services = {
			# back-end.service = {
			# 	useHostStore = true;
			# 	command = ["${packages.back-end}/bin/main"];
			# 	# host:container
			# 	ports = [
			# 		"8000:8000"
			# 		"80:80"
			# 	];
			# 	environment = {
			# 		WEB_ROOT = "${pkgs.nix.doc}/share/doc/nix/manual";
			# 	};
			# };
			minio.service = {
				image = "docker.io/minio/minio:latest";
				ports = [
					"${toString port_minio_api}:9000"
					"${toString port_minio_console}:9001"
				];
				volumes = ["minio-data:/data"];
				command = ''server /data --console-address=":${toString port_minio_console}"'';
				environment = {
					MINIO_ROOT_USER     = "development";
					MINIO_ROOT_PASSWORD = "development";
				};
			};
			postgres.service = {
				image = "docker.io/postgres:14.4-alpine";
				# host:container
				ports = [
					"${toString port_postgres}:5432"
				];
				volumes = [
					{ type = "volume"; source = "postgres-data"; target = "/var/lib/postgresql/data"; }
					{ type = "bind"; source = "./docker/postgres/init_scripts"; target = "/docker-entrypoint-initdb.d"; }
				];
				environment = {
					POSTGRES_DB       = "main";
					POSTGRES_USER     = "development";
					POSTGRES_PASSWORD = "development";
				};
			};
		} // (let
			ory_kratos_common.service = {
				image = "docker.io/oryd/kratos:v0.11.0";
				volumes = [
					{ type = "bind"; source = "./docker/ory_kratos"; target = "/etc/ory_kratos"; }
				];
				environment = {
					DSN = "postgres://ory_kratos:development@postgres/ory_kratos?sslmode=disable&max_conns=20&max_idle_conns=4";
					# I don't think this needs to be secure, but I generated something hopefully hard enough to guess / brute force just in case.
					SECRETS_DEFAULT = "recourse-development-yZkyxKiYWjJc9ybn";
				};
			};
		in {
			ory_kratos-migrate = pkgs.lib.recursiveUpdate ory_kratos_common {
				service = {
					restart = "on-failure";
					command = ''-c /etc/ory_kratos/kratos.yml migrate sql -e --yes'';
				};
			};
			ory_kratos = pkgs.lib.recursiveUpdate ory_kratos_common {
				service = {
					depends_on = ["ory_kratos-migrate"];
					command = ''serve --config /etc/ory_kratos/kratos.yml  --dev --watch-courier'';

					# host:container
					ports = [
						"${toString port_ory_kratos_public}:4433"
						"${toString port_ory_kratos_admin}:4434"
					];
				};
			};
		});
	};
}
