{
	description = "recourse";

	nixConfig = {
		extra-experimental-features = "nix-command flakes";
		extra-trusted-substituters = [
			"https://frantisek-hanzlik-recourse.cachix.org"
		];
		extra-trusted-public-keys = [
			"frantisek-hanzlik-recourse.cachix.org-1:RzKgDGshvUT8J5GtNMg6A/F8Y4iPNDLNnhQ2YRqwmlk="
		];
	};

	inputs = {
		flake-compat = { url = "github:edolstra/flake-compat"; flake = false; };
		nixpkgs      = { url = "github:nixos/nixpkgs/nixos-unstable";         };
		flake-utils  = { url = "github:numtide/flake-utils";                  };
		cargo2nix    = { url = "github:cargo2nix/cargo2nix";  inputs.nixpkgs.follows = "nixpkgs"; };
		rust-overlay = { url = "github:oxalica/rust-overlay"; inputs = { nixpkgs.follows = "nixpkgs"; flake-utils.follows = "flake-utils"; }; };
	};

	outputs = { self, nixpkgs, cargo2nix, flake-utils, rust-overlay, ... }:
		flake-utils.lib.eachDefaultSystem (system:
			let
				pkgs = import nixpkgs {
					inherit system;
					overlays = [
						cargo2nix.overlays.default
						rust-overlay.overlays.default
						(final: prev: {
							diesel-cli = final.callPackage ./nix/packages/diesel-cli.nix {};
						})
					];
				};

				node_pkg = pkgs.nodejs-18_x.override { enableNpm = false; };

				rust = import ./nix/rust.nix {
					inherit pkgs;
					channel = "nightly";
					version = "2023-02-26";
				};
			in rec {
				devShells = {
					default = pkgs.mkShell {
						nativeBuildInputs = with pkgs; [
							(diesel-cli.override { rustPlatform = rust.platform; sqliteSupport = false; mysqlSupport = false; postgresqlSupport = true; })
							binaryen
							cargo-expand
							cargo2nix.outputs.packages.${system}.cargo2nix
							openssl
							pkg-config
							pkgs.arion
							postgresql.lib
							rust.toolchain
							wasm-bindgen-cli
							wasm-pack


							(yarn.override { nodejs = node_pkg; }) # by default yarn uses the latest version of nodejs, so we override it to the correct version here
							node_pkg
						];
					};
				};

				packages = {
					cms = (pkgs.callPackage ./yarn-project.nix {
						nodejs = node_pkg;
					} {
						src = ./.;
					}).overrideAttrs (oldAttrs: {
						buildPhase = ''
							runHook preBuild

							pushd packages/cms
								yarn build
							popd

							runHook postBuild
						'';

						postInstall = ''
							cp ./scripts/* $out/bin/
						'';
					});
					back-end = (rust.project.workspace.recourse_back_end {}).bin;
					dev_cluster = pkgs.writeShellApplication {
						name = "recourse-dev_cluster";
						runtimeInputs = with pkgs; [ nomad ];
						text = ''
							sudo nomad agent -dev -bind 0.0.0.0 -log-level INFO
						'';
					};
				};

				apps = let inherit (flake-utils.lib) mkApp; in {
					cms = mkApp { drv = packages.cms; exePath = "/bin/start_cms"; };
					back-end = mkApp { drv = packages.back-end; exePath = "/bin/recourse_back_end"; };
					dev_cluster = mkApp { drv = packages.dev_cluster; exePath = "/bin/recourse-dev_cluster"; };
				};

				arion = import ./nix/arion.nix { inherit pkgs; };
			}
		);
}
