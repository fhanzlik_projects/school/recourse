//----------//
// features //
//----------//
#![feature(try_trait_v2)]
#![feature(never_type)]
//-------//
// lints //
//-------//
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this lint is not very useful and sometimes demands to make `pub` things that can't be.
#![allow(clippy::redundant_pub_crate)]
// actix requires all request handlers to be `async`
#![allow(clippy::unused_async)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

//---------//
// modules //
//---------//

mod api_result;
pub mod models;
pub mod serialization;
pub mod service;

pub use api_result::{ApiError, ApiResult, IntoApiResult};
