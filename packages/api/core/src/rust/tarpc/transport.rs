use {
	bytes::{Bytes, BytesMut},
	educe::Educe,
	serde::{Deserialize, Serialize},
	std::{
		fmt::{Debug, Display},
		marker::PhantomData,
		ops::Deref,
		pin::Pin,
	},
	tarpc::tokio_serde::{Deserializer, Serializer},
};

pub struct Error(postcard::Error);
impl Debug for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		Debug::fmt(&self.0, f)
	}
}
impl Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		Display::fmt(&self.0, f)
	}
}
impl From<postcard::Error> for Error {
	fn from(e: postcard::Error) -> Self {
		Self(e)
	}
}
impl Deref for Error {
	type Target = postcard::Error;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
impl std::error::Error for Error {
	fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
		std::error::Error::source(&self.0)
	}

	fn backtrace(&self) -> Option<&std::backtrace::Backtrace> {
		std::error::Error::backtrace(&self.0)
	}
}

// this impl is required by tarpc for whatever reason.
// it is based on the impl in serde_json: https://docs.rs/serde_json/1.0.82/src/serde_json/error.rs.html#132-173
impl From<Error> for std::io::Error {
	fn from(e: Error) -> Self {
		use {
			postcard::Error::{
				DeserializeBadBool, DeserializeBadChar, DeserializeBadEncoding, DeserializeBadEnum,
				DeserializeBadOption, DeserializeBadUtf8, DeserializeBadVarint,
				DeserializeUnexpectedEnd, NotYetImplemented, SerdeDeCustom, SerdeSerCustom,
				SerializeBufferFull, SerializeSeqLengthUnknown, WontImplement,
			},
			std::io::ErrorKind,
		};

		Self::new(
			match *e {
				WontImplement | NotYetImplemented => unreachable!(),
				SerializeBufferFull => ErrorKind::StorageFull,
				SerializeSeqLengthUnknown
				| DeserializeUnexpectedEnd
				| DeserializeBadVarint
				| DeserializeBadBool
				| DeserializeBadChar
				| DeserializeBadUtf8
				| DeserializeBadOption
				| DeserializeBadEnum
				| DeserializeBadEncoding
				| SerdeSerCustom
				| SerdeDeCustom => ErrorKind::InvalidData,
				_ => ErrorKind::Other,
			},
			e,
		)
	}
}

#[derive(Educe)]
#[educe(Debug, Default)]
pub struct Postcard<Item, SinkItem> {
	#[educe(Debug(ignore), Default(expression = "PhantomData"))]
	ghost: PhantomData<(Item, SinkItem)>,
}

impl<Item, SinkItem> Deserializer<Item> for Postcard<Item, SinkItem>
where
	for<'a> Item: Deserialize<'a>,
{
	type Error = Error;

	fn deserialize(self: Pin<&mut Self>, src: &BytesMut) -> Result<Item, Self::Error> {
		Ok(postcard::from_bytes(&src[..])?)
	}
}

impl<Item, SinkItem> Serializer<SinkItem> for Postcard<Item, SinkItem>
where
	SinkItem: Serialize,
{
	type Error = Error;

	fn serialize(self: Pin<&mut Self>, item: &SinkItem) -> Result<Bytes, Self::Error> {
		Ok(postcard::to_stdvec(item).map(Into::into)?)
	}
}
