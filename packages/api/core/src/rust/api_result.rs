use serde::{Deserialize, Serialize};

pub type ApiResult<T> = Result<T, ApiError>;

#[derive(thiserror::Error, Debug, Serialize, Deserialize)]
pub enum ApiError {
	#[error("internal server error")]
	InternalServerError,
	#[error("malformed request")]
	MalformedRequest,
}

#[allow(clippy::module_name_repetitions)]
pub trait IntoApiResult {
	type T;
	fn into_api_result(self) -> Result<Self::T, ApiError>;
}

impl<T, E> IntoApiResult for Result<T, E>
where
	E: Into<ApiError>,
{
	type T = T;

	fn into_api_result(self) -> Result<Self::T, ApiError> {
		self.map_err(Into::into)
	}
}
