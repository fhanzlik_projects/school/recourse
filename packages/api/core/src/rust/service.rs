// triggered by wasm-bindgen
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::use_self)]

use {
	crate::{models, ApiResult},
	serde::{Deserialize, Serialize},
	std::{convert::Into, fmt::Debug, ops::Deref},
	uuid::Uuid,
	wasm_bindgen::{prelude::wasm_bindgen, JsError},
};

// triggered by wasm-bindgen
#[allow(clippy::unsafe_derive_deserialize)]
// TODO: move this somewhere appropriate
#[wasm_bindgen]
#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct WasmUuid(Uuid);

impl Debug for WasmUuid {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.0.fmt(f)
	}
}

impl WasmUuid {
	#[must_use]
	pub const fn new(inner: Uuid) -> Self {
		Self(inner)
	}
}

#[wasm_bindgen]
impl WasmUuid {
	pub fn from_slice(value: &[u8]) -> Result<WasmUuid, JsError> {
		Self::try_from(value).map_err(Into::into)
	}

	#[must_use]
	pub fn to_slice(&self) -> Box<[u8]> {
		Box::new(*self.0.as_bytes())
	}
}

impl TryFrom<&[u8]> for WasmUuid {
	type Error = uuid::Error;

	fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
		Ok(Self::new(Uuid::from_slice(value)?))
	}
}

impl Deref for WasmUuid {
	type Target = Uuid;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

#[tarpc::service]
pub trait World {
	async fn app_course_list() -> ApiResult<Vec<models::AppCourseListCourse>>;
	async fn app_lesson_detail(
		id: WasmUuid,
		hack_user: Option<WasmUuid>,
	) -> ApiResult<models::AppLessonDetail>;
	async fn app_lesson_set_completed(
		lesson: WasmUuid,
		value: bool,
		hack_user: WasmUuid,
	) -> ApiResult<()>;

	async fn cms_course_list() -> ApiResult<Vec<models::CmsCourseListCourse>>;
	async fn cms_course_get(id: WasmUuid) -> ApiResult<models::CmsCourseGet>;
	async fn cms_course_update(course: models::CmsCourseUpdate) -> ApiResult<()>;

	async fn cms_lesson_list() -> ApiResult<Vec<models::CmsLessonListLesson>>;
	async fn cms_lesson_get(id: WasmUuid) -> ApiResult<models::CmsLessonGet>;
	async fn cms_lesson_update(course: models::CmsLessonUpdate) -> ApiResult<()>;

	async fn files_request_upload(id: WasmUuid, mime_type: String) -> ApiResult<String>;
}
