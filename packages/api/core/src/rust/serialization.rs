use {
	postcard::to_stdvec,
	serde::{Deserialize, Serialize},
	std::{result::Result, vec::Vec},
	thiserror::Error,
};

#[derive(Error, Debug)]
pub enum Error {
	#[error("error from the postcard library")]
	Postcard(#[from] postcard::Error),
}

pub trait SerializePostcard {
	fn serialize(&self) -> Result<Vec<u8>, Error>;
}

pub trait DeserializePostcard<'a> {
	fn take_from(bytes: &'a [u8]) -> Result<(Self, &'a [u8]), Error>
	where
		Self: Sized;
}

impl<Type> SerializePostcard for Type
where
	Type: Serialize,
{
	fn serialize(&self) -> Result<Vec<u8>, Error>
	where
		Self: Serialize,
	{
		Ok(to_stdvec(self)?)
	}
}

impl<'a, T> DeserializePostcard<'a> for T
where
	T: Deserialize<'a>,
{
	fn take_from(bytes: &'a [u8]) -> Result<(Self, &'a [u8]), Error>
	where
		Self: Deserialize<'a>,
	{
		Ok(postcard::take_from_bytes(bytes)?)
	}
}
