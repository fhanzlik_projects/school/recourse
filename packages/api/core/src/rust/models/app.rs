// wasm bindgen loves to trigger these
#![allow(clippy::use_self)]
#![allow(clippy::unsafe_derive_deserialize)]
#![allow(clippy::missing_const_for_fn)]
#![allow(clippy::extra_unused_type_parameters)]
#![allow(clippy::module_name_repetitions)]

use {
	crate::service::WasmUuid,
	serde::{Deserialize, Serialize},
};

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct AppCourseListCourse {
	pub id: WasmUuid,
	pub name: String,
	pub description: String,
	pub cover_image: String,
	pub first_lesson: WasmUuid,
	pub lessons: Vec<AppCourseListCourseLesson>,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct AppCourseListCourseLesson {
	pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct AppLessonDetail {
	pub lesson: AppLessonDetailLesson,
	pub course: AppLessonDetailCourse,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct AppLessonDetailLesson {
	pub id: WasmUuid,
	pub name: String,
	pub content: String,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct AppLessonDetailCourse {
	pub id: WasmUuid,
	pub name: String,
	pub lessons: Vec<AppLessonDetailCourseLesson>,
}

#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct AppLessonDetailCourseLesson {
	pub id: WasmUuid,
	pub name: String,
	pub completed: bool,
}
