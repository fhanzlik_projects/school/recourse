// wasm bindgen loves to trigger these
#![allow(clippy::use_self)]
#![allow(clippy::unsafe_derive_deserialize)]
#![allow(clippy::missing_const_for_fn)]
#![allow(clippy::extra_unused_type_parameters)]
#![allow(clippy::module_name_repetitions)]

use {
	crate::service::WasmUuid,
	serde::{Deserialize, Serialize},
	wasm_bindgen::prelude::wasm_bindgen,
};

/////////////
// courses //
/////////////

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct CmsCourseListCourse {
	pub id: WasmUuid,
	pub name: String,
	pub description: String,
	pub cover_image: String,
}

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct CmsCourseGet {
	pub id: WasmUuid,
	pub name: String,
	pub description: String,
	pub cover_image: String,
}

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct CmsCourseUpdate {
	pub id: WasmUuid,
	pub name: Option<String>,
	pub description: Option<String>,
	pub cover_image: Option<String>,
}

/////////////
// lessons //
/////////////

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct CmsLessonListLesson {
	pub id: WasmUuid,
	pub name: String,
	pub course_name: String,
}

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct CmsLessonGet {
	pub id: WasmUuid,
	pub name: String,
	pub content: String,
}

#[wasm_bindgen(getter_with_clone)]
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
pub struct CmsLessonUpdate {
	pub id: WasmUuid,
	pub name: Option<String>,
	pub content: Option<String>,
}

#[wasm_bindgen]
impl CmsCourseUpdate {
	#[must_use]
	pub fn new(
		id: WasmUuid,
		name: Option<String>,
		description: Option<String>,
		cover_image: Option<String>,
	) -> Self {
		Self {
			id,
			name,
			description,
			cover_image,
		}
	}
}

#[wasm_bindgen]
impl CmsLessonUpdate {
	#[must_use]
	pub fn new(id: WasmUuid, name: Option<String>, content: Option<String>) -> Self {
		Self { id, name, content }
	}
}
