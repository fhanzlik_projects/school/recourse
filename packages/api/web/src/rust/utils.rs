use {
	recourse_commons::tracing_formats,
	tracing_subscriber::{
		fmt::format::Pretty, prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt,
		EnvFilter,
	},
	tracing_web::{performance_layer, MakeConsoleWriter},
};

// when the `wee_alloc` feature is enabled, use `wee_alloc` as the global allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub fn init() {
	// when the `console_error_panic_hook` feature is enabled, we can call the
	// `set_panic_hook` function at least once during initialization, and then
	// we will get better error messages if our code ever panics.
	//
	// for more details see
	// https://github.com/rustwasm/console_error_panic_hook#readme
	#[cfg(feature = "console_error_panic_hook")]
	console_error_panic_hook::set_once();

	// ansi is only partially supported across browsers
	let fmt_layer = tracing_subscriber::fmt::layer()
		.event_format(tracing_formats::Web::default())
		.with_ansi(false)
		.with_writer(MakeConsoleWriter);
	let perf_layer = performance_layer().with_details_from_fields(Pretty::default());

	tracing_subscriber::registry()
		.with(EnvFilter::try_new("warn,recourse_api[]=info").unwrap())
		.with(fmt_layer)
		.with(perf_layer)
		.init();
}
