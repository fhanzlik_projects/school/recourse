//----------//
// features //
//----------//
#![feature(once_cell)]
#![feature(try_blocks)]
//-------//
// lints //
//-------//
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// wasm-bindgen generates unsafe blocks that are tedious to allow-annotate by hand
#![allow(unsafe_code)]
// wasm-bindgen triggers this everywhere
#![allow(clippy::use_self)]
// this lint is not very useful and sometimes demands to make `pub` things that can't be.
#![allow(clippy::redundant_pub_crate)]
// actix requires all request handlers to be `async`
#![allow(clippy::unused_async)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

use {
	js_sys::Array,
	recourse_api::{
		models,
		service::{WasmUuid, WorldClient},
	},
	recourse_commons::error_formatting::FormattedError,
	std::future::Future,
	tarpc::{
		client::{self, NewClient},
		context,
	},
	tarpc_transport_postcard::TransportPostcard,
	tarpc_transport_websocket_web_sys::TransportWebsocket,
	tracing::info,
	wasm_bindgen::{prelude::wasm_bindgen, JsCast, JsError, JsValue},
	wasm_bindgen_futures::{future_to_promise, spawn_local},
	ws_stream_wasm::WsMeta,
};

mod utils;

#[wasm_bindgen]
extern "C" {
	fn alert(s: &str);
}

#[wasm_bindgen(start)]
pub fn initialize() {
	utils::init();
}

#[wasm_bindgen(typescript_custom_section)]
const TS_APPEND_CONTENT: &'static str = r#"
export type AppLessonDetail = {
	lesson: {
		id: string;
		name: string;
		content: string;
	};
	course: {
		id: string;
		name: string;
		lessons: {
			id: string;
			name: string;
			completed: boolean;
		}[];
	};
};

export type AppCourseListCourse = {
	id: string;
	name: string;
	description: string;
	cover_image: string;
	first_lesson: string;
	lessons: {
		name: string;
	}[];
};
"#;

#[wasm_bindgen]
extern "C" {
	#[wasm_bindgen(typescript_type = "Promise<AppCourseListCourse[]>")]
	pub type PromiseAppCourseList;

	#[wasm_bindgen(typescript_type = "Promise<AppLessonDetail>")]
	pub type PromiseAppLessonDetail;

	#[wasm_bindgen(typescript_type = "Promise<CmsCourseListCourse[]>")]
	pub type PromiseCmsCourseList;
	#[wasm_bindgen(typescript_type = "Promise<CmsCourseGet>")]
	pub type PromiseCmsCourseGet;

	#[wasm_bindgen(typescript_type = "Promise<CmsLessonListLesson[]>")]
	pub type PromiseCmsLessonList;
	#[wasm_bindgen(typescript_type = "Promise<CmsLessonGet>")]
	pub type PromiseCmsLessonGet;

	#[wasm_bindgen(typescript_type = "Promise<void>")]
	pub type PromiseVoid;

	#[wasm_bindgen(typescript_type = "Promise<string>")]
	pub type PromiseString;
}

#[wasm_bindgen]
#[derive(Clone)]
pub struct Client {
	inner: WorldClient,
}

#[wasm_bindgen]
#[allow(clippy::missing_const_for_fn)]
#[allow(clippy::future_not_send)]
// wasm-bindgen emits something that doesn't compile if we use Self in the return position
#[allow(clippy::use_self)]
impl Client {
	pub async fn connect() -> Result<Client, JsError> {
		let (_socket_meta, socket) =
			WsMeta::connect("ws://localhost:8080/api_socket", None).await?;
		info!("connection to api socket successfully established.");

		let transport = TransportPostcard::new(TransportWebsocket::new(socket));
		let NewClient { client, dispatch } =
			recourse_api::service::WorldClient::new(client::Config::default(), transport);

		spawn_local(async {
			#[allow(clippy::match_same_arms)]
			match dispatch.await {
				Ok(_) => {},
				Err(client::ChannelError::Ready(tarpc_transport_postcard::Error::Next(
					tarpc_transport_websocket_web_sys::Error::Internal(
						ws_stream_wasm::WsErr::ConnectionNotOpen,
					),
				))) => {
					// this normally happens when refreshing the page.
					// in case this also happens on other occasions, we should handle it specially
					// so it doesn't clutter the browser console
				},
				Err(e) => {
					panic!("{}", FormattedError::new(e))
				},
			};
		});

		Ok(Self { inner: client })
	}

	// app //

	#[must_use]
	pub fn app_course_list(&self) -> PromiseAppCourseList {
		async_closure(self, move |this| async move {
			Ok(this
				.inner
				.app_course_list(context::current())
				.await??
				.into_iter()
				.map(|course| serde_wasm_bindgen::to_value(&course))
				.collect::<Result<Array, _>>()?
				.into())
		})
	}

	#[must_use]
	pub fn app_lesson_detail(
		&self,
		id: WasmUuid,
		hack_user: Option<WasmUuid>,
	) -> PromiseAppLessonDetail {
		async_closure(self, move |this| async move {
			Ok(serde_wasm_bindgen::to_value(
				&this
					.inner
					.app_lesson_detail(context::current(), id, hack_user)
					.await??,
			)?)
		})
	}

	#[must_use]
	pub fn app_lesson_set_completed(
		&self,
		lesson: WasmUuid,
		completed: bool,
		hack_user: WasmUuid,
	) -> PromiseVoid {
		async_closure(self, move |this| async move {
			this.inner
				.app_lesson_set_completed(context::current(), lesson, completed, hack_user)
				.await??;

			Ok(JsValue::UNDEFINED)
		})
	}

	// cms //

	#[must_use]
	pub fn cms_course_list(&self) -> PromiseCmsCourseList {
		async_closure(self, move |this| async move {
			Ok(this
				.inner
				.cms_course_list(context::current())
				.await??
				.into_iter()
				.map(JsValue::from)
				.collect::<Array>()
				.into())
		})
	}

	#[must_use]
	pub fn cms_course_get(&self, id: WasmUuid) -> PromiseCmsCourseGet {
		async_closure(self, move |this| async move {
			Ok(this
				.inner
				.cms_course_get(context::current(), id)
				.await??
				.into())
		})
	}

	#[must_use]
	pub fn cms_course_update(&self, course: models::CmsCourseUpdate) -> PromiseVoid {
		async_closure(self, move |this| async move {
			this.inner
				.cms_course_update(context::current(), course)
				.await??;

			Ok(JsValue::UNDEFINED)
		})
	}

	#[must_use]
	pub fn cms_lesson_list(&self) -> PromiseCmsLessonList {
		async_closure(self, move |this| async move {
			Ok(this
				.inner
				.cms_lesson_list(context::current())
				.await??
				.into_iter()
				.map(JsValue::from)
				.collect::<Array>()
				.into())
		})
	}

	#[must_use]
	pub fn cms_lesson_get(&self, id: WasmUuid) -> PromiseCmsLessonGet {
		async_closure(self, move |this| async move {
			Ok(this
				.inner
				.cms_lesson_get(context::current(), id)
				.await??
				.into())
		})
	}

	#[must_use]
	pub fn cms_lesson_update(&self, lesson: models::CmsLessonUpdate) -> PromiseVoid {
		async_closure(self, move |this| async move {
			this.inner
				.cms_lesson_update(context::current(), lesson)
				.await??;

			Ok(JsValue::UNDEFINED)
		})
	}

	#[must_use]
	pub fn files_request_upload(&self, id: WasmUuid, mime_type: String) -> PromiseString {
		async_closure(self, move |this| async move {
			Ok(this
				.inner
				.files_request_upload(context::current(), id, mime_type)
				.await??
				.into())
		})
	}
}

fn async_closure<S: Clone + 'static, R: JsCast, F: Future<Output = Result<JsValue, JsError>>>(
	this: &S,
	closure: impl FnOnce(S) -> F + 'static,
) -> R {
	let this = this.clone();
	future_to_promise(async move { closure(this).await.map_err(JsValue::from) }).unchecked_into()
}
