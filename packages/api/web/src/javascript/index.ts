import { Uuid } from "@recourse/commons";
export type { AppLessonDetail } from "recourse_api";

export const initializeApi = () => {
	const api = import("recourse_api");
	const client = api.then((a) => a.Client.connect());

	return {
		app: {
			course: {
				list: async () => await (await client).app_course_list(),
			},
			lesson: {
				detail: async (id: Uuid, hack_user: Uuid | undefined) =>
					await (
						await client
					).app_lesson_detail(
						(await api).WasmUuid.from_slice(id),
						hack_user && (await api).WasmUuid.from_slice(hack_user),
					),
				set_completed: async (
					lesson: Uuid,
					hack_user: Uuid,
					completed: boolean,
				) =>
					await (
						await client
					).app_lesson_set_completed(
						(await api).WasmUuid.from_slice(lesson),
						completed,
						(await api).WasmUuid.from_slice(hack_user),
					),
			},
		},
		cms: {
			course: {
				list: async () => await (await client).cms_course_list(),
				get: async (id: Uuid) =>
					await (
						await client
					).cms_course_get((await api).WasmUuid.from_slice(id)),
				update: async ({
					id,
					name,
					coverImage,
				}: {
					id: Uuid;
					name: string | undefined;
					coverImage: string | undefined;
				}) =>
					await (
						await client
					).cms_course_update(
						(
							await api
						).CmsCourseUpdate.new(
							(await api).WasmUuid.from_slice(id),
							name,
							coverImage,
						),
					),
			},
			lesson: {
				list: async () => await (await client).cms_lesson_list(),
				get: async (id: Uuid) =>
					await (
						await client
					).cms_lesson_get((await api).WasmUuid.from_slice(id)),
				update: async ({
					id,
					name,
					content,
				}: {
					id: Uuid;
					name: string | undefined;
					content: string | undefined;
				}) =>
					await (
						await client
					).cms_lesson_update(
						(
							await api
						).CmsLessonUpdate.new(
							(await api).WasmUuid.from_slice(id),
							name,
							content,
						),
					),
			},
		},
		files: {
			request_upload: async ({
				id,
				mimeType,
			}: {
				id: Uuid;
				mimeType: string;
			}) =>
				await (
					await client
				).files_request_upload(
					(await api).WasmUuid.from_slice(id),
					mimeType,
				),
		},
	};
};

export type ApiClient = Awaited<ReturnType<typeof initializeApi>>;
