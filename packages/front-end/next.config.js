const path = require(`path`);

/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	webpack: (config, { dev, isServer }) => {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
		Object.assign(config.experiments, {
			topLevelAwait: true,
			syncWebAssembly: true,
		});

		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
		config.module.rules.push(
			{
				test: /\.svg$/i,
				resourceQuery: { not: [/url/] }, // exclude react component if *.svg?url
				issuer: /\.[jt]sx?$/,
				use: [
					{
						loader: `@svgr/webpack`,
						options: {
							typescript: true,
						},
					},
				],
			},
			{
				test: /\.svg$/i,
				resourceQuery: /url/, // *.svg?url
				issuer: /\.[jt]sx?$/,
				type: `asset`,
			},
		);

		if (dev && !isServer) {
			const originalEntry = config.entry;
			config.entry = async () => {
				const wdrPath = path.resolve(__dirname, `./src/wdyr.ts`);
				const entries = await originalEntry();

				if (
					entries[`main.js`] &&
					!entries[`main.js`].includes(wdrPath)
				) {
					entries[`main.js`].push(wdrPath);
				}
				return entries;
			};
		}

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return config;
	},
	experimental: {
		urlImports: [`https://flackr.github.io`],
	},
	compiler: {
		emotion: true,
	},
};

module.exports = nextConfig;
