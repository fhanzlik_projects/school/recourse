import "@emotion/react";
import { Theme as OurTheme } from "@recourse/commons";

declare module "@emotion/react" {
	export interface Theme extends OurTheme {}
}
