import "@asciidoctor/core";

declare global {
	export const Opal: {
		id: (o: object) => number | undefined;
	};
}
