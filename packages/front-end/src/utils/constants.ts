export const appUrls = {
	signIn: () => `/auth/sign-in`,
	signUp: () => `/auth/sign-up`,
};
