import styled from "@emotion/styled";
import { background, breakpoints, css, easing } from "@recourse/commons";

export const problemListVisibility = ({ invalid }: { invalid: boolean }) =>
	invalid
		? css`
				visibility: visible;
				opacity: 1;
				transform: initial;
		  `
		: css`
				visibility: hidden;
				opacity: 0;
				transform: translateY(-0.25rem);
		  `;

const tooltipShiftBp = breakpoints.xl;

export const ProblemList = styled.ul`
	position: absolute;

	/* needed to keep the tooltip above other inputs, since they are also positioned */
	z-index: 1;

	inset: 0;
	top: 0.5rem;
	bottom: auto;

	padding: 0.5rem 1.25rem;
	border-radius: 1rem;
	list-style: none;
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
	${background(`elevated.0`)}

	@media (min-width: ${tooltipShiftBp}) {
		inset: auto;
		left: 1rem;
		bottom: 0;
		width: 24rem;
	}

	${problemListVisibility({ invalid: false })}
	/*
	 * visibility is not animatable, but needs to be a part of the transition anyway or the element
	 * will be made invisible before the opacity animation finishes, cutting off the animation
	 */
	transition: visibility 0.5s, opacity 0.5s ${easing.outQuart},
		0.5s ${easing.outQuart};
`;

export const InputWrapper = styled.div<{
	invalid: boolean;
}>`
	/* on small devices the anchor uses the wrapper as an anchor, so it needs to be positioned too */
	position: relative;
	display: flex;

	&:hover,
	&:focus-within {
		${ProblemList} {
			${problemListVisibility}
		}
	}
`;

export const FormFields = styled.div`
	display: flex;
	flex-direction: column;

	&:focus-within {
		${InputWrapper}:not(:focus-within) ${ProblemList} {
			transition: visibility 0s, opacity 0s, transform 0s;
			${problemListVisibility({ invalid: false })}
		}
	}
`;
