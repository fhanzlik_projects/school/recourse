import { FrontendApi } from "@ory/client";

export const getUserSession = async (ory: FrontendApi) => {
	try {
		return await ory.toSession();
	} catch {
		// the user doesn't have a session.
		return undefined;
	}
};
