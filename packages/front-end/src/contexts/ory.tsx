import { Configuration, FrontendApi } from "@ory/client";
import { error } from "@recourse/commons";
import { createContext, useContext } from "react";

const oryContext = createContext<FrontendApi | undefined>(undefined);

export const useOryClient = () => {
	return (
		useContext(oryContext) ??
		error(
			`attempted to retrieve an ory client even though its provider did not render at this component depth.`,
		)
	);
};

export const OryClientProvider = (props: { children: JSX.Element }) => {
	const basePath = `http://localhost:4433`;
	const client = new FrontendApi(
		new Configuration({
			basePath,
			baseOptions: {
				withCredentials: true,
			},
		}),
	);

	return (
		<oryContext.Provider value={client}>
			{props.children}
		</oryContext.Provider>
	);
};
