import "../wdyr";

import { Global, ThemeProvider, useTheme } from "@emotion/react";
import { globalStyles, themes } from "@recourse/commons";
import type { AppProps } from "next/app";
import { QueryClient, QueryClientProvider } from "react-query";
import { ApiClientProvider } from "../contexts/api";
import { OryClientProvider } from "../contexts/ory";

const queryClient = new QueryClient();

export default function CustomApp({ Component, pageProps }: AppProps) {
	return (
		<>
			<ThemeProvider theme={themes.dark}>
				<GlobalStyles />
				<QueryClientProvider client={queryClient}>
					<ApiClientProvider>
						<OryClientProvider>
							<Component {...pageProps} />
						</OryClientProvider>
					</ApiClientProvider>
				</QueryClientProvider>
			</ThemeProvider>
		</>
	);
}

const GlobalStyles = () => {
	return <Global styles={globalStyles({ theme: useTheme() })}></Global>;
};
