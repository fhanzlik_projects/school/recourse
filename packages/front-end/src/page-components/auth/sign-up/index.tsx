import styled from "@emotion/styled";
import { RegistrationFlow, UpdateRegistrationFlowBody } from "@ory/client";
import {
	css,
	dbg,
	error,
	runBackgroundAsyncTask,
	text,
} from "@recourse/commons";
import { AxiosError } from "axios";
import { NextPage } from "next";
import { NextRouter, useRouter } from "next/router";
import { darken, lighten, transparentize } from "polished";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import Zod from "zod";
import { useOryClient } from "../../../contexts/ory";
import { appUrls } from "../../../utils/constants";
import { getUserSession } from "../../../utils/ory";
import { screens } from "../../index/screen";
import { Flow } from "../flows/flow";
import login from "../flows/icons/login.jpg";

export const Page: NextPage = () => {
	const ory = useOryClient();
	const [flow, setFlow] = useState<RegistrationFlow>();
	const router = useRouter();

	const {
		return_to: returnTo,
		flow: flowId,
		// Refresh means we want to refresh the session. This is needed, for example, when we want to update the password
		// of a user.
		refresh,
		// AAL = Authorization Assurance Level. This implies that we want to upgrade the AAL, meaning that we want
		// to perform two-factor authentication/verification.
		aal,
	} = router.query;

	useEffect(() => {
		if (!router.isReady || flow) {
			return;
		}
		runBackgroundAsyncTask(async () => {
			const session = await getUserSession(ory);

			if (session !== undefined) {
				console.log(`user logged in, redirectiong to main page`);
				await router.push(`/`);
				return;
			}

			if (flowId) {
				try {
					const { data } = await ory.getRegistrationFlow({
						id: String(flowId),
					});
					setFlow(data);
				} catch (e) {
					await handleFlowError(router, appUrls.signUp(), setFlow)(e);
				}
				return;
			}

			try {
				const { data } = await ory.createBrowserRegistrationFlow({
					returnTo: returnTo ? String(returnTo) : undefined,
				});
				setFlow(dbg(data));
			} catch (e) {
				await handleFlowError(router, appUrls.signUp(), setFlow)(e);
			}
		});
	}, [flowId, router, router.isReady, aal, refresh, returnTo, flow, ory]);

	const submit = async (values: UpdateRegistrationFlowBody) => {
		if (flow === undefined) {
			console.error(
				`can't submit the sign up form because the sign up flow was not (yet?) initialised!`,
			);
			return;
		}
		// On submission, add the flow ID to the URL but do not navigate. This prevents the user loosing
		// his data when she/he reloads the page.
		await router.replace({ query: { ...router.query, flow: flow.id } });

		try {
			await ory.updateRegistrationFlow({
				flow: String(flow.id),
				updateRegistrationFlowBody: values,
			});

			if (flow.return_to) {
				window.location.href = flow.return_to;
				return;
			}
			await router.push(`/`);
		} catch (e) {
			try {
				await handleFlowError(router, appUrls.signUp(), setFlow)(e);
			} catch (e) {
				const axiosErrorParsed = Zod.object({
					response: Zod.object({
						status: Zod.number(),
						data: Zod.any(),
					}).optional(),
				}).safeParse(e);

				// If the previous handler did not catch the error it's most likely a form validation error
				if (
					axiosErrorParsed.success &&
					axiosErrorParsed.data.response?.status === 400
				) {
					// Yup, it is!
					setFlow(
						axiosErrorParsed.data.response.data as RegistrationFlow,
					);
					return;
				}
				throw e;
			}
		}
	};
	return (
		<Container>
			<Heading>Welcome!</Heading>
			<FormContainer>
				<Flow onSubmit={submit} flow={flow} />
			</FormContainer>
			<LoginLink href={appUrls.signIn()}>
				Already have an account?
			</LoginLink>
		</Container>
	);
};
const handleFlowError =
	<S,>(
		router: NextRouter,
		flowUrl: string,
		resetFlow: Dispatch<SetStateAction<S | undefined>>,
	) =>
	async (err: unknown) => {
		const errAxios = err as AxiosError<{
			error?: { id: string };
			redirect_browser_to?: string;
		}>;
		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
		switch (errAxios.response?.data.error?.id) {
			case `session_aal2_required`:
				// 2FA is enabled and enforced, but user did not perform 2fa yet!
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
				window.location.href =
					errAxios.response.data.redirect_browser_to ??
					error.unexpectedNullish();
				return;
			case `session_already_available`:
				// User is already signed in, let's redirect them home!
				await router.push(flowUrl);
				return;
			case `session_refresh_required`:
				// We need to re-authenticate to perform this action
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
				window.location.href =
					errAxios.response.data.redirect_browser_to ??
					error.unexpectedNullish();
				return;
			case `self_service_flow_return_to_forbidden`:
				// The flow expired, let's request a new one.
				console.error(`The return_to address is not allowed.`);
				resetFlow(undefined);
				await router.push(flowUrl);
				return;
			case `self_service_flow_expired`:
				// The flow expired, let's request a new one.
				console.error(
					`Your interaction expired, please fill out the form again.`,
				);
				resetFlow(undefined);
				await router.push(flowUrl);
				return;
			case `security_csrf_violation`:
				// A CSRF violation occurred. Best to just refresh the flow!
				console.error(
					`A security violation was detected, please fill out the form again.`,
				);
				resetFlow(undefined);
				await router.push(flowUrl);
				return;
			case `security_identity_mismatch`:
				// The requested item was intended for someone else. Let's request a new flow...
				resetFlow(undefined);
				await router.push(flowUrl);
				return;
			case `browser_location_change_required`:
				// Ory Kratos asked us to point the user to this URL.
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
				window.location.href =
					errAxios.response.data.redirect_browser_to ??
					error.unexpectedNullish();
				return;
		}

		switch (errAxios.response?.status) {
			case 410:
				// The flow expired, let's request a new one.
				resetFlow(undefined);
				await router.push(flowUrl);
				return;
		}

		// We are not able to handle the error? Return it.
		throw errAxios;
	};

const Container = styled(screens.Common)`
	padding: 3rem;

	${({
		theme: { colours: { background } } = error(`theme not provided`),
	}) => css`
		background: linear-gradient(
				to right,
				${background.normal} 30%,
				${transparentize(0.3, background.normal)}
			),
			/* trickery to make the gradient appear less banded (my attempts at dithering looked bad) */
				linear-gradient(
					30deg,
					${transparentize(0.9, lighten(1, background.normal))},
					${transparentize(0.9, darken(1, background.normal))}
				),
			linear-gradient(
				60deg,
				${transparentize(0.9, lighten(1, background.normal))},
				${transparentize(0.9, darken(1, background.normal))}
			),
			url(${login.src}) center/cover;
	`}

	justify-content: center;
`;

const Heading = styled.h1`
	${text({ family: `montserrat`, weight: `bold`, colour: `bright`, size: 2 })}

	padding-bottom: 3rem;
`;

const FormContainer = styled.div`
	max-width: 25rem;
`;
const LoginLink = styled.a`
	${text({ weight: `bold` })}
	padding:2rem 5rem;
	display: block;
`;
