import { UiNode } from "@ory/client";
import {
	isUiNodeAnchorAttributes,
	isUiNodeImageAttributes,
	isUiNodeInputAttributes,
	isUiNodeScriptAttributes,
	isUiNodeTextAttributes,
} from "@ory/integrations/ui";

import { NodeAnchor } from "./nodeAnchor";
import { NodeImage } from "./nodeImage";
import { NodeInput } from "./nodeInput";
import { NodeScript } from "./nodeScript";
import { NodeText } from "./nodeText";

type Props<T> = {
	node: UiNode;
	disabled: boolean;
	value: T;
	setValue: (value: T) => void;
	dispatchSubmit: (e: React.MouseEvent) => Promise<void>;
};

export const Node = <T,>(props: Props<T>) => {
	if (isUiNodeImageAttributes(props.node.attributes)) {
		return (
			<NodeImage node={props.node} attributes={props.node.attributes} />
		);
	}

	if (isUiNodeScriptAttributes(props.node.attributes)) {
		return (
			<NodeScript node={props.node} attributes={props.node.attributes} />
		);
	}

	if (isUiNodeTextAttributes(props.node.attributes)) {
		return (
			<NodeText node={props.node} attributes={props.node.attributes} />
		);
	}

	if (isUiNodeAnchorAttributes(props.node.attributes)) {
		return (
			<NodeAnchor node={props.node} attributes={props.node.attributes} />
		);
	}

	if (isUiNodeInputAttributes(props.node.attributes)) {
		return (
			<NodeInput
				dispatchSubmit={props.dispatchSubmit}
				value={props.value}
				setValue={props.setValue}
				node={props.node}
				disabled={props.disabled}
				attributes={props.node.attributes}
			/>
		);
	}

	return null;
};
