import { UiNode, UiNodeScriptAttributes } from "@ory/client";
import { HTMLAttributeReferrerPolicy, useEffect } from "react";

type Props = {
	node: UiNode;
	attributes: UiNodeScriptAttributes;
};

export const NodeScript = (props: Props) => {
	useEffect(() => {
		const script = document.createElement(`script`);

		script.async = true;
		script.setAttribute(
			`data-testid`,
			`node/script/${props.attributes.id}`,
		);
		script.src = props.attributes.src;
		script.async = props.attributes.async;
		script.crossOrigin = props.attributes.crossorigin;
		script.integrity = props.attributes.integrity;
		script.referrerPolicy = props.attributes
			.referrerpolicy as HTMLAttributeReferrerPolicy;
		script.type = props.attributes.type;

		document.body.appendChild(script);

		return () => {
			document.body.removeChild(script);
		};
	}, [props.attributes]);

	return null;
};
