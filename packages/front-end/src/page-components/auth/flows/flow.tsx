import styled from "@emotion/styled";
import {
	LoginFlow,
	RecoveryFlow,
	RegistrationFlow,
	SettingsFlow,
	UiNode,
	UiNodeInputAttributes,
	UpdateLoginFlowBody,
	UpdateRecoveryFlowBody,
	UpdateRegistrationFlowBody,
	UpdateSettingsFlowBody,
	UpdateVerificationFlowBody,
	VerificationFlow,
} from "@ory/client";
import { getNodeId, isUiNodeInputAttributes } from "@ory/integrations/ui";
import { error, logAsyncFailure } from "@recourse/commons";
import _ from "lodash";
import { FormEvent, useEffect, useMemo, useState } from "react";
import Zod from "zod";
import {
	InputWrapper,
	ProblemList,
	problemListVisibility,
} from "../../../utils/forms";
import { Messages } from "./messages";
import { Node } from "./node";

export type Values = Partial<
	| UpdateLoginFlowBody
	| UpdateRegistrationFlowBody
	| UpdateRecoveryFlowBody
	| UpdateSettingsFlowBody
	| UpdateVerificationFlowBody
>;

export type Methods =
	| "oidc"
	| "password"
	| "profile"
	| "totp"
	| "webauthn"
	| "link"
	| "lookup_secret";

export type Props<T> = {
	// The flow
	flow?:
		| LoginFlow
		| RegistrationFlow
		| SettingsFlow
		| VerificationFlow
		| RecoveryFlow;
	// Only show certain nodes. We will always render the default nodes for CSRF tokens.
	only?: Methods;
	// Is triggered on submission
	onSubmit: (values: T) => Promise<void>;
	// Do not show the global messages. Useful when rendering them elsewhere.
	hideGlobalMessages?: boolean;
};

const zValue = Zod.union([
	Zod.string(),
	Zod.number(),
	Zod.boolean(),
	Zod.undefined(),
]);
const zValueObject = Zod.object({}).catchall(zValue);

export const Flow = <T extends Values>({ flow, only, onSubmit }: Props<T>) => {
	const [isLoading, setIsLoading] = useState(false);

	const nodesFiltered = useMemo((): UiNode[] => {
		if (!flow) {
			return [];
		}
		return flow.ui.nodes.filter(({ group }) => {
			if (!only) {
				return true;
			}
			return group === `default` || group === only;
		});
	}, [flow, only]);

	const nodesPartitioned = useMemo(
		() =>
			_(nodesFiltered).groupBy((e) =>
				e.type === `input` &&
				(e.attributes as UiNodeInputAttributes).type === `submit`
					? `actions`
					: `fields`,
			),
		[nodesFiltered],
	);

	const [values, setValues] = useState<T>({} as T);

	useEffect(() => {
		const newValues = {} as T & Zod.infer<typeof zValueObject>;

		nodesFiltered.forEach((node) => {
			// This only makes sense for text nodes
			if (isUiNodeInputAttributes(node.attributes)) {
				if (
					node.attributes.type === `button` ||
					node.attributes.type === `submit`
				) {
					// In order to mimic real HTML forms, we need to skip setting the value
					// for buttons as the button value will (in normal HTML forms) only trigger
					// if the user clicks it.
					return;
				}
				newValues[node.attributes.name] = zValue.parse(
					node.attributes.value,
				);
			}
		});

		setValues(newValues);
	}, [nodesFiltered]);
	const handleSubmit = async (
		event: FormEvent<HTMLFormElement> | React.MouseEvent,
	) => {
		// Prevent double submission!
		if (isLoading) {
			return;
		}
		setIsLoading(true);

		const findParentForm = (node: Element): HTMLFormElement =>
			node instanceof HTMLFormElement
				? node
				: findParentForm(
						node.parentElement ?? error(`reached document root`),
				  );
		const form = findParentForm(event.currentTarget);
		const formData = Object.fromEntries(new FormData(form));
		const submitter =
			event.target instanceof HTMLButtonElement &&
			event.target.name === `method`
				? event.target
				: event.nativeEvent instanceof SubmitEvent &&
				  event.nativeEvent.submitter instanceof HTMLButtonElement
				? event.nativeEvent.submitter
				: error(`couldn't detect who submited the form`);

		const method = submitter.value;

		await onSubmit({ ...formData, ...values, method }).finally(() => {
			// We wait for reconciliation and update the state after 50ms
			// Done submitting - update loading status
			setIsLoading(false);
		});
	};

	return (
		<div>
			{flow ? (
				<Form
					action={flow.ui.action}
					method={flow.ui.method}
					onSubmit={(e) => {
						e.stopPropagation();
						e.preventDefault();
						logAsyncFailure(handleSubmit(e));
					}}
				>
					<Fields>
						{nodesPartitioned.get(`fields`).map((node) => {
							const id = getNodeId(node) as Extract<
								keyof T,
								string
							>;
							return (
								<Node
									key={id}
									disabled={isLoading}
									node={node}
									value={values[id]}
									dispatchSubmit={handleSubmit}
									setValue={(value) => {
										setValues((values) => ({
											...values,
											[getNodeId(node)]: value,
										}));
									}}
								/>
							);
						})}
					</Fields>
					<Messages messages={flow.ui.messages} />
					<ButtonRow>
						{nodesPartitioned.get(`actions`).map((node) => {
							const id = getNodeId(node) as Extract<
								keyof T,
								string
							>;
							return (
								<Node
									key={id}
									disabled={isLoading}
									node={node}
									value={values[id]}
									dispatchSubmit={handleSubmit}
									setValue={(value) => {
										setValues((values) => ({
											...values,
											[getNodeId(node)]: value,
										}));
									}}
								/>
							);
						})}
					</ButtonRow>
				</Form>
			) : // No flow was set yet? It's probably still loading...
			//
			// Nodes have only one element? It is probably just the CSRF Token
			// and the filter did not match any elements!
			null}
		</div>
	);
};

const Form = styled.form`
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
`;

const Fields = styled.div`
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
	${InputWrapper}:not(:focus-within)${ProblemList} {
		transition: visibility 0s, opacity 0s, transform 0s;
		${problemListVisibility({ invalid: false })}
	}
`;

const ButtonRow = styled.div`
	padding-top: 2rem;
	display: flex;
	justify-content: flex-end;
`;
