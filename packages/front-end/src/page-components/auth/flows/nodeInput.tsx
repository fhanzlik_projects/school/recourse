import { NodeInputButton } from "./nodeInputButton";
import { NodeInputCheckbox } from "./nodeInputCheckbox";
import { NodeInputDefault } from "./nodeInputDefault";
import { NodeInputHidden } from "./nodeInputHidden";
import { NodeInputSubmit } from "./nodeInputSubmit";
import { UiNode, UiNodeInputAttributes } from "@ory/client";

export type NodeInputProps<T> = {
	node: UiNode;
	attributes: UiNodeInputAttributes;
	value: T;
	disabled: boolean;
	dispatchSubmit: (e: React.MouseEvent) => Promise<void>;
	setValue: (value: T) => void;
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function NodeInput(props: NodeInputProps<any>) {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
	const { attributes } = props;

	// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
	switch (attributes.type) {
		case `hidden`:
			// Render a hidden input field
			return <NodeInputHidden {...props} />;
		case `checkbox`:
			// Render a checkbox. We have one hidden element which is the real value (true/false), and one
			// display element which is the toggle value (true)!
			return <NodeInputCheckbox {...props} />;
		case `button`:
			// Render a button
			return <NodeInputButton {...props} />;
		case `submit`:
			// Render the submit button
			return <NodeInputSubmit {...props} />;
	}

	// Render a generic text input field.
	return <NodeInputDefault {...props} />;
}
