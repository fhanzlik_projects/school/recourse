import styled from "@emotion/styled";
import { UiText } from "@ory/client";
import { text } from "@recourse/commons";
import { BiError } from "react-icons/bi";

export const Message = (props: { message: UiText }) => {
	return (
		<StyledMessage>
			{props.message.type === `error` ? <BiError size={48} /> : `info`}
			<h3>{props.message.text}</h3>
		</StyledMessage>
	);
};

export const Messages = (props: { messages?: UiText[] }) => {
	return (
		<div>
			{props.messages?.map((messages) => (
				<Message key={messages.id} message={messages} />
			))}
		</div>
	);
};

const StyledMessage = styled.div`
	${text({ colour: `error`, weight: `bold` })}
	display: flex;
	gap: 1rem;
	align-items: center;
`;
