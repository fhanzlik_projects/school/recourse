import Zod from "zod";
import { NodeInputProps } from "./nodeInput";

export function NodeInputHidden(props: NodeInputProps<string>) {
	// Render a hidden input field
	return (
		<input
			type={props.attributes.type}
			name={props.attributes.name}
			value={Zod.string().parse(props.attributes.value) || `true`}
		/>
	);
}
