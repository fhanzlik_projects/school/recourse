import styled from "@emotion/styled";
import { getNodeLabel } from "@ory/integrations/ui";
import { text } from "@recourse/commons";
import { NodeInputProps } from "./nodeInput";

export function NodeInputCheckbox(props: NodeInputProps<boolean>) {
	// Render a checkbox.s
	return (
		<Label>
			<input
				type="checkbox"
				name={props.attributes.name}
				defaultChecked={props.attributes.value === true}
				onChange={(e) => props.setValue(e.target.checked)}
				disabled={props.attributes.disabled || props.disabled}
			/>
			{getNodeLabel(props.node)}
			{props.node.messages.find(({ type }) => type === `error`)
				? `error`
				: undefined}
			{props.node.messages.map(({ text }) => text).join(`\n`)}
		</Label>
	);
}
const Label = styled.label`
	${text({ family: `montserrat`, weight: `bold`, colour: `bright` })}
	display:flex;
	padding: 0.5rem 1rem;
	gap: 1rem;
`;
