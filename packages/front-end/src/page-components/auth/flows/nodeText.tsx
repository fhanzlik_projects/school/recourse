import { UiNode, UiNodeTextAttributes, UiText } from "@ory/client";
import styled from "@emotion/styled";

type Props = {
	node: UiNode;
	attributes: UiNodeTextAttributes;
};
export const NodeText = (props: Props) => {
	return (
		<>
			<p>{props.node.meta.label?.text}</p>
			<Content node={props.node} attributes={props.attributes} />
		</>
	);
};

const Content = (props: Props) => {
	switch (props.attributes.text.id) {
		case 1050015: {
			// This text node contains lookup secrets. Let's make them a bit more beautiful!

			const secrets = (
				props.attributes.text.context as { secerts: UiText[] }
			).secerts.map((text: UiText, k: number) => (
				<div key={k} className="col-xs-3">
					{/* Used lookup_secret has ID 1050014 */}
					<code>{text.id === 1050014 ? `Used` : text.text}</code>
				</div>
			));
			return (
				<div className="container-fluid">
					<div className="row">{secrets}</div>
				</div>
			);
		}
	}

	return (
		<ScrollableCodeBox>
			<code>{props.attributes.text.text}</code>
		</ScrollableCodeBox>
	);
};

const ScrollableCodeBox = styled.pre`
	overflow-x: auto;
`;
