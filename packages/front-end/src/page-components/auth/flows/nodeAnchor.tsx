import { UiNode, UiNodeAnchorAttributes } from "@ory/client";

export const NodeAnchor = (props: {
	node: UiNode;
	attributes: UiNodeAnchorAttributes;
}) => {
	return (
		<button
			onClick={(e) => {
				e.stopPropagation();
				e.preventDefault();
				window.location.href = props.attributes.href;
			}}
		>
			{props.attributes.title.text}
		</button>
	);
};
