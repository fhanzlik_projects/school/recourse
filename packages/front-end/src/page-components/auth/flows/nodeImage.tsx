import { UiNode, UiNodeImageAttributes } from "@ory/client";
import Image from "next/image";
export const NodeImage = (props: {
	node: UiNode;
	attributes: UiNodeImageAttributes;
}) => {
	return (
		<Image
			src={props.attributes.src}
			alt={props.node.meta.label?.text ?? ``}
		/>
	);
};
