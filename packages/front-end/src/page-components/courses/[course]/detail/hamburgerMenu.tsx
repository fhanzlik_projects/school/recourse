import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { breakpoints } from "@recourse/commons";

export const HamburgerIcon = ({
	size,
	opened,
	toggle,
}: {
	size: string;
	opened: boolean;
	toggle: () => void;
}) => (
	<Wrapper>
		<Container size={size} onClick={toggle}>
			<BarTop opened={opened} />
			<BarMid opened={opened} />
			<BarBottom opened={opened} />
		</Container>
	</Wrapper>
);

const barSize = 1 / 10;

const Container = styled.button<{ size: string }>`
	@media (min-width: ${breakpoints.xl}) {
		display: none;
	}
	width: ${(p) => p.size};
	height: ${(p) => p.size};

	display: flex;
	flex-direction: column;
	justify-content: space-between;

	border: none;
	background: none;

	padding: calc(${(p) => p.size} / 7 * 2) calc(${(p) => p.size} / 7 / 2);
`;

const bar = css`
	width: 100%;
	height: ${barSize * 100}%;

	background-color: white;
	border-radius: 64rem;

	transition: 0.5s transform;
	transform-origin: 50% 50%;
`;

const shiftPercentage = 50 * (1 / barSize - 1);

const BarTop = styled.div<{ opened: boolean }>`
	${bar}

	transform: ${(p) =>
		p.opened && `translateY(${shiftPercentage}%) rotate(50grad)`};
`;

const BarMid = styled.div<{
	opened: boolean;
}>`
	${bar}

	transform: ${(p) => p.opened && `scaleX(0)`};
`;

const BarBottom = styled.div<{ opened: boolean }>`
	${bar}

	transform: ${(p) =>
		p.opened && `translateY(-${shiftPercentage}%) rotate(-50grad)`};
`;
const Wrapper = styled.div`
	padding-left: 1rem;
`;
