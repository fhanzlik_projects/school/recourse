import { error, runBackgroundAsyncTask } from "@recourse/commons";
import stackBlitz from "@stackblitz/sdk";
import { useEffect, useRef } from "react";

export const Playground = (props: { source: string } | { id: string }) => {
	const initialized = useRef(false);
	const ref = useRef<HTMLDivElement>(null);
	useEffect(() => {
		if (!initialized.current) {
			initialized.current = true;
			runBackgroundAsyncTask(async () => {
				if (`source` in props) {
					await stackBlitz.embedProject(
						ref.current ?? error.unexpectedNullish(),
						{
							title: `React`,
							template: `create-react-app`,
							description: `React App`,
							files: {
								"index.html": `<div id="root"></div>`,
								"index.tsx": `import * as React from 'react';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';

import App from './App';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
	<StrictMode>
	<App />
	</StrictMode>
);`,
								"App.tsx": props.source,
							},
							dependencies: {
								"@types/react": `^18.0.8`,
								"@types/react-dom": `^18.0.2`,
							},
						},
						{
							openFile: `App.tsx`,
							hideNavigation: true,
						},
					);
				} else {
					await stackBlitz.embedProjectId(
						ref.current ?? error.unexpectedNullish(),
						props.id,
						{
							hideNavigation: true,
							height: 750,
							view: `preview`,
						},
					);
				}
			});
		}
	}, [props]);
	return <div ref={ref} />;
};
