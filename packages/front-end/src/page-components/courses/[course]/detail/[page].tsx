import createAsciiDoctor, * as ADoc from "@asciidoctor/core";
import styled from "@emotion/styled";
import { AppLessonDetail } from "@recourse/api";
import {
	assert,
	background,
	breakpoints,
	error,
	getScrollParent,
	text,
	uuid,
} from "@recourse/commons";
import { useRouter } from "next/router";
import { Fragment, useEffect, useMemo, useRef, useState } from "react";
import { useQuery } from "react-query";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { vscDarkPlus } from "react-syntax-highlighter/dist/cjs/styles/prism";
import { useApiClient } from "../../../../contexts/api";
import { useOryClient } from "../../../../contexts/ory";
import { getUserSession } from "../../../../utils/ory";
import { screens } from "../../../index/screen";
import { GuidedPlayground } from "./guidedPlayground";
import { HamburgerIcon } from "./hamburgerMenu";
import { Playground } from "./playground";
import { Quiz } from "./quiz";

const asciiDoctor = createAsciiDoctor();

export const Page = () => {
	const router = useRouter();

	const courseId = router.query[`course`];
	const lessonId = router.query[`page`];

	if (courseId === undefined || lessonId === undefined) {
		// FIXME: add loaders
		return null;
	}

	assert(!Array.isArray(courseId), `there must only be single courseId`);
	assert(!Array.isArray(lessonId), `there must only be single pageId`);

	return <WithRouterParams courseId={courseId} lessonId={lessonId} />;
};

// eslint-disable-next-line @typescript-eslint/require-await
Page.getInitialProps = async () => {
	// we don't really need to fetch anything we just need to disable ASO
	return {};
};

const WithRouterParams = ({
	courseId,
	lessonId,
}: {
	courseId: string;
	lessonId: string;
}) => {
	const api = useApiClient();
	const ory = useOryClient();
	const user = useQuery({ queryFn: async () => getUserSession(ory) });
	const response = useQuery({
		queryKey: [`app.lesson.detail`, { lessonId }],
		queryFn: () =>
			api.app.lesson.detail(
				uuid.parse(lessonId),
				user.data?.data && uuid.parse(user.data.data.id),
			),
		enabled: !user.isLoading,
	});

	if (response.data === undefined) {
		// FIXME: add loaders
		return null;
	}

	return (
		<WithApiData
			courseId={courseId}
			lessonId={lessonId}
			response={response.data}
		/>
	);
};

const WithApiData = ({
	courseId,
	lessonId,
	response,
}: {
	courseId: string;
	lessonId: string;
	response: AppLessonDetail;
}) => {
	const contentRef = useRef<HTMLDivElement>(null);
	const [activeSection, setActiveSection] = useState<string>();

	//mobile menu
	const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
	const toggleTransform = () => {
		setMobileMenuOpen(!mobileMenuOpen);
	};

	const content = useMemo(() => {
		return asciiDoctor.load(response.lesson.content);
	}, [response.lesson.content]);

	const intersectionSections = useRef<IntersectionObserverEntry[]>([]);

	//scroll checking right navbar
	useEffect(() => {
		const observer = new IntersectionObserver((entries) => {
			if (entries.length > intersectionSections.current.length)
				intersectionSections.current = entries;
			intersectionSections.current = intersectionSections.current.map(
				(c) => entries.find((e) => e.target === c.target) ?? c,
			);
		});

		content.getSections().forEach((s) => {
			const id = s.getId();
			const target =
				document.getElementById(id) ?? error.unexpectedNullish();

			observer.observe(target);
		});

		return () => {
			observer.disconnect();
		};
	}, [content]);

	//scroll checking right navbar
	useEffect(() => {
		const scrollParent = getScrollParent(
			contentRef.current ?? error.unexpectedNullish(),
		);
		const abort = new AbortController();
		scrollParent.addEventListener(
			`scroll`,
			() => {
				const visible = intersectionSections.current.filter(
					(s) => s.isIntersecting,
				);
				if (visible.length === 0) return;
				const scrollRatio =
					scrollParent.scrollTop /
					(scrollParent.scrollHeight - scrollParent.offsetHeight);
				setActiveSection(
					visible[Math.round(scrollRatio * (visible.length - 1))]
						.target.id,
				);
			},
			{
				passive: true,
				signal: abort.signal,
			},
		);

		return () => abort.abort();
	}, []);

	return (
		<Container>
			{/* desktop navbar */}
			<Navbar role="list">
				<Course
					href={`http://localhost:3000/courses/${response.course.id}/detail/${response.lesson.id}`}
				>
					{response.course.name}
				</Course>
				{response.course.lessons.map((lesson) => (
					<Fragment key={lesson.id}>
						<LessonContainer>
							<LessonQuizTracker passed={lesson.completed} />
							<Lesson
								current={lessonId === lesson.id}
								key={lesson.id}
								href={`http://localhost:3000/courses/${response.course.id}/detail/${lesson.id}`}
							>
								{lesson.name}
							</Lesson>
						</LessonContainer>
					</Fragment>
				))}
			</Navbar>
			<Center ref={contentRef}>
				<HamburgerIcon
					opened={mobileMenuOpen}
					toggle={toggleTransform}
					size="calc(2rem + 3vw)"
				/>

				{/* mobile navbar */}
				<MobileMenuAnchor>
					<MobileMenuContainer visible={mobileMenuOpen}>
						<MobileLessonTitle
							href={`http://localhost:3000/courses/${
								response.course.id
							}/detail/${0}`}
						>
							{response.course.name}
						</MobileLessonTitle>
						<MobileNav>
							<MobileLessonList>
								{response.course.lessons.map((lesson) => (
									<Fragment key={lesson.id}>
										<LessonContainer>
											<LessonQuizTracker
												passed={lesson.completed}
											/>
											<MobileLesson
												current={lessonId === lesson.id}
												key={lesson.id}
												href={`http://localhost:3000/courses/${response.course.id}/detail/${lesson.id}`}
											>
												{lesson.name}
											</MobileLesson>
										</LessonContainer>
									</Fragment>
								))}
							</MobileLessonList>
							<MobileSectionDivider />
							<MobileSectionList>
								{content.getSections().map((section) => (
									<SectionLinkContainer key={section.getId()}>
										<SectionLink
											href={`#${section.getId()}`}
										>
											{section.getTitle()}
										</SectionLink>
									</SectionLinkContainer>
								))}
							</MobileSectionList>
						</MobileNav>
					</MobileMenuContainer>
				</MobileMenuAnchor>

				<Content dimmed={mobileMenuOpen}>
					{content.getSections().map((section) => (
						<AbstractBlock block={section} key={section.getId()} />
					))}
				</Content>
			</Center>
			<SectionNavbar>
				{content.getSections().map((section) => (
					<SectionLinkContainer key={section.getId()}>
						<SectionLinkTracker
							active={activeSection == section.getId()}
						/>
						<SectionLink href={`#${section.getId()}`}>
							{section.getTitle()}
						</SectionLink>
					</SectionLinkContainer>
				))}
			</SectionNavbar>
		</Container>
	);
};

export const BlockChildren = ({ block }: { block: ADoc.AbstractBlock }) => {
	return (
		<>
			{block.getBlocks().map((block: ADoc.AbstractBlock) => (
				// FIXME: using Id here is haram
				<AbstractBlock block={block} key={Opal.id(block)} />
			))}
		</>
	);
};

export const AbstractBlock = ({ block }: { block: ADoc.AbstractBlock }) => {
	switch (block.getStyle()) {
		case `quiz`:
			return <Quiz quiz={block} />;
		case `guided_playground`:
			return <GuidedPlayground block={block} />;
	}
	switch (block.getNodeName()) {
		case `paragraph`: {
			const blockCast = block as ADoc.Block;
			return (
				<Text
					dangerouslySetInnerHTML={{
						__html: blockCast.applySubstitutions(
							blockCast.getSourceLines().join(`\n`),
						) as string,
					}}
				/>
			);
		}
		case `section`:
			return (
				<Section>
					<Heading id={block.getId()}>
						{block.getCaptionedTitle()}
					</Heading>
					<BlockChildren block={block} />
				</Section>
			);
		case `listing`: {
			const blockCast = block as ADoc.Block;
			//checks if the source block is marked as interactive
			return blockCast.hasAttribute(`interactive-option`) ? (
				blockCast.hasAttribute(`by-id-option`) ? (
					<Playground id={blockCast.getSource()} />
				) : (
					<Playground source={blockCast.getSource()} />
				)
			) : (
				<SyntaxHighlighter
					codeTagProps={{
						style: { lineHeight: `inherit`, fontSize: `inherit` },
					}}
					customStyle={{
						lineHeight: `inherit`,
						fontSize: `inherit`,
						margin: `0.25rem 0rem`,
					}}
					language="tsx"
					style={vscDarkPlus}
				>
					{blockCast.getSource()}
				</SyntaxHighlighter>
			);
		}
		case `ulist`:
			return (
				<ListContainer>
					<BlockChildren block={block} />
				</ListContainer>
			);
		case `list_item`: {
			const blockCast = block as ADoc.ListItem;
			return (
				<li>
					<ListItemText
						dangerouslySetInnerHTML={{
							__html: blockCast.getText(),
						}}
					/>
					<BlockChildren block={block} />
				</li>
			);
		}
		case `open`: {
			return <BlockChildren block={block} />;
		}

		default:
			return error(
				`encountered unknown block: ${block.getNodeName()}[${block.getStyle()}]`,
			);
	}
};

const Container = styled(screens.Common)`
	display: flex;
	flex-direction: row;
	@media (min-width: ${breakpoints.md}) {
		padding: 2rem 0 2rem 0;
	}
	${background(`normal`)}
	padding: 1rem 0 2rem 0;
	justify-content: center;
`;
//==left==
const Navbar = styled.nav`
	@media (min-width: ${breakpoints.xl}) {
		display: flex;
		flex-direction: column;
		width: 12rem;
		padding: 1rem;
		position: sticky;
		top: 0;
	}
	display: none;
`;

const Course = styled.a`
	${text({ family: `montserrat`, weight: `bold`, size: 1.25 })};
	padding: 0;
	margin: 0;
`;

const LessonContainer = styled.div`
	display: flex;
`;

const LessonQuizTracker = styled.div<{ passed: boolean }>`
	width: 0.1rem;
	margin-left: 0.5rem;
	left: 0.5rem;
	flex-shrink: 0;
	background-color: ${(p) => (p.passed ? `green` : `grey`)};
`;

const Lesson = styled.a<{ current: boolean }>`
	padding: 0.25rem 0.5rem;
	margin: 0;
	${text({ family: `montserrat` })}
	font-weight:${(p) => (p.current ? `bold` : `regular`)};
	color: ${(p) => (p.current ? `#1d90f5` : ``)};
`;
//==center==
const Center = styled.div`
	border-radius: 0.5rem;
	flex-grow: 1;
	/* the 100% here makes sure that none of the content overflows */
	max-width: min(60rem, 100%);

	display: flex;
	flex-direction: column;
`;

const Content = styled.div<{ dimmed: boolean }>`
	padding: 1rem 1rem 5rem 1rem;
	isolation: isolate;
	filter: brightness(${(p) => (p.dimmed ? 0.7 : 1)});
	transition: 0.25s;
	display: flex;
	flex-direction: column;
	${text()}
`;

const Section = styled.section`
	display: flex;
	flex-direction: column;
`;

const Text = styled.p`
	${text()}
	padding: 0.25rem 1rem;
	strong {
		${text({ weight: `bold` })}
	}
`;

const Heading = styled.h1`
	${text({
		family: `montserrat`,
		weight: `bold`,
		size: 2,
	})}
	padding:0.5rem 0.5rem;
`;

export const ListContainer = styled.ul`
	padding: 0.25rem 2rem;
`;

const ListItemText = styled.span`
	padding: 0;
`;

//==right==
const SectionNavbar = styled.nav`
	@media (min-width: ${breakpoints.xl}) {
		display: flex;
		flex-direction: column;
		width: 12rem;
		padding: 1rem;
		position: sticky;
		top: 0;
		flex-shrink: 0;
	}
	display: none;
`;

const SectionLinkContainer = styled.div`
	display: flex;
`;

const SectionLinkTracker = styled.div<{ active: boolean }>`
	width: 0.1rem;
	margin-left: 0.5rem;
	flex-shrink: 0;
	background-color: ${(p) =>
		p.active ? p.theme.colours.foreground.primary : `grey`};
`;

const SectionLink = styled.a`
	${text({ family: `montserrat` })}
	padding: 0.25rem 0.5rem;
`;

//==mobile==
const MobileMenuAnchor = styled.div`
	position: relative;
`;

const MobileMenuContainer = styled.div<{ visible: boolean }>`
	@media (min-width: ${breakpoints.xl}) {
		visibility: hidden;
	}
	z-index: 1;
	visibility: visible;
	position: absolute;
	left: ${(p) => (p.visible ? 0 : `-120%`)};
	width: 100%;
	border-radius: 1rem;
	${background(`normal`)};
	box-shadow: 0 0rem 0.5rem 0 hsla(0, 0%, 0%, 0.5);
	padding: 1rem;
	transition: 0.25s ease-in-out;
	display: flex;
	flex-direction: column;
`;
const MobileLessonTitle = styled.a`
	${text({
		family: `montserrat`,
		size: 1.25,
		weight: `bold`,
		colour: `bright`,
	})};
`;
const MobileLessonList = styled.div`
	display: flex;
	flex-direction: column;
`;
const MobileSectionDivider = styled.div`
	width: 3px;
	background-color: black;
	border-radius: 1rem;
`;

const MobileLesson = styled.a<{ current: boolean }>`
	${text({
		family: `montserrat`,
		size: 1,
		weight: `regular`,
	})}
	padding-left: 1rem;
	padding-top: 0.5rem;
	font-weight: ${(p) => (p.current ? `bold` : `regular`)};
	color: ${(p) => (p.current ? `#1d90f5` : ``)};
`;
const MobileSectionList = styled.div`
	${text({
		family: `montserrat`,
		size: 1,
		weight: `regular`,
	})}
	display:flex;
	flex-direction: column;
	flex-grow: 1;
`;

const MobileNav = styled.div`
	display: flex;
	flex-direction: row;
	gap: 1rem;
`;
