import * as ADoc from "@asciidoctor/core";
import styled from "@emotion/styled";
import { breakpoints, button, error } from "@recourse/commons";
import { useState } from "react";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { vscDarkPlus } from "react-syntax-highlighter/dist/cjs/styles/prism";
import { Playground } from "./playground";
import { AbstractBlock, ListContainer } from "./[page]";

export const GuidedPlayground = ({ block }: { block: ADoc.AbstractBlock }) => {
	const [hintCounter, setHintCounter] = useState(0);
	const [showSolution, setShowSolution] = useState(false);
	const {
		hints = error(`no hints provided!`),
		solution = error(`no solution provided`),
		initalContent = error(`no inital content`),
	} = block.getBlocks().reduce<{
		hints?: ADoc.AbstractBlock[];
		solution?: ADoc.Block;
		initalContent?: ADoc.Block;
	}>((acc, child: ADoc.AbstractBlock) => {
		switch (child.getStyle()) {
			case `hints`: {
				if (acc.hints !== undefined)
					error(`multiple hint lists encountered`);
				acc.hints = child.getBlocks();
				break;
			}
			case `solution`: {
				const blockCast = child as ADoc.Block;
				acc.solution = blockCast;
				break;
			}
			case `initial_contents`: {
				const blockCast = child as ADoc.Block;
				acc.initalContent = blockCast;
				break;
			}
		}
		return acc;
	}, {});
	return (
		<GPContainer>
			<Playground source={initalContent.getSource()} />
			<ButtonContainer>
				{hintCounter < hints.length && (
					<GPButton onClick={() => setHintCounter(hintCounter + 1)}>
						show hint
					</GPButton>
				)}
				{!showSolution && (
					<GPButton onClick={() => setShowSolution(true)}>
						show solution
					</GPButton>
				)}
			</ButtonContainer>
			<ListContainer>
				{hints.map((hint, index) => {
					return (
						index < hintCounter && (
							<AbstractBlock block={hint} key={Opal.id(hint)} />
						)
					);
				})}
			</ListContainer>
			{showSolution && (
				<>
					<SubHeading>Solution</SubHeading>
					<SyntaxHighlighter
						codeTagProps={{
							style: {
								lineHeight: `inherit`,
								fontSize: `inherit`,
							},
						}}
						customStyle={{
							lineHeight: `inherit`,
							fontSize: `inherit`,
						}}
						language="tsx"
						style={vscDarkPlus}
					>
						{solution.getSource()}
					</SyntaxHighlighter>
				</>
			)}
		</GPContainer>
	);
};

const GPContainer = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1rem;
	border-radius: 4rem;
`;
const SubHeading = styled.h3`
	font-size: 1.75rem;
`;
const ButtonContainer = styled.div`
	@media (min-width: ${breakpoints.md}) {
		justify-content: space-between;
		flex-direction: row;
		gap: 0;
	}
	gap: 1rem;
	display: flex;
	flex-direction: column;
	padding: 0 25% 0 25%;
`;
const GPButton = styled.button`
	@media (min-width: ${breakpoints.md}) {
		width: 30%;
	}
	${button({ colour: `primary` })}
	padding:0.5rem;
	width: 100%;
`;
