import * as ADoc from "@asciidoctor/core";
import styled from "@emotion/styled";
import { AppLessonDetail } from "@recourse/api";
import {
	background,
	button,
	error,
	foreground,
	text,
	uuid,
} from "@recourse/commons";
import assert from "assert";
import _ from "lodash";
import { useRouter } from "next/router";
import { Fragment, useMemo, useState } from "react";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useApiClient } from "../../../../contexts/api";
import { useOryClient } from "../../../../contexts/ory";
import { getUserSession } from "../../../../utils/ory";
import { AbstractBlock } from "./[page]";

export const Quiz = ({ quiz }: { quiz: ADoc.AbstractBlock }) => {
	const [currentQuestion, setCurrentQuestion] = useState(0);
	const [answers, setAnswers] = useState<ADoc.ListItem[]>([]);
	const [submitted, setSubmitted] = useState(false);

	const blockIsListItem = (
		block: ADoc.AbstractBlock,
	): block is ADoc.ListItem => block.getNodeName() === `list_item`;

	const questionAnswerPairs = useMemo(() => {
		return quiz
			.getBlocks()
			.reduce<
				{ question: ADoc.AbstractBlock; answers?: ADoc.ListItem[] }[]
			>((questionAnswerPairs, child) => {
				if (child.getStyle() === `question`) {
					// create a new pair for every encountered question
					questionAnswerPairs.push({ question: child });
				}
				if (child.getStyle() === `answers`) {
					const lastPair =
						questionAnswerPairs.at(-1) ??
						error(`encountered answers with no question for them`);
					assert(
						lastPair.answers === undefined,
						`encountered multiple answer sets for a question`,
					);
					lastPair.answers = child.getBlocks().map((item) => {
						assert(
							blockIsListItem(item),
							`answers should only contain list items`,
						);
						return item;
					});
				}

				return questionAnswerPairs;
			}, [])
			.map(({ question, answers }) => {
				assert(
					answers !== undefined,
					`every question should have an answer set`,
				);
				return { question, answers };
			});
	}, [quiz]);

	return (
		<QuizContainer>
			{currentQuestion !== questionAnswerPairs.length ? (
				<QuizPart
					{...questionAnswerPairs[currentQuestion]}
					setAnswer={(value) => {
						const newAnswers = [...answers];
						newAnswers[currentQuestion] = value;
						setAnswers(newAnswers);
					}}
					currentAnswer={answers[currentQuestion]}
				/>
			) : (
				<FinalScreen
					answers={answers}
					submitted={submitted}
					setSubmitted={setSubmitted}
					setCurrentQuestion={setCurrentQuestion}
					currentQuestion={currentQuestion}
					questionAnswerPairs={questionAnswerPairs}
				/>
			)}
			<Buttons
				currentQuestion={currentQuestion}
				submitted={submitted}
				setCurrentQuestion={setCurrentQuestion}
				answers={answers}
				questionAnswerPairs={questionAnswerPairs}
				setSubmitted={setSubmitted}
			/>
		</QuizContainer>
	);
};

const Buttons = ({
	currentQuestion,
	submitted,
	setCurrentQuestion,
	answers,
	questionAnswerPairs,
	setSubmitted,
}: {
	currentQuestion: number;
	submitted: boolean;
	setCurrentQuestion: (value: number) => void;
	answers: ADoc.ListItem[];
	questionAnswerPairs: {
		question: ADoc.AbstractBlock;
		answers: ADoc.ListItem[];
	}[];
	setSubmitted: (value: boolean) => void;
}) => {
	return (
		<ButtonBar>
			<Button
				hidden={submitted}
				disabled={currentQuestion === 0 || submitted}
				onClick={() => {
					setCurrentQuestion(currentQuestion - 1);
				}}
			>
				<FaChevronLeft />
			</Button>
			<Button
				hidden={!submitted}
				onClick={() => {
					setCurrentQuestion(0);
					setSubmitted(false);
				}}
			>
				Try again?
			</Button>
			<Button
				hidden={submitted}
				disabled={
					// is on final screen
					currentQuestion === questionAnswerPairs.length ||
					// didn't answer current question
					currentQuestion >= answers.length ||
					submitted
				}
				onClick={() => {
					setCurrentQuestion(currentQuestion + 1);
				}}
			>
				<FaChevronRight />
			</Button>
		</ButtonBar>
	);
};

const QuizPart = ({
	question,
	answers,
	setAnswer,
	currentAnswer,
}: {
	question: ADoc.AbstractBlock;
	answers: ADoc.ListItem[];
	setAnswer: (value: ADoc.ListItem) => void;
	currentAnswer: ADoc.ListItem | undefined;
}) => {
	return (
		<>
			{/*question*/}
			<AbstractBlock block={question} />
			{/*answers*/}
			<QuizAnsForm>
				{answers.map((ans) => (
					<Fragment key={Opal.id(ans)}>
						<label>
							<input
								name={`quiz_answers/${
									Opal.id(answers) ??
									error.unexpectedNullish()
								}`}
								type={`radio`}
								checked={currentAnswer === ans ? true : false}
								onChange={() => setAnswer(ans)}
							/>
							<QuizAnsSpan
								dangerouslySetInnerHTML={{
									__html: ans.getText(),
								}}
							/>
						</label>
					</Fragment>
				))}
			</QuizAnsForm>
		</>
	);
};

const FinalScreen = ({
	answers,
	submitted,
	setSubmitted,
}: {
	answers: ADoc.ListItem[];
	submitted: boolean;
	setSubmitted: (value: boolean) => void;
	setCurrentQuestion: (value: number) => void;
	currentQuestion: number;
	questionAnswerPairs: {
		question: ADoc.AbstractBlock;
		answers: ADoc.ListItem[];
	}[];
}) => {
	const correctAnswers = answers.reduce((acc, ans) => {
		return ans.hasAttribute(`checked`) ? acc + 1 : acc;
	}, 0);
	const percentage = Math.floor((correctAnswers / answers.length) * 100);
	const passed = percentage > 50;

	const router = useRouter();
	const lessonId = router.query[`page`] ?? error.unexpectedNullish();
	assert(!Array.isArray(lessonId), `there must only be single pageId`);

	const queryClient = useQueryClient();

	const api = useApiClient();
	const ory = useOryClient();
	const user = useQuery({ queryFn: async () => getUserSession(ory) });
	const setLessonCompletedMutation = useMutation({
		mutationFn: ({ lesson, user }: { lesson: string; user: string }) =>
			api.app.lesson.set_completed(
				uuid.parse(lesson),
				uuid.parse(user),
				passed,
			),
		onSuccess: () => {
			queryClient.setQueryData<AppLessonDetail | undefined>(
				[`app.lesson.detail`, { lessonId }],
				(oldData) =>
					oldData &&
					_.merge({}, oldData, {
						course: {
							lessons: oldData.course.lessons.map((lesson) => {
								if (lesson.id !== lessonId) return lesson;
								return {
									...lesson,
									completed: passed,
								};
							}),
						},
					}),
			);
		},
	});

	return submitted ? (
		<>
			<FinalScreenContainer>
				<ResultsText>Result: </ResultsText>
				<ResultsContainer>
					<AnsVAll>
						{correctAnswers} / {answers.length}
					</AnsVAll>
					<Percentage>{percentage}%</Percentage>
				</ResultsContainer>
				{passed ? (
					<ResultsText>
						Congratulations you have
						<TestTextPass> Passed</TestTextPass>
					</ResultsText>
				) : (
					<ResultsText>
						You have
						<TestTextFail> Failed</TestTextFail>
					</ResultsText>
				)}
				{` `}
			</FinalScreenContainer>
		</>
	) : (
		<Button
			onClick={() => {
				setSubmitted(true);
				if (user.data?.data === undefined) return;
				setLessonCompletedMutation.mutate({
					lesson: lessonId,
					user: user.data.data.id,
				});
			}}
			hidden={false}
		>
			Submit Answers
		</Button>
	);
};

const QuizContainer = styled.div`
	display: flex;
	flex-direction: column;
	${background(`elevated.0`)}
	max-width: 45rem;
	align-self: center;
	width: 100%;
	border-radius: 0.25rem;
	padding: 2rem 0.75rem;
	margin: 1rem;
	box-shadow: 0 0rem 0.5rem 0 hsla(0, 0%, 0%, 0.5);
	gap: 0.5rem;
`;

const QuizAnsForm = styled.form`
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
	padding: 0rem 2rem;
`;

const QuizAnsSpan = styled.span`
	padding-left: 1rem;
	${text({
		colour: `bright`,
	})};
`;

const ButtonBar = styled.div`
	display: flex;
	flex-direction: row;
	${background(`elevated.0`)}
	justify-content:space-evenly;
	padding: 0.5rem 0.5rem;
	align-self: stretch;
	max-height: 3rem;
`;

const AnsVAll = styled.p`
	${text({
		colour: `bright`,
	})};
`;

const Percentage = styled.p`
	${text({
		colour: `bright`,
	})};
`;

const TestTextFail = styled.span`
	${foreground(`normal`)};
`;

const TestTextPass = styled.span`
	${foreground(`secondary`)}
`;

const FinalScreenContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-self: center;
	max-width: 70%;
	width: 100%;
	gap: 1rem;
	${text({
		family: `montserrat`,
		weight: `bold`,
		size: 1.5,
	})}
`;

const ResultsContainer = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-evenly;
`;

const ResultsText = styled.p`
	${text({
		family: `montserrat`,
		weight: `regular`,
		size: 1.5,
	})}
	align-self:center;
`;

const Button = styled.button<{ hidden: boolean }>`
	${button({ colour: `primary` })}
	${text({
		family: `montserrat`,
		weight: `bold`,
		size: 1.5,
	})}
	align-self: center;
	display: flex;
	align-items: center;
	justify-content: center;
	visibility: ${(p) => (p.hidden ? `hidden` : ``)};
`;
