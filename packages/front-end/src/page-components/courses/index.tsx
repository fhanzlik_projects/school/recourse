import styled from "@emotion/styled";
import { background, breakpoints, text } from "@recourse/commons";
import type { NextPage } from "next";
import { Fragment, useState } from "react";
import { useQuery } from "react-query";
import { useApiClient } from "../../contexts/api";
import { screens } from "../index/screen";
import SearchIcon from "./icons/searchIcon.svg";

export const Page: NextPage = () => {
	const [input, setInput] = useState(``);

	const api = useApiClient();
	const courses = useQuery(`app.course.list`, () => api.app.course.list());

	return (
		<AllContainer>
			<Container>
				<TextContainer>
					<Headline>Courses</Headline>
					<Subtitle>
						Step by step tutorials to guide you through your react
						journey
					</Subtitle>
				</TextContainer>
				<SearchbarWrapper>
					<SearchIconWrapper>
						<SearchIcon />
					</SearchIconWrapper>
					<Searchbar
						type="text"
						placeholder="Search"
						onChange={(i) => {
							setInput(i.target.value);
						}}
					/>
				</SearchbarWrapper>
				<CoursesContainter>
					<Courses role="list">
						{courses.data
							?.filter(
								(course) =>
									course.name
										.toLowerCase()
										.includes(input.toLowerCase()) ||
									course.lessons.some((lesson) =>
										lesson.name
											.toLowerCase()
											.includes(input.toLowerCase()),
									),
							)

							.map((course, index) => {
								return (
									<Fragment key={course.id}>
										{index !== 0 && <CourseSplitter />}
										<li>
											<Course
												key={index}
												href={`http://localhost:3000/courses/${course.id}/detail/${course.first_lesson}`}
											>
												<PictureWrapper>
													<Picture
														alt=""
														src={course.cover_image}
													/>
												</PictureWrapper>
												<div>
													<CourseHeadline>
														{course.name}
													</CourseHeadline>
													<CourseText>
														{course.description}
													</CourseText>
												</div>
											</Course>
										</li>
									</Fragment>
								);
							})}
					</Courses>
				</CoursesContainter>
			</Container>
		</AllContainer>
	);
};

const AllContainer = styled(screens.Common)`
	display: flex;
	justify-content: center;
`;

const Container = styled.div`
	@media (min-width: ${breakpoints.sm}) {
		width: 34rem;
	}
	@media (min-width: ${breakpoints.md}) {
		width: 46rem;
	}
	@media (min-width: ${breakpoints.lg}) {
		width: 58rem;
	}
	@media (min-width: ${breakpoints.xl}) {
		width: 70rem;
	}
	@media (min-width: ${breakpoints.xxl}) {
		width: 88rem;
	}
	width: 22rem;

	display: flex;
	flex-direction: column;
`;

const TextContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	gap: 1rem;
	padding-bottom: 5rem;
	justify-content: center;
`;

const Headline = styled.p`
	${text({ size: 2 })}
	padding-top:4rem;
`;

const Subtitle = styled.p`
	@media (min-width: ${breakpoints.md}) {
		text-align: left;
	}
	text-align: center;
	${text({ family: `montserrat`, weight: `bold`, size: 2 })}
`;
const SearchbarWrapper = styled.div`
	display: flex;
	flex-direction: row;
	height: 3.68rem;
	${background(`elevated.1`)};
	${text()};
	border-radius: 1rem;
	padding-left: 1.25rem;
	gap: 1rem;
`;
const SearchIconWrapper = styled.div`
	@media (min-width: ${breakpoints.md}) {
		display: none;
	}
	padding-top: 1.25rem;
`;
const Searchbar = styled.input`
	@media (min-width: ${breakpoints.md}) {
		border-radius: 0.5rem;
	}
	${text()};
	background-color: transparent;
	border-radius: 1rem;
	width: 100%;
	height: 3.68rem;
	outline: none;
`;

const CoursesContainter = styled.div`
	@media (min-width: ${breakpoints.md}) {
		align-items: center;
		justify-content: center;
		align-self: stretch;
		padding: 3rem 6rem 1rem 6rem;
	}
	align-items: stretch;
	align-self: center;
	padding-top: 3rem;
`;

const Courses = styled.ul`
	display: flex;
	flex-direction: column;
	gap: 2rem;
	align-items: center;
	list-style: none;

	@media (min-width: ${breakpoints.md}) {
		align-items: center;
	}
`;

const Course = styled.a`
	@media (min-width: ${breakpoints.md}) {
		flex-direction: row;
		height: 10rem;
		width: 100%;
	}
	width: 22rem;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	${background(`normal`)};
	border-radius: 1rem;
	box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.7);
	transition: transform 0.2s;
	&:hover {
		transform: translateX(0.5rem);
	}
	text-decoration: none;
`;

const CourseSplitter = styled.div`
	@media (min-width: ${breakpoints.md}) {
		width: 95%;
	}
	width: 18rem;
	height: 2px;
	border-radius: 1rem;
	${background(`elevated.1`)};
`;

const Picture = styled.img`
	@media (min-width: ${breakpoints.md}) {
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: 1rem 0 0 1rem;
	}
	width: 100%;
	height: 100%;
	object-fit: cover;
	border-radius: 1rem 1rem 0 0;
`;

const PictureWrapper = styled.div`
	@media (min-width: ${breakpoints.md}) {
		flex-basis: 18rem;
		flex-shrink: 0;
	}
	flex-basis: 12rem;
	flex-shrink: 0;
`;

const CourseHeadline = styled.p`
	${text({ weight: `bold`, size: 2 })}
	padding:1rem 0 0.5rem 1rem;
	@media (min-width: ${breakpoints.md}) {
		justify-content: flex-start;
	}
	justify-content: center;
	display: flex;
`;

const CourseText = styled.p`
	${text({ family: `montserrat` })}
	padding-left: 1rem;
	@media (min-width: ${breakpoints.md}) {
		text-align: left;
	}
	text-align: center;
`;
