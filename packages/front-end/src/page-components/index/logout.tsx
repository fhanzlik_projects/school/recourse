import styled from "@emotion/styled";
import { runBackgroundAsyncTask, text } from "@recourse/commons";
import { AxiosError } from "axios";
import { useRouter } from "next/router";
import { useCallback } from "react";
import { useOryClient } from "../../contexts/ory";

export const LogoutButton = () => {
	const ory = useOryClient();
	const router = useRouter();
	const logout = useCallback(async () => {
		let logoutToken: string;
		try {
			logoutToken = (await ory.createBrowserLogoutFlow()).data
				.logout_token;
		} catch (err: unknown) {
			if (err instanceof AxiosError) {
				switch (err.response?.status) {
					case 401:
						// do nothing, the user is not logged in
						return;
				}
			}
			// Something else happened!
			throw err;
		}
		await ory.updateLogoutFlow({ token: logoutToken });
		router.reload();
	}, [ory, router]);
	return (
		<Button onClick={() => runBackgroundAsyncTask(logout)}>Logout</Button>
	);
};
const Button = styled.button`
	${text({ weight: `bold`, family: `montserrat` })}
	display: flex;
	align-items: center;
	background-color: transparent;

	padding: 1rem;
`;
