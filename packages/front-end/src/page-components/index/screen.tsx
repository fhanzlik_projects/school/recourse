import styled from "@emotion/styled";
import { background } from "@recourse/commons";
import { ReactNode } from "react";
import { Header } from "./header";

export const screens = {
	Custom: (props: { children: ReactNode; className?: string }) => (
		<Container className={props.className}>{props.children}</Container>
	),
	Common: (props: { children: ReactNode; className?: string }) => (
		<screens.Custom>
			<Header />
			<Main className={props.className}>{props.children}</Main>
		</screens.Custom>
	),
};

const Container = styled.div`
	min-width: 100vw;
	height: 100vh;
	scroll-behavior: smooth;
	display: flex;
	flex-direction: column;
`;

const Main = styled.div`
	flex-grow: 1;
	align-items: flex-start;
	overflow: auto;

	scroll-behavior: smooth;
	box-shadow: inset 0 1rem 1rem -1rem hsla(0, 0%, 0%, 0.1); //move to commons
	${background(`normal`)}
`;
