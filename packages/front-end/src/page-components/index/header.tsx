import styled from "@emotion/styled";
import { background, foreground, text } from "@recourse/commons";
import Link from "next/link";
import { useQuery } from "react-query";
import Zod from "zod";
import { useOryClient } from "../../contexts/ory";
import { appUrls } from "../../utils/constants";
import { getUserSession } from "../../utils/ory";
import LogoIcon from "./icons/icon.svg";
import { LogoutButton } from "./logout";

export const Header = () => {
	const ory = useOryClient();
	const user = useQuery({ queryFn: () => getUserSession(ory) });

	return (
		<HeaderContainer>
			<Nav>
				<LogoIconA href="/">
					<LogoIcon />
				</LogoIconA>
				<HeaderLink href="/courses">Courses</HeaderLink>
				{user.data &&
					Zod.object({ isAdmin: Zod.boolean().default(false) }).parse(
						user.data.data.identity.traits,
					).isAdmin && <HeaderLink href="/cms">Content</HeaderLink>}
			</Nav>
			{user.status === `loading` || user.data === undefined ? (
				<HeaderLink href={appUrls.signIn()}>Sign in</HeaderLink>
			) : (
				<LogoutButton />
			)}
		</HeaderContainer>
	);
};

export const headerHeight = `4rem`;

const HeaderContainer = styled.header`
	position: sticky;
	top: 0;

	box-shadow: 0 0rem 0.5rem 0 hsla(0, 0%, 0%, 0.5);
	justify-content: space-between;
	flex-basis: ${headerHeight};
	display: flex;
	${background(`normal`)}
`;

const Nav = styled.nav`
	display: flex;
`;

export const HeaderLink = styled(Link)`
	${text({ weight: `bold`, family: `montserrat` })}

	display: flex;
	align-items: center;

	padding: 1rem;
`;

const LogoIconA = styled(Link)`
	padding: 0.5rem;
	${foreground(`primary`)};
`;
