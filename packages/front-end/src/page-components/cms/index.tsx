import styled from "@emotion/styled";
import { runBackgroundAsyncTask } from "@recourse/commons";
import { useRouter } from "next/router";
import { useQuery } from "react-query";
import { useOryClient } from "../../contexts/ory";
import { appUrls } from "../../utils/constants";
import { getUserSession } from "../../utils/ory";
import { screens } from "../index/screen";

export const Page = () => {
	const router = useRouter();
	const ory = useOryClient();
	const user = useQuery({ queryFn: async () => getUserSession(ory) });
	if (!user.isLoading && user.data === undefined) {
		runBackgroundAsyncTask(async () => {
			await router.push(appUrls.signIn());
		});
	}
	return (
		<screens.Common>
			<StyledFrame
				id="cms"
				title="cms"
				src="http://localhost:5173/content-manager/collections"
			/>
		</screens.Common>
	);
};
const StyledFrame = styled.iframe`
	width: 100%;
	height: 100%;
`;
