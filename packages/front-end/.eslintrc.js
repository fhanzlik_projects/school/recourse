module.exports = {
	plugins: ["@typescript-eslint"],
	parserOptions: {
		tsconfigRootDir: __dirname,
		project: [
			"./tsconfig.json",
			"../tsconfig/tsconfig.settings.json",
			"../tsconfig/tsconfig.base.json",
		],
	},
	extends: [
		"next/core-web-vitals",
		"eslint:recommended",
		"plugin:jsx-a11y/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"plugin:@typescript-eslint/strict",
		"prettier",
	],
	rules: {
		"@typescript-eslint/quotes": [
			"warn",
			"backtick",
			{ avoidEscape: true },
		],
		"@typescript-eslint/no-unused-vars": [
			"warn",
			{ argsIgnorePattern: "^_" },
		],
		"@typescript-eslint/consistent-type-definitions": ["warn", "type"],
		"react/self-closing-comp": ["warn"],
	},

	overrides: [
		{
			files: ["*.d.ts"],
			rules: {
				// in `.d.ts` files we usually want declaration merging
				"@typescript-eslint/no-empty-interface": ["off"],
				"@typescript-eslint/consistent-type-definitions": ["off"],
			},
		},
	],
};
