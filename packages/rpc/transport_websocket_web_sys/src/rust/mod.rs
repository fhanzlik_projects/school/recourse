//---------------//
// feature gates //
//---------------//
//-------//
// lints //
//-------//
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

use {
	futures::{Sink, SinkExt, Stream, StreamExt},
	std::{fmt::Debug, pin::Pin},
	thiserror::Error,
	ws_stream_wasm::WsStream,
};

pub struct TransportWebsocket {
	stream: WsStream,
}

impl TransportWebsocket {
	#[must_use]
	pub const fn new(stream: WsStream) -> Self {
		Self { stream }
	}
}

impl Sink<Vec<u8>> for TransportWebsocket {
	type Error = Error;

	fn poll_ready(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		self.stream.poll_ready_unpin(cx).map_err(Error::from)
	}

	fn start_send(mut self: Pin<&mut Self>, item: Vec<u8>) -> Result<(), Self::Error> {
		self.stream
			.start_send_unpin(ws_stream_wasm::WsMessage::Binary(item))
			.map_err(Error::from)
	}

	fn poll_flush(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		self.stream.poll_flush_unpin(cx).map_err(Error::from)
	}

	fn poll_close(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		// TODO: set proper close code/reason
		self.stream.poll_close_unpin(cx).map_err(Error::from)
	}
}

impl Stream for TransportWebsocket {
	type Item = Result<Vec<u8>, Error>;

	fn poll_next(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Option<Self::Item>> {
		self.stream.poll_next_unpin(cx).map(|item| {
			item.map(|item| match item {
				ws_stream_wasm::WsMessage::Text(_) => Err(Error::ReceivedText),
				ws_stream_wasm::WsMessage::Binary(buffer) => Ok(buffer),
			})
		})
	}
}

#[derive(Debug, Error)]
pub enum Error {
	#[error(transparent)]
	Internal(#[from] ws_stream_wasm::WsErr),

	#[error("the websocket has received a string, but only array buffers are supported")]
	ReceivedText,
}
