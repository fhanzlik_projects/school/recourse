//---------------//
// feature gates //
//---------------//
//-------//
// lints //
//-------//
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

use {
	futures::{ready, Sink, SinkExt, Stream, StreamExt},
	serde::{Deserialize, Serialize},
	std::{fmt::Debug, marker::PhantomData, pin::Pin, task::Poll},
	thiserror::Error,
};

pub struct TransportPostcard<Sent, Received, Next> {
	phantom_data: PhantomData<(Sent, Received)>,
	next: Next,
}

impl<Sent, Received, Next> TransportPostcard<Sent, Received, Next>
where
	Next: Sink<Vec<u8>> + Stream<Item = Result<Vec<u8>, Next::Error>> + Unpin,
{
	pub const fn new(next: Next) -> Self {
		Self {
			phantom_data: PhantomData,
			next,
		}
	}
}

impl<Sent, Received, Next> Sink<Sent> for TransportPostcard<Sent, Received, Next>
where
	Sent: Serialize + Unpin,
	Received: Unpin,
	Next: Sink<Vec<u8>> + Unpin,
	Next::Error: std::error::Error + 'static + Sync + Send,
{
	type Error = Error<Next::Error>;

	fn poll_ready(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		self.get_mut()
			.next
			.poll_ready_unpin(cx)
			.map_err(Error::Next)
	}

	fn start_send(self: Pin<&mut Self>, item: Sent) -> Result<(), Self::Error> {
		self.get_mut()
			.next
			.start_send_unpin(::postcard::to_allocvec(&item)?)
			.map_err(Error::Next)
	}

	fn poll_flush(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		self.get_mut()
			.next
			.poll_flush_unpin(cx)
			.map_err(Error::Next)
	}

	fn poll_close(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		self.get_mut()
			.next
			.poll_close_unpin(cx)
			.map_err(Error::Next)
	}
}

impl<Sent, Received, Next, NextError> Stream for TransportPostcard<Sent, Received, Next>
where
	Self: Unpin,
	Received: for<'de> Deserialize<'de>,
	Next: Stream<Item = Result<Vec<u8>, NextError>> + Unpin,
	NextError: std::error::Error + Sync + Send + 'static,
{
	type Item = Result<Received, Error<NextError>>;

	fn poll_next(
		self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Option<Self::Item>> {
		let item = ready!(self.get_mut().next.poll_next_unpin(cx));
		let item = match item {
			Some(item) => item.map_err(Error::Next)?,
			None => return Poll::Ready(None),
		};

		Poll::Ready(Some(Ok(
			::postcard::from_bytes(&item).map_err(Error::Postcard)?
		)))
	}
}

#[derive(Debug, Error)]
pub enum Error<NextError> {
	#[error("serialization/deserialization ({0:?})")]
	Postcard(#[from] postcard::Error),
	#[error(transparent)]
	Next(NextError),
}
