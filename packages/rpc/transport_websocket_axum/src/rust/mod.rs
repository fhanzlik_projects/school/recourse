//---------------//
// feature gates //
//---------------//
#![feature(let_chains)]
//-------//
// lints //
//-------//
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

use {
	axum::extract::ws::{Message, WebSocket},
	futures::{ready, Sink, SinkExt, Stream, StreamExt},
	std::{error::Error as _, fmt::Debug, pin::Pin, task::Poll},
};

pub struct TransportWebsocket {
	stream: WebSocket,
}

impl TransportWebsocket {
	pub const fn new(socket: WebSocket) -> Self {
		Self { stream: socket }
	}
}

impl Sink<Vec<u8>> for TransportWebsocket {
	type Error = Error;

	fn poll_ready(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		Poll::Ready(
			ready!(self.stream.poll_ready_unpin(cx))
				.or_else(|e| {
					if let Some(source) = e.source()
						&& let Some(r) = source.downcast_ref()
						&& matches!(r, tungstenite::Error::ConnectionClosed)
					{
						// for whatever reason tarpc tries to ready the connection for sending
						// even after it is notified that it has been closed.
						Ok(())
					} else {
						Err(e)
					}
				})
				.map_err(Error::from),
		)
	}

	fn start_send(mut self: Pin<&mut Self>, item: Vec<u8>) -> Result<(), Self::Error> {
		self.stream
			.start_send_unpin(Message::Binary(item))
			.map_err(Error::from)
	}

	fn poll_flush(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		Poll::Ready(
			ready!(self.stream.poll_flush_unpin(cx))
				.or_else(|e| {
					if let Some(source) = e.source()
						&& let Some(r) = source.downcast_ref()
						&& matches!(r, tungstenite::Error::ConnectionClosed)
					{
						// for whatever reason tarpc tries to flush the connection
						// even after it is notified that it has been closed.
						Ok(())
					} else {
						Err(e)
					}
				})
				.map_err(Error::from),
		)
	}

	fn poll_close(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Result<(), Self::Error>> {
		// TODO: set proper close code/reason
		self.stream.poll_close_unpin(cx).map_err(Error::from)
	}
}

impl Stream for TransportWebsocket {
	type Item = Result<Vec<u8>, Error>;

	fn poll_next(
		mut self: Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
	) -> std::task::Poll<Option<Self::Item>> {
		let item = match ready!(self.stream.poll_next_unpin(cx)) {
			None => return Poll::Ready(None),
			Some(item) => item?,
		};

		match item {
			Message::Text(_) => Poll::Ready(Some(Err(Error::ReceivedText))),
			Message::Binary(buffer) => Poll::Ready(Some(Ok(buffer))),
			Message::Ping(_) | Message::Pong(_) => {
				// we are not interested in these messages. immediately poll for the next one.
				self.poll_next(cx)
			},
			Message::Close(_) => Poll::Ready(None),
		}
	}
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
	#[error(transparent)]
	Internal(#[from] axum::Error),

	#[error("the websocket has received a string, but only array buffers are supported")]
	ReceivedText,
}
