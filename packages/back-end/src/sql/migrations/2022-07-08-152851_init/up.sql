-- main app data --

create table "course" (
	"id"             UUID  not null  primary key,
	"name"           text  not null,
	"description"    text  not null,
	"cover_image"    text  not null
);

create table "lesson" (
	"id"            UUID      not null  primary key,
	"course"        UUID      not null  references "course",
	"name"          text      not null,
	"content"       text      not null,

	"course_order"  smallint  not null
);

-- ugly hacks --

create table "lesson_user_state" (
	"user"       UUID     not null,
	"lesson"     UUID     not null  references "lesson",
	"completed"  boolean  not null,

	primary key ("user", "lesson")
);

-- some sample data --

insert into course("id", "name", "description", "cover_image")
values
	('b34fddb9-5d98-46b6-9bc7-6baeba51309b', 'React basics', 'Learn the basics of React, one step at a time', 'https://images.unsplash.com/photo-1516116216624-53e697fedbea?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1228&q=80'),
	('6cbe9315-ac7f-424e-902e-15b61a5d2004', 'Lights out', 'Getting bored yet? Worry not. In this course, we''ll build a fun simple game to sharpen your newly acquired skills!', 'https://i.imgur.com/e9SPTlT.png')
on conflict (id) do update set
	"name"        = EXCLUDED."name",
	"cover_image" = EXCLUDED."cover_image";

insert into lesson("id", "course", "course_order", "name", "content")
values
	('9b2df6f5-31ba-4e5d-9dff-4fe93bb0d61b', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 00, 'Introduction', $$
= Introduction

== What is React?

* React is a JavaScript library for building user interfaces
* It was created by Facebook
* It is used for the development of single-page web applications
* In React, you can split UI into smaller,  reusable "components"

But you don't care about that, you are here to learn how to *use* React.

Before we get started though, we need to know *where* to write React.

== Getting Started with React

First, you need to install https://nodejs.org/en/[Node.js] if you haven't done so already.

Open a code editor of your choice. Open the terminal and type:

[source, shell]
----
npx create-react-app react-demo
----

* *npx* is a tool that comes with Node.js
* *create-react-app* is a command that creates a React environment
* *react-demo* is the name of the directory in which the environment will be created, you can change it to whatever you want

This command creates a directory for our app optimised for development.

You can now lauch the app by navigating to the project directory:

[source, shell]
----
cd react-demo
----

and typing the following command:

[source, shell]
----
npm start
----

The app should now automatically open in your browser.

$$),
	('70a87abc-c0d4-4080-b308-22991ea4449f', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 01, 'React Basics', $$

== React Basics

If you open the App.js file in the src directory of our React app, you will see this code:

[%interactive, source, ts]
----
import * as React from 'react';

// We have commented the import statement below, otherwise this editable window would't work on our site,
// but you can leave it uncommented if you are working on your own machine.
//import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
----

If you launch the app with "npm start", what will be displayed on screen will match the code inside the return() statement of the App Component.

We want to make our own page, so let's change the content.

Delete the contents of the return() statement and replace it with a <h1> element, like so:

[source, ts]
----
function App() {
  return (
    <h1>My first line of React code!</h1>
  );
}
----

If you reload the page now, the changes will be applied automatically.

== Returning Multiple Elements

Let's add some <p> elements so the page doesn't look so empty.

[source, ts]
----
function App() {
  return (
    <h1>My first line of React code!</h1>
    <p>That wasn't so hard.</p>
    <p>Almost a pro.</p>
  );
}
----

Let's reload the page and... wait, what's this?!

The code doesn't work. That's because React elements always need to be wrapped in a parent element. So when you want to return multiple elements, always put them in a "box" of sorts with another element, like a <div> for example. If you cannot put your elements into a parent element in a way that would make sense, you can put them into an empty element, which are called fragments:

[source, ts]
----
function App() {
  return (
    <>
        <h1>My first line of React code!</h1>
        <p>That wasn't so hard.</p>
        <p>Almost a pro.</p>
    </>
  );
}
----

== Returning Single Tag Elements

But there is still something missing on our page... It's pictures of cute animals! *Obviously*. Cute animals sell, so let's add some to our page. Download an image of your favourite pet, add it to the src directory and add it to the page.

[source, ts]
----
function App() {
  return (
    <div>
        <h1>My first line of React code!</h1>
        <p>That wasn't so hard.</p>
        <p>Almost a pro.</p>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200">
    </div>
  );
}
----

But this doesn't work _again_. That's because in React, you need to close single tag elements with a forward slash, like so: *<img />*. So this should work:

[source, ts]
----
function App() {
  return (
    <div>
        <h1>My first line of React code!</h1>
        <p>That wasn't so hard.</p>
        <p>Almost a pro.</p>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
    </div>
  );
}
----

== Adding Styling

What if we wanted to make our site look less appaling. What if we wanted to add some flavor to it, some... *style*.

Simple, you might think, let's add a class attribute to our HTML elements and create css code for our site. But there is a problem. The HTML elements we have been writing so far are not actually HTML code. They are JavaScript code disguised as HTML code so it is easier to read. That means you have to keep in mind that you have not been writing HTML code into those return() statements, it is still JavaScript, and since JavaScript is an object-oriented language (you split code into classes), Class is a reserved keyword in JavaScript. Therefore, React uses ClassName as a substitude for the Class attribute of HTML elements.

If you wanted to style your code, you could do something like this:

[source, ts]
----
function App() {
  return (
    <div className='stylish-text'>
        <h1>My page has style!</h1>
    </div>
  );
}
----

== Quiz

[quiz]
====

[question]
Which console command creates a react app?

[answers]
* [ ] npx build-react-app app-name
* [ ] npm create-react-app app-name
* [x] npx create-react-app app-name
* [ ] npm create app-name

[question] 
Which console command starts your react app?

[answers]
* [ ] npx start-react-app
* [ ] npm start-react-app
* [ ] npx start
* [x] npm start
 
[question]
--
Why won't the following code work?

[source, ts]
----
function App() {
  return (
    <h1>My first line of React code!</h1>
    <p>That wasn't so hard.</p>
    <p>Almost a pro.</p>
  );
}
----
--

[answers]
* [ ] There should be a semicolon after each html element
* [ ] The code will work
* [x] Multiple html elements to return should be wrapped in a "parent" element, like a <div>
* [ ] The App() function is missing an input parameter of App(State state)

[question]
Choose the correct way to display a single tag element in React

[answers]
* [x] <img src='image.jpg'/>
* [ ] <img src='image.jpg'>
* [ ] <img src='image.jpg'>alternative text</img>

[question] 
--
Display the greeting in the h1 element.

[source, ts]
----
const greeting = "Hello there!";

function App() {
  return (
    <h1>[Statement to fill in]</h1>
  );
}
----
--

[answers]
* [x] {greeting}
* [ ] [greeting]
* [x] Hello there!
* [ ] {const greeting}

[question]
Choose the correct name for the class attribute in React for styling

[answers]
* [ ] class
* [x] className
* [ ] classname
* [ ] ClassName
====

$$),('c64dfe83-b52c-42d8-9905-ff9c4126a449', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 02, 'Components', $$

== Intro

The App() function that we have been writing into so far is what we call a Component. We know this is a Component because it returns HTML elements that should be displayed on screen.

[%interactive, source, ts]
----
import * as React from 'react';
//import './App.css';

function App() {
  return (
    <>
      <h1>Animal name: Bongo</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
    </>    
  );
}

export default App;
----

But isn't this a saddening sight. We have only one cute animal on our page! The more animals, the better. So let's add some more.

[source, ts]
----
function App() {
  return (
    <>
      <h1>Animal name: Bongo</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      <h1>Animal name: Stacy</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
    </>
  );
}
----

If you wanted to make your page really appealing and popular, you would keep adding more cute animals... and more... and more... But that is so boring! Do we really need to copy paste this code and change parts of it every time we want to reuse it? Well obiously *not*, since we are talking about it. And if you remember, at the beginning of this course, we mentioned that one of the main features of React is splitting UI into smaller, *reusable* "Components".

== Creating Components

So let's create our own component. Write a new Animal component somewhere in the App.js file, like so:

[source, ts]
----
import './App.css';

function App() {
  return (
    <>
      <h1>Animal name: Bongo</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      <h1>Animal name: Stacy</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
    </>    
  );
}

function Animal() {
  return (
    <>
      <h1>Animal name: Bongo</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
    </>
  );
}
export default App;
----

Now we can use our Animal component instead of copy pasting the same code over and over again.

[source, ts]
----
import './App.css';

function App() {
  return (
    <>
      <Animal/>
      <Animal/>
    </>  
  );
}

function Animal() {
    return (
      <>
        <h1>Animal name: Bongo</h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      </>    
    );
}
export default App;
----

== Props

Even though this made our code more efficient and easier to read, we still have one issue. All our animals have the same name! We need to find a way to tell our animal component what name should it display for different animals. And for that we will use Props(aka properties). We can add custom attributes to our html elements through which we can pass values to our Components. Let's add such an attribute to our Animals and pass their names through them.

[source, ts]
----
import './App.css';

function App() {
  return (
    <>
      <Animal name="Bongo"/>
      <Animal name="Stacy"/>
    </>  
  );
}

function Animal(props) {
    return (
      <>
        <h1>Animal name: {props.name}</h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      </>        
    );
}
export default App;
----

== Quiz

[quiz]
====
[question]
What is the purpose of Components?

[answers]
* [ ] Effectively separating websites into individual, accessible, responsive pages
* [ ] They enhance CSS styling capabilities
* [x] Separating UI into smaller, reusable, easier to manage parts
 
[question]
--
Choose the correct statement that will display the Greeting component:

[source, ts]
----  
 function App() {
  return (
    [Statement to fill in]
  );
}

function Greeting() {
    return (
      <h1>Hello There!</h1>
    );
}
----
--

[answers]
* [x] <Greeting/>
* [ ] {Greeting}
* [ ] [Greeting]
* [ ] (Greeting/)

[question]
--
Choose the correct statement that will display the greeting passed through the string attribute:

[source, ts]
----
 function App() {
  return (
    <Greeting string="Hello There!"/>
  );
}

function Greeting(props) {
    return (
      <h1>[Statement to fill in]</h1>
    );
}
----
--

[answers]
* [ ] props.string
* [ ] props.greeting
* [x] {props.string}
* [ ] {props.greeting}
====
$$),('1cbac46c-f62c-4b32-a58c-d312fb8ee426', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 03, 'Event Handling', $$

== Event Handling

Handling events in React works the same as handling events in JavaScript HTML DOM elements, with a few differences.

Firstly, as with the classname HTML attribute, we write events using the camelCase syntax. So, if we wanted to tell a button what it should do when clicked, we would write it like this:

[source, ts]
----
<button onClick={Do Stuff}>
   Click Me!
</button>
----

And how do we tell the button what to do? We put a function into the event handler, like so:

[source, ts]
----
<button onClick={() => console.log("I got clicked!")}>
   Click Me!
</button>
----

Note: We use the JavaScript https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions[Arrow funtion syntax] in the example above.

We can also pass arguments to the event handling funtion. What if, for example, we had a text input, and we wanted to work with the text every time it gets changed by the user.

[source, ts]
----
   <input
      placeholder="Write your name here"
      onChange={(event) => {
         Console.log(event.target.value);
      }}
   />
----

Let's try making our Animal component a little interactive. Let's give our animals the ability to make a noise.

Let's add a paragraph to our Animal component, where we will display some text when a button is clicked, and add said button as well.

[%interactive, source, ts]
----
import * as React from 'react';
//import './App.css';

function Animal(props) {
    return (
      <>
        <h1>Animal name: {props.name}</h1>
        <button onClick={() => document.getElementById('bark').innerHTML = "Woof!"}>
           Click Me!
        </button>
        <p id='bark'></p>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      </>        
    );
}

function App() {
  return (
    <Animal name="Bongo"/>
  );

}

export default App;
----

Great! Now all our animals bark, which isn't very realistic, but it works as an introduction to event handling.

== Quiz

[quiz]
====
[question]
--
Choose the correct function that runs when a button is clicked:

[source, ts]
----
<button [Statement to fill in]={Do Stuff}>
   Click Me!
</button>
----
--

[answers]
* [ ] onClicked
* [x] onClick
* [ ] OnClick
* [ ] onClickHandler

[question]
--
Choose the correct statement to print out text from the text input:

[source, ts]
----
<input
    placeholder="put name here"
    value={name}
    onChange={(event) => {
        setName([Statement to fill in]);
    }}
/>
----
--

[answers]
* [x] event.target.value
* [ ] event.value
* [ ] target.value
* [ ] name.value
====

$$),('ff9dc223-04d0-418b-880d-e4a0808a1645', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 04, 'State', $$

== Introduction to State

You now have a neverending horde of cute animals, so you launch the app to see them in all their adorable glory. But when you launch the app, you decide that you don't like the name or image of some of the animals. So, you decide to add a feature where you could change the animals on the fly.

Let's create a simple input field where we can change our animal's name:

[source, ts]
----
function Animal(props) {
  return (
    <>
      <h1>Animal name: //TODO</h1>
      <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      <span>Put name here: </span>
      <input
      placeholder="put name here"
      value={props.name}
      onChange={(event) => {
         //TODO
      }}
      />
    </>        
  );
}
----

Creating an input field isn't so bad, but how do we change the animal's name?

We cannot use the same approach as with the button in the previous lesson because remember, we want to have multiple animals with different names, but all of the elements where the name belongs would have the same ID. You can see for yourself:

[source, ts]
----
function Animal(props) {
  return (
    <>
        <h1>Animal name: <span id="name">{props.name}</span></h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
        <span>Put name here: </span>
        <input
            placeholder="put name here"
            onChange={(event) => {
            document.getElementById('name').innerText = event.target.value
            }}
        />
    </>        
  );
}
----

So... we need to assign a unique name to every animal and we need to give every animal the ability to change it's own name. If you have experience with object-oriented programming, you can probably guess the solution to our problem based on the title already.

Yes, we will implement State to our animal Component.

But... what is State?

== Implementing State

State are variables that belong to an instance of a Component. What does that mean, you say? Let's look at an example.

Here, we have two instances of the Animal Component:

[source, ts]
----
function App() {
  return (
    <>
      <Animal name="Bongo"/>
      <Animal name="Stacy"/>
    </>  
  );
}
----

One with the name Bongo and one with the name Stacy. We want both of them to keep their names saved somewhere, so they can change it later, and we also want them to have access to only their name alone, so they won't change the other one's name. That's what State will give us.

Let's implement it into our Animal Component:

[source, ts]
----
function Animal(props) {
    return (
      <>
        <h1>Animal name: {props.name}</h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      </>
    );
}
----

First, we need to write this import statement at the top of our file.

[source, ts]
----
import { useState } from 'react';
----

Then, we need to declare a State variable. Let's call our variable "name":

[source, ts]
----
function Animal(props) {
const [name, setName] = useState(props.name);

    return (
      <>
        <h1>Animal name: {props.name}</h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
      </>
    );
}
----

Lets'a take a closer look at this command.

In the square brackets [name, setName], we declare a name for our variable ("name" in our case) and a function which we will use to change the value of our variable (We named it "setName", but you can name it whatever you want. However, it should always be named with "set" and whatever you want to set, that's a convention.). The useState command creates our state variable and gives it an innitial value (in our case, the name we pass to the Component through props).

We want to display our name variable instead of props now, so we can't replace it:

[source, ts]
----
<h1>Animal name: {name}</h1>
----

Now, we can put our input field back:

[source, ts]
----
function Animal(props) {
const [name, setName] = useState(props.name);

    return (
      <>
        <h1>Animal name: {name}</h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
        <input
            placeholder="put name here"
            value={name}
            onChange={(event) => {
            //TODO - change name
            }}
        />
      </>
    );
}
----

And finally, let's try to change the name variable in the onChange() function:

[source, ts]
----
onChange={(event) => {
    name = event.target.value;
  }
}
----

Now, let's try to launch the app and change the animal's name.

Unfortunately, this doesn't work, and here's why.

== Correctly modifying State

When React displays something on the web page, React doesn't refresh it, except under very specific conditions. When we change the State variable "name" the way we did, we technically did change it, but React didn't refresh the Component. When React Components change on the fly, they need to be displayed anew = a new version of the Component needs to be rendered instead of the old one. But we need to somehow tell React when to re-render Components.

Thankfully, there is a way to tell React to re-render Components with State.

All we have to do is use the setName function that we created with the State variable and that's it.

[source, ts]
----
onChange={(event) => {
    setName(event.target.value);
  }
}
----

Our Animal "app" should now look like this:

[%interactive, source, ts]
----
import * as React from 'react';
//import './App.css';
import { useState } from 'react';

function App() {
  return (
    <>
      <Animal name="Bongo"/>
      <Animal name="Stacy"/>
    </>  
  );
}

function Animal(props) {
const [name, setName] = useState(props.name);

    return (
      <>
        <h1>Animal name: {name}</h1>
        <img src='https://images.freeimages.com/images/large-previews/e57/bonnie-2-1637745.jpg' width="200" height="200"/>
        <input
            placeholder="put name here"
            value={name}
            onChange={(event) => {
            setName(event.target.value);
            }}
        />
      </>
    );
}

export default App;
----

== Good to know

Before we finish with this lesson, there are some things to look out for when it comes to State.

Firstly, React re-renders only those parts of a Component instance that need to be re-rendered. For example, if you had a Component with two State variables, and you ran a setState function on one of them, that is the only part of the Component that gets re-rendered. The rest stays the same. Neat.

Another thing to keep in mind is that React does not update State and Props at the same time. Therefore, you should NOT perform any calculations with the two while setting a State variable, like so:

[source, ts]
----
setNumber1(number1 + props.number2)
----

Because the variables might not be set in the order you might want, so the calculation might be false.

Instead, if you want to perform calculations in you setter funtions, put them in a function, like this:

[source, ts]
----
setNumber1((number1) => (number1 + props.number2))
----

== Quiz

[quiz]
====
[question]
What is state?

[answers]
* [x] A variable that belongs to an instance of a Component
* [ ] State points to the line of code that is currently being executed
* [ ] State is the event handler for a stateful event

[question]
Why do you have to set a State variable with the setter function (setName for example)?

[answers]
* [ ] Because it makes code cleaner and prettier to look at
* [ ] Because it is the fastest and most efficient way of setting a State variable
* [x] Because it forces Components to re-render

[question]
How does React re-render Components?

[answers]
* [x] It re-renders only those parts of a Component that need to be re-rendered
* [ ] It re-renders the entire Component to make sure everything is in order

[question]
Is it safe to perform calculations between State and Props when setting a new value for a State variable?

[answers]
* [ ] No, because new values might not be assigned to both of them at the same time
* [ ] Yes, it is even required in some cases
* [x] Yes, it is if you perform the calculation within a function
====

$$),('289d7e40-e411-4a20-b3da-87d6c0ea20a0', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 05, 'Conditional rendering', $$

== Conditional Rendering

You don't always want to show the entire web page to the user. Depending on certain _conditions_, you might want to hide or show certain elements or Components to the user.

Let's say we have a Dog Component, and we want to show a different dog to the user depending on if the user has a snack for the dog or not.

== How to render pieces of a page

The easiest way we you can accomplish this is with simple if statements.

[%interactive, source, ts]
----
import * as React from 'react';
//import './App.css';

function Dog(props) {
    
    if (props.userHasSnack) {
        return(
            <>
                <h1>I am a happy dog!</h1>
                <img src="https://images.freeimages.com/images/large-previews/8b7/pure-joy-1358099.jpg" width="200" height="200"/>
            </> 
        )
    } else {
        return(
            <>
                <h1>I am a sad dog!</h1>
                <img src="https://images.freeimages.com/images/large-previews/637/sad-dog-1604766.jpg" width="200" height="200"/>
            </>
        )
    }
}

function App() {
    return(
        <Dog userHasSnack="false" />
    );
}

export default App;
----

You can also put elements and Components into variables to make your code easier to read.

[source, ts]
----
function Dog(props) {
    let dogToShow;
    
    if (props.userHasSnack) {
        dogToShow = (
            <>
                <h1>I am a happy dog!</h1>
                <img src="https://images.freeimages.com/images/large-previews/8b7/pure-joy-1358099.jpg" width="200" height="200"/>
            </>
        );
    } else {
        dogToShow = (
            <>
                <h1>I am a sad dog!</h1>
                <img src="https://images.freeimages.com/images/large-previews/637/sad-dog-1604766.jpg" width="200" height="200"/>
            </>
        );
    }

    return(
        dogToShow;
    )
}

function App() {
    return(
        <Dog userHasSnack="false" />
    );
}
----

You may also use conditional statements in return statements between other elements, if you put them inside curly braces, like so:

[source, ts]
----
function Dog(props) {

    return(
        <h1>Dog</h1>
        {if (!props.userHasSnack){<p>Dog makes angry noises</p>}}
        <img src="https://images.freeimages.com/images/large-previews/637/sad-dog-1604766.jpg" width="200" height="200"/>
    )
}

function App() {
    return(
        <Dog userHasSnack="false" />
    );
}
----

Or, you can use the shortened version of if-else:

[source, ts]
----
function Dog(props) {
    return(
        <h1>Dog</h1>
        <p>Dog is: {props.userHasSnack ? "Happy" : "Sad"}</p>
        <img src="https://images.freeimages.com/images/large-previews/637/sad-dog-1604766.jpg" width="200" height="200"/>
    )
}

function App() {
    return(
        <Dog userHasSnack="false" />
    );
}
----

== How to render nothing at all

If you do not want to show a Component to the user, you can return null in the return statement.

So if the user doesn't have a snack for our dog, it won't even bother showing up to the user.

[source, ts]
----
function Dog(props) {
    if (!props.userHasSnack) {
        return null;
    }
    
    return(
        <h1>Dog</h1>
        <p>Dog is happy</p>
        <img src="https://images.freeimages.com/images/large-previews/8b7/pure-joy-1358099.jpg" width="200" height="200"/>
    )
}

function App() {
    return(
        <Dog userHasSnack="false" />
    );
}
----

== Quiz

[quiz]
====
[question]
Can you save Components into variables?

[answers]
* [x] Yes, you can
* [ ] No, you can't

[question]
--
Can you put if statements and other JavaScript statements into Component return statements between React elements, like this: ?

[source, ts]
----
function Component(props) {
    return(
        <h1></h1>
        if (props.true) {<p>First paragraph</p>}
        <p>Second paragraph</p>
    )
}
----
--

[answers]
* [ ] Yes, you can
* [x] Yes, but you have to put them inside curly braces
* [ ] No, you can't

[question]
Can a Component return null?

[answers]
* [x] Yes, it can, if i don't want the Component to display anything
* [ ] No, it can't. It has to return at least an empty string.
====

$$),('6b93edd9-47a8-4c71-9257-d486c645296e', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 06, 'Lists and Keys', $$

== Lists

Let's imagine for a second that we are managers of a pet shop.

Every week, new animals arrive to our shop and we want to display a list of them on our website to the customer, so he can pick and choose which one he wants.

[source, ts]
----
function PetList(props) {
    let dogs = ["Stacy", "Merci", "Dante", "Alexis"];
    
    return(
    )
}
----

== Map() function

Now, surely you have ideas about how you could diplay a list of the contents of the array to the user, but we will use a method that is most likely easier and also required in React for reasons that will be explained later.

We will use the javascript map() function.

This function goes through the entire array and does whatever we specify to every element of the array. Then, it returns a new array with the changes applied to every element.

For example, we can tell the map() function to create a <li> element for every dog name in our dogs[] array and then save this modified version of the dogs array to a new array, dogList.

[source, ts]
----
function PetList(props) {
    let dogs = ["Stacy", "Merci", "Dante", "Alexis"];
    let dogList = dogs.map( (dogName) => <li>{dogName}</li> );
    
    return(
    )
}

function App() {
    return(
        <PetList/>
    );
}
----

Then, we can return these <li> elements as a list like so:

[%interactive, source, ts]
----
import * as React from 'react';
//import './App.css';

function PetList(props) {
    let dogs = ["Stacy", "Merci", "Dante", "Alexis"];
    let dogList = dogs.map( (dogName) => <li>{dogName}</li> );
    
    return(
        <ul>{dogList}</ul>
    )
}

function App() {
    return(
        <PetList/>
    );
}

export default App;
----

== Do not forget Keys

There is one issue with our code, however...

In fact, React might even give you a warning about it.

When you create arrays of elements in React, every element needs a Key. In our dogs.map() function, we are creating an array of <li> elements and returning it to the dogList.

A Key is a special attribute that allows React to track changes to a list of elements.

We can try assigning the dogs array's index as a key for our elements:

[source, ts]
----
function PetList(props) {
    let dogs = ["Stacy", "Merci", "Dante", "Alexis"];
    let dogList = dogs.map( (dogName) =>
        <li key={index}>
            {dogName}
        </li>
    );
    
    return(
        <ul>{dogList}</ul>
    )
}
----

This is what React does by default if you don't specify a key.

But if React does this by default, this won't fix our problem, will it?

That's right, it won't. But what is the issue with having indexes as keys in the first place? Let's take a look.

Let's say we have a list with the following indexes:

----
Stacy, Merci, Dante, Alexis
0      1      2      3

Displays:
*Stacy
*Merci
*Dante
*Alexis
----

If we add a new name at the end of the list:

----
Stacy, Merci, Dante, Alexis, Hachiko
0      1      2      3       4

Displays:
*Stacy
*Merci
*Dante
*Alexis
*Hachiko
----

Everything looks good so far. But what if we modify the list in the middle? Let's delete Alexis from the list.

----
Stacy, Merci, Dante, Hachiko
0      1      2      3

Displays:
*Stacy
*Merci
*Dante
*Alexis
----

React deletes Hachiko instead of Alexis, but why? Because React tracks the changes made to the keys in a list.

----
Stacy, Merci, Dante, Alexis, Hachiko
0      1      2      3       4
Stacy, Merci, Dante, Hachiko
0      1      2      3
----

That's why React keeps Alexis and deletes Hachiko even though that's not what we want. It sees that we kept key 3 in the same place but got rid of key 4, so it modified the list accordingly. It doesn't care about the contents of the list elements.

Because the reason shown above, it is not a good idea to use array indexes as list keys if you expect the list to change in any way(If you want to add, move or remove elements from the list). You can still use indexes as keys if you don't expect your list to change though.

But what if you want to change your list, what are you supposed to use as keys then?

Well, you need to pick a key that will be unique among every element of the same list, even if you modify the list.

If you get the data for yout list from a database, you already have a good pick - the column's ID.

== Quiz

[quiz]
====
[question]
What function would you use to transform an array into a list of React elements?

[answers]
* [ ] assign()
* [ ] transform()
* [x] map()

[question]
We want to create a shopping cart for our React app. Our app will allow users to add and remove products from the cart. We will display the products in the shopping cart by transforming an array of products into a list of elements to display. Is it okay to set the array indexes as keys for the new list of elements?

[answers]
* [ ] yes
* [x] no
====

$$),('dfc2415f-df9e-4a1d-a971-ef7c16d3ac0f', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 07, 'Forms', $$

== Forms

What if the list of animals in our pet shop wasn't enough. What if we wanted to add new animals to the list ourselves? We would naturally need some sort of form for that. And that's what we will learn about in this chapter.

Throughout this chapter we will build a simple form for this purpose.

[%interactive, source, ts]
----
import * as React from 'react';
//import './App.css';
import { useState } from 'react';

function PetForm(props) {
    return(
        <form>
            <label>Animal name:
                <input type="text" />
            </label>
        </form>
    )
}

function App() {
    return(
        <PetForm/>
    );
}

export default App;
----

== Submit a form

Now we need some way to process the data we submit to the form. In React, forms are handled by React itself. We just need to tell our PetForm Component what to do with the submitted data. We cand do all this in a handler function.

Let's create such a funtion and set it as the handler of our form.

[source, ts]
----
function PetForm(props) {
    const handleSubmit = (event) => {

    }

    return(
        <form onSubmit={handleSubmit}>
            <label>Animal name:
                <input type="text" />
            </label>
        </form>
    )
}
----

HTML forms send you to a new page by default. Since our form's data is handled by the Component itself and since we are building single-page applications with React, we want to disable this behavior by adding this line to our handler function:

[source, ts]
----
    const handleSubmit = (event) => {
        event.preventDefault();
    }
----

== Managing form input state

When we write some data into a HTML form, the form elements keep track of the changes made by managing their own state. However, HTML elements in React work a little differently. We need to manage their state ourselves. You already might have some idea how to do this from the chapter about State.

We need to create a state variable where the animal's name will be kept and a handler function that will save the animal's name into the state variable every time it is changed in the input field.

Let's implement state to our form.

[source, ts]
----
function PetForm(props) {
    const [name, setName] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
    }
    
    const handleChange = (event) => {
        setName(event.target.value);
    }
    

    return(
        <form onSubmit={handleSubmit}>
            <label>Animal name:
                <input type="text" value={name} onChange={handleChange}/>
            </label>
        </form>
    )
}
----

Since we need to maintain the value of form intput fields with React, textarea and select input fields would not work, because we would have no way of accessing textarea's content and select's selected property.

Therefore, they work a little differently in React.

== Textarea

Textarea has a value property as well, which allows us to access it's content:

[source, ts]
----
function PetForm(props) {
    const [text, setText] = useState("");
    
    const handleChange = (event) => {
        setText(event.target.value);
    }
    

    return(
        <form>
            <textarea value={text} onChange={handleChange}/>
        </form>
    )
}
----

== Select

And select also has a value property, which tells us which option is currently selected.

[source, ts]
----
function PetForm(props) {
    const [selected, setSelected] = useState("cat");
    
    const handleChange = (event) => {
        setSelected(event.target.value);
    }
    

    return(
        <form>
            <select value={selected} onChange={this.handleChange}>            
                <option value="dog">Dog</option>
                <option value="cat">Cat</option>
                <option value="hamster">Hamster</option>
          </select>
        </form>
    )
}
----

== Handling multiple input fields

We will naturally have multiple input fields in a form at some point. We might create a handler function for each of them, but it is easier to have one function handle them all. If we want to do this however, we need to differentiate between the input fields, so we know what state variable to change in the handler function. We use the name attribute for this purpose.

[source, ts]
----
function PetForm(props) {
    const [inputs, setInputs] = useState({});

    const handleSubmit = (event) => {
        event.preventDefault();
    }
    
    const handleChange = (event) => {
        setInputs([event.target.name] : event.target.value);
    }
    

    return(
        <form onSubmit={handleSubmit}>
            <label>Animal name:
                <input type="text" name="name" value={inputs.name || ""} onChange={handleChange}/>
            </label>
            <label>Animal type:
                <select name="selected" value={inputs.selected || "dog"} onChange={this.handleChange}>            
                    <option value="dog">Dog</option>
                    <option value="cat">Cat</option>
                    <option value="hamster">Hamster</option>
                </select>
            </label>
        </form>
    )
}
----

== Quiz

[quiz]
====
[question]
How do we access the content of the Textarea element and the selected value of the Select element?

[answers]
* [ ] With Textarea's "content" property and Select's "selected" property
* [ ] With Textarea's getContent() method and Select's getSelected() method
* [x] With their "value" property

[question]
How does React keep track of changes made in form's input?

[answers]
* [ ] React does it automatically
* [x] React doesn't do it, we have to do it ourselves with state
* [ ] Form elements keep track of their own state themselves, like in HTML

[question]
If we want to build single-page applications with React, how do we make our forms NOT send the user to a new page on submit?

[answers]
* [x] We put event.preventDefault() into the form's handler method
* [ ] We put preventDefault into our application's config file
* [ ] We put action="preventDefault" as an attribute of our form

[question]
How do we differentiate between multiple input fields in a form when using a single handler function for the entire form?

[answers]
* [x] We use the input field's "name" attribute
* [ ] We use the input field's "id" attribute
* [x] We can use event.target.type to determine the type of input field
====
$$),('69b7daea-0e3e-41ec-b536-0ce0ecb86ef7', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 08, 'Hooks', $$

== What are Hooks?

Throughout the tutorial, we have been writing Components as functions. This is the preffered way of writing Components today. In the past however, you couldn't use state in function Components, and so the preffered way of writing Components used to be to write them as JavaScript classes.

That was before Hooks though. They allow us to use features of JavaScript classes with function Components.

You are already familiar with one Hook, the useState Hook, which you learned about in the chapter about State.

== How to use Hooks

If you want to use a Hook, you need to import it, like so:

[source, ts]
----
import React, { useState, useEffect } from 'react';
----

Before using any Hooks, there are two important rules that you need to keep in mind.

* You should call Hooks only from the highest level in you function Components. Do not put them into conditions, loops or nested functions.

* You should call Hooks only from function Components and custom Hooks (yes, you can build your own Hooks), not regular functions.

Now that we know how to use Hooks properly, let's look at some elementary Hooks (aside useState).

== useEffect

React executes code inside a Component every time said Component is rendered. What if we wanted to perform some actions outside of that, let's say after or before a Component re-renders? These actions are called side-effects in React and there is a Hook for them, useEffect.

If we wanted to run some code after a Component renders, we would write this:

[source, ts]
----
import React, { useEffect } from 'react';

function RandomComponent(props) {

  useEffect(() => {
    //This code runs after the Component renders
  }
  
  return (
  );
}
----

To run code before a subsequent re-render, put it into a return statement inside the useEffect Hook.

[source, ts]
----
import React, { useEffect } from 'react';

function RandomComponent(props) {

  useEffect(() => {
    //This code runs after the Component renders
    return () => {
      //This code runs before the next re-render
    };
  }
  
  return (
  );
}
----

== useContext

Let's say we have a TopComponent with a state variable. We would like to use this variable not only in our TopComponent, but also in a Component rendered by our TopComponent, the LowerComponent. If we wanted to achieve this, we would pass it down to the LowerComponent through props. 

[source, ts]
----
import React, { useState } from 'react';

function TopComponent(props) {
    const [name, setName] = useState("");
  
    return (
        <LowerComponent name={name}>
    );
}
----

But what if LowerComponent rendered an EvenLowerComponent which also needed the value of the name variable? Simple, we would pass it through props even lower.

Not too complicated so far, but imagine you would have to pass down this variable even lower and lower, until you got to the LowestComponent. Even worse, what if the only Components that would need the name variable were TopComponent and LowestComponent. Then you would unnecessarily pass the variable down through Components that do not need it in their props.

To solve this, we would need to find a way to make State global = allow any Component to access it, no matter if it was passed down to that Component's props.

The useContext Hook allows just that.

First, we need to import the createContext Hook and create context.

[source, ts]
----
import React, { useState, createContext } from 'react';

const NameContext = createContext();

function TopComponent(props) {
    const [name, setName] = useState("");
  
    return (
        <LowerComponent name={name}>
    );
}
----

Next, we need to create a context provider, which will allow us to use a state variable in lower Components without passing it down through props.

[source, ts]
----
import React, { useState, createContext } from 'react';

const NameContext = createContext();

function TopComponent(props) {
    const [name, setName] = useState("");
  
    return (
        <NameContext.Provider value={name}>
            <LowerComponent name={name}>
        </NameContext.Provider>
    );
}
----

Finally, we import the useContext Hook and use it in our LowestComponent.

[source, ts]
----
import React, { useState, createContext, useContext } from 'react';

function LowestComponent(props) {
    const name = useContext(NameContext);
  
    return (
        <h1>Dog's name is: {name}</h1>
    );
}
----

== useMemo

Imagine we have a Component that sorts and displays a list of animals. The Component gets the list through props.

[source, ts]
----
import React, { useMemo } from 'react';

function AnimalList(props) {
    const sortedList = sortAnimals(props.animals, props.order);
    const animalList = sortedList.map( (animalName) => <li>{animalName}</li> );
    
    return (
        <ul>{animalList}</ul>
    );
}
----

Our AnimalList Component will work. And if we sort dozens or hundreds of animals, it should work fast enough. However, what if we had to sort hundreds of thousands of animals? That might take a noticeable hit on the performance of our site.

That's what we have useMemo for. This Hook let's us store the result of a calculation in the cache, so we don't have to perform it on every render, if it isn't necessary.

UseMemo has two arguments. First, a function that returns the value we want to calculate. Second, the "dependencies", or the values which are used in the calculation.

[source, ts]
----
import React, { useMemo } from 'react';

function AnimalList(props) {
    const sortedList = useMemo(() => sortAnimals(props.animals, props.order), [props.animals, props.order]);
    const animalList = sortedList.map( (animalName) => <li>{animalName}</li> );
    
    return (
        <ul>{animalList}</ul>
    );
}
----

UseMemo performs the first calculation on the first render of the Component and stores the value in cache, so it doesn't need to be calculated again. With every subsequent render, it checks wether the values we have specified as dependencies have changed form the previous render. If any of them changed, useMemo will perform the calculation again and store it in cache. If they haven't changed, useMemo will not perform the calculation again and will return the value stored in cache.

== Keep in mind

A common mistake when using Hooks is to do something like this:

[source, ts]
----
function Sum(props) {
    if (props.numberOne <= 0 || props.numberTwo <= 0) {
        return <p>Numbers must be greater than zero</p>
    }

    const sum = useMemo(() => props.numberOne + props.numberTwo, [props.numberOne, props.numberTwo]);
    
    return (
        <p>The sum of entered numbers: {sum}</p>
    );
}
----

This Component returns the sum of two numbers, and because we don't want the value to be calculated on every render, we use useMemo to save the value to cache (In practice, using useMemo for calculating the sum of two numbers is overkill, it is not a difficult task to calculate). We also have an if statement in the Component that checks wheter the numbers have been set or not, and if not, returns a paragraph warning the user instead of returning the sum.

And that is where the probelm lies.

Every Hooks needs to run on every render. UseMemo needs to run on every render to see if any dependencies have changed, but if we return a paragrapg before useMemo runs, it will never be able to do so.

We need to have Hooks at the top of the Component, so they always run on every render, but we also do not want the Hook to run if the numbers are not greater than zero. We cannot have both at the same time.

A possible solution is to split the Component into two Components:

[source, ts]
----
function Sum(props) {
    if (props.numberOne <= 0 || props.numberTwo <= 0) {
        return <p>Numbers must be greater than zero</p>
    }

    return (
        <SumDisplay numberOne={props.numberOne} numberTwo={props.numberTwo}/>
    );
}

function SumDisplay(props) {
    const sum = useMemo(() => props.numberOne + props.numberTwo, [props.numberOne, props.numberTwo]);

    return (
        <p>The sum of entered numbers: {sum}</p>
    );
}
----

== Quiz

[quiz]
====
[question]
What is the purpose of React Hooks?

[answers]
* [ ] They give us the ability to manage memory usage of our React App
* [ ] They enable autoloading for required JavaScript classes
* [x] They enable us to use features of JavaScript classes in function Components

[question]
--
Why won't the following code work?

[source, ts]
----
import React, { useState } from 'react';

function ComponentOne(props) {
    if (props.setName) {
        const [name, setName] = useState("");
    }
  
    return (
        <ComponentTwo name={name || ""}>
    );
}
----
--

[answers]
* [ ] Because the value of props.setName has never been set
* [ ] Because the useState Hook does not accept an empty string as an input parameter
* [x] Because Hooks should only be used from the highest level in a component, and therefore, not in an if statement
* [ ] Because we cannot use the logical or (||) operator when setting the name prop of ComponentTwo

[question]
Where can we use Hooks?

[answers]
* [ ] In class Components
* [x] In function Components
* [ ] In class and function Components

[question]
What is the useEffect Hook used for?

[answers]
* [ ] To add visual effects to components when they are displayed on the page
* [x] To control the lifecycle of a Component. To declare what should happen before or after a Component re-renders
* [ ] To declare what Components have access to this Component's state, in other words, which Components can have "effect" on this Component

[question]
What is the Context Hook used for?

[answers]
* [x] To give lower Components in the Component tree access to this Component's state without passing it down through props
* [ ] To declare in which context should some of the Component's methods be used, or in other words, declare certain methods of a Component as event handlers for specific events on the page
* [ ] To manipulate metadata about a chosen Component

[question]
What is the useMemo Hook used for?

[answers]
* [ ] To reference a value that is not needed for rendering
* [ ] To sort a large array
* [x] To save the result of a complex calculation into cache to optimize performace of the site
====

$$),('91ac5592-b808-444b-8ef0-6f413293bab6', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 09, 'Final Quiz', $$

== Final Quiz

[quiz]
====
[question]
What command do you use to create a new React app from the terminal?

[answers]
* [ ] npx create-app react-demo
* [ ] npm create-app react-demo
* [x] npx create-react-app react-demo
* [ ] npm create-react-app react-demo

[question]
How do you return multiple elements

[answers]
* [ ] You put them in a <collection> tag
* [ ] With a returnMultiple() function
* [x] You put them into a parent element or a fragment

[question]
--
Choose the correct statement that will display the greeting passed through the string attribute:

[source, ts]
----
function App() {
  return (
    <Greeting string="Hello There!"/>
  );
}

function Greeting(props) {
    return (
      <h1>[Statement to fill in]</h1>
    );
}
----
--

[answers]
* [x] {props.string}
* [ ] [props.string]
* [ ] (props.greeting)
* [ ] [props.greeting]

[question]
What is the purpose of Components?

[answers]
* [ ] Effectively separating websites into individual, accessible, responsive pages
* [ ] They enable us to use features of JS classes in JS functions
* [x] Separating UI into smaller, reusable, easier to manage parts

[question]
--
Choose the correct statement to print out text from the text input:

[source, ts]
----
<input
    placeholder="put name here"
    value={name}
    onChange={(event) => {
        setName([Statement to fill in]);
    }}
/>
----
--

[answers]
* [ ] event.value
* [ ] target.value
* [x] event.target.value
* [ ] name.value

[question]
Why do you have to set a State variable with the setter function (setName for example)?

[answers]
* [ ] State variables can only be accessed with getter functions and changed with setter functions (getName and setName, for example)
* [x] Because it forces Components to re-render
* [ ] Because it allows us to manage memory usage and improve the performance of our app

[question]
Is it safe to perform calculations between State and Props when setting a new value for a State variable?

[answers]
* [x] Yes, it is if you perform the calculation within a function
* [ ] Yes, it is if you perform the calculation within a setter function
* [ ] No, because new values might not be assigned to both of them at the same time

[question]
Can you save Components into variables?

[answers]
* [x] Yes, you can
* [ ] Yes, you can, if you use the saveComponent Hook
* [ ] No, you can't

[question]
What do you put into the return statement if you do not want a Component to return anything at all?

[answers]
* [ ] <p></p>
* [ ] I simply put nothing in the return statement
* [x] The keyword "null"

[question]
What does the map() function do?

[answers]
* [x] It iterates through all elements in an array, does a specified action with them and returns a new array with the changes applied
* [ ] It enables us to make a state variable global and use it in Components lower down the hirearchy without passing the variable down through props
* [ ] It enabels us to increase performance while executing complex calculations

[question]
Let's say you want to create a list of elements out of an array. When is it ok to use the array's indexes as keys for the list?

[answers]
* [ ] When we expect elements to be removed from the list
* [x] When we do not expect the list of elements to change in any way
* [ ] When we expect the list of elements to be sorted

[question]
Does React keep track of changes made in form's input?

[answers]
* [Yes]
* [No]

[question]
How do we differentiate between multiple input fields in a form when using a single handler function for the entire form?

[answers]

* [ ] We use the input field's "id" attribute
* [ ] We use the input field's "key" attribute
* [x] We use the input field's "name" attribute

[question]
How do we access selected value of the Select element?

[answers]
* [x] With the "value" property
* [ ] With the "selected" property
* [ ] With the getSelected() method

[question]
How do we access selected value of the Select element?

[answers]
* [x] With the "value" property
* [ ] With the "selected" property
* [ ] With the getSelected() method

[question]
--
Choose which rule the following code breaks:

[source, ts]
----
function ComponentOne(props) {
    if (props.name === undefined) {
        return <p>you have no name!</p>
    }

    const initial = useMemo(() => props.name[0], [props.name]);

    return <p>Your initial: {initial}</p>
}
----
--

[answers]
* [ ] There can be only one return statement
* [ ] Hooks can never be used in an if statement
* [x] Hooks need to run on every render
* [ ] useMemo has only two arguments

[question]
Which Hook would you use if you wanted to clean up the connection to a database after you are done using a Component?

[answers]
* [ ] useReducer
* [x] useEffect
* [ ] useMemo
* [ ] useContext

[question]
Which Hook would you use if you wanted to increase the performance of your app when executing complex calculations?

[answers]
* [ ] useContext
* [ ] useInsertionEffect
* [ ] useImperativeHandle
* [x] useMemo

[question]
Which Hook would you use if you wanted a Component lower in the Component tree to have access to a state variable of a Component higher in the tree without passing the variable down throught props?

[answers]
* [x] useContext
* [ ] useCallback
* [ ] useId
* [ ] useRef
====

$$),('3d473049-b9f0-449a-b7cd-338f27b91033', 'b34fddb9-5d98-46b6-9bc7-6baeba51309b', 10, 'What now?', $$

== What now?
Congrats! You made it to the very end of this React course.

But... what now?

If you want to refresh your knowledge of React, you might take another React course on this site, which takes you step by step through a tutorial on how to build a simple and fun game with React. Cool!


$$),('bada9ae3-d475-4dd6-8206-202ac6c18254', '6cbe9315-ac7f-424e-902e-15b61a5d2004', 00, 'Introduction', $$

== Introduction

Ready to make your first react game? Alright let's get started. First of all we're gonna learn the fundementals of this game.

* We play on a grid
* The goal is to turn of all the lights
* We start with random number of lights on
* When we click on a light it and surrounding light changes state

== Creating project

Nothing special needed for this, so we are going to run this npx command.
----
npx create-react-app lights-out --template typescript
----
$$),('d4e1f1a0-51e1-4432-b510-29c410eabc0e', '6cbe9315-ac7f-424e-902e-15b61a5d2004', 01, 'Making the board', $$
== Making the board

First of all, we want to create new file. Since we are creating board let's call it `Board.tsx`.

Since we want to have a board, we will have to make two dimensional array. The size of the board can vary, so we want to make a variable. Also we need to controll how many lights are on, so let's make a variable for that too.

We will pass the two variables as props. Since we want to use this component in a different file, we need to expot it.
[source, jsx]
----
export const Board = ({ size, chance }: { size: number; chance: number }) => {

};
----

Now let's make a function for the gird itself.
[source, jsx]
----
const makeGrid = () => {
  return Array.from({ length: size }, () =>
     Array.from({ length: size }, () => {
       // generate a randomly enabled/disabled cell

      return Math.random() < chance;
    })
  );
};
const [grid, setGrid] = useState(makeGrid());
----

This may seem confusing at first but it's simple. As the name suggests, the function `Array.from()` makes an array for a given length. The first argument is the size of the board and the second one is an arrow function that has another `Array.from()` inside of it. And lastly, to set the `cell` on or off, we generate a random nuber and then compare it with the prop `chance` Let's look at an example. Say that `Math.random()` has given us a value of 0.4 and `chance` is 0.2. As we can see 0.4 is greater than 0.2 and the cell isn't turned on. Then we save the value to `useState()`.

Nice work, we have the working board. Now let's add html and css to is it in action.

First of all, another file. This time `Board.css` and let's add some css straight away.
[source, css]
----
.board {
  background-color: rgb(59, 59, 59);
  border-radius: 1.5rem;
  display: flex;
  flex-direction: column;
  gap: 2rem;
  padding: 2rem;
}

.row {
  display: flex;
  gap: 2rem;
}
----
These are styles for the board, now we need to add the cells.
[source, css]
----
.cell {
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 50%;
  border: none;
  transition: background-color 0.1s ease-out;
}
---- 

Now we need to separate when the cell is on and off.

[source, css]
----
.cellOn {
  background-color: rgb(255, 187, 0);
  box-shadow: 0 0 2.5rem rgb(255, 187, 0), 0 0 5rem rgb(255, 187, 0),
    0 0 6rem rgb(255, 187, 0);
}
.cellOff {
  background-color: rgb(80, 80, 80);
}
----

It wouldn't be complete without a title. And especially this great looking one. We want to have the title and the board in one container, so let's do it.
[source, css]
----
.container {
  display: flex;
  flex-direction: column;
  align-items: center;
}

.container,
h1 {
  font-family: "Bungee";
  font-size: 5rem;
}

.title {
  margin: 1rem;
  user-select: none;
}

.title1 {
  color: rgb(255, 187, 0);
  text-shadow: 0 0 2.5rem rgb(255, 187, 0), 0 0 5rem rgb(255, 187, 0);
}

.title2 {
  color: rgb(80, 80, 80);
}

----

And lastly some nice looking hover styles.

[source, css]
----
.cellOn:hover {
  background-color: rgba(255, 187, 0, 0.8);
}
.cellOff:hover {
  background-color: rgba(255, 187, 0, 0.1);
}
----
Now let's import the css file.
----
import "./Board.css"
----
File `Board.tsx` looks like this now. 
[source, jsx]
----
import "./Board.css"

export const Board = ({ size, chance }: { size: number; chance: number }) => {
  
  const makeGrid = () => {
    return Array.from({ length: size }, () =>
      Array.from({ length: size }, () => {
        // generate a randomly enabled/disabled cell

        return Math.random() < chance;
      })
    );
  };

  const [grid, setGrid] = useState(makeGrid());
  
  return (
    
  );
};
----

We are gonna add html to the `return()`.
[source, jsx]
----
<div className="container">
      <h1 className="title">
        <span className="title1">LIGHTS</span>
        &nbsp;
        <span className="title2">OUT</span>
      </h1>

      <div className="board">
        {grid.map((row, rowIndex) => (
          <div className="row" key={rowIndex}>
            {row.map((cell, columnIndex) => (
              /*cell*/
              <button
                className={"cell " + (cell ? "cellOn" : "cellOff")}
                key={columnIndex}
              ></button>
            ))}
          </div>
        ))}
      </div>
  </div>
  
----

Wow! That's a lot of code, but again it's not that complicated. We can see that there are two `maps()`, they are used to get inside of the two dimensional array. The inside map returns `button`, which is our cell or light, whether how you want to call it. Now even the `cellOn` and `cellOff` styles come handy as we apply them based on the cell's state.

This all looks great, so try and run this. Oh no! Why isn't it working? That's because we forgot to use `Board`, we are only creating it. Now how to fix it. 

In file `App.tsx` delete everything from `return()` and add our component `Board`.Don't forget to import it.

[source, jsx]
----
import { Board } from "./Board";
----

Also don't forget to pass the `props`. Prop `size` is whatever number you like but usually this game is played on 5 by 5 grid. And prop `change` is number from 0 to 1. When set to 0 no lights will turn on and 1 means all lights turn on. Do as you wish! 

Here's what I've chosen.
[source, jsx]
----
import { Board } from "./Board";

function App() {
  return <Board size={5} chance={0.25} />;
}

export default App;

----
$$),('56e6edf6-c9df-4d34-ad7d-01870053e299', '6cbe9315-ac7f-424e-902e-15b61a5d2004', 02, 'Turning lights on and off', $$

== Turning lights on and off

This where things get little bit more difficult but don't worry, you can do it. And I'm here to help you with everything.

First of all let's make a function that checks what button we've clicked and changes the state.

[source, jsx]
----
 const toggleOne = (currentRow: number, currentColumn: number) => {
    
  };
----

This looks good. It takes index of the row and column that we've clicked. Now to do something with it.

[source, jsx]
----
 const toggleOne = (currentRow: number, currentColumn: number) => {
    setGrid((latestGrid) =>
      latestGrid.map((row, rowIndex) =>
        currentRow === rowIndex
          ? row.map((cell, columnIndex) =>
              currentColumn === columnIndex ? !cell : cell
            )
          : row
      )
    );
  };
----
Can you belive it? This is all we need to change state of one light.

Now to tell you what it does. First thing we see is that we're change the value of useState() `grid`. Next the already familiar two `maps()`. And what they do? Yes they acces the two dimensional array. This time the first one checks if `currentRow` which is index of the button we've clicked is the same as `rowIndex`. If it is the same, the second `map()` is executed. And this one checks if the `currentColumn` is the same as `columnIndex`. Again if the condition is true, the light changes state, if not it stays the same.

As this changes the state of only one light, we need to call it five time with different values to toggle all of the lights.

[source, jsx]
----
const toggleLights = (currentRow: number, currentColumn: number) => {
    toggleOne(currentRow, currentColumn); // self
    toggleOne(currentRow - 1, currentColumn); // up
    toggleOne(currentRow + 1, currentColumn); // down
    toggleOne(currentRow, currentColumn + 1); // right
    toggleOne(currentRow, currentColumn - 1); // left
  };
----
Same props as before. As you can see we are calling function `toggleOne()` five times. First time we pass the indexes of the clicked button and then we add or subtract 1.

Now let's call this funcion when we click the button.
[source, jsx]
----
 <button
   className={"cell " + (cell ? "cellOn" : "cellOff")}
   onClick={() => toggleLights(rowIndex, columnIndex)}
   key={columnIndex}
  ></button>
----
We have just added `onclick()` to the existing button and passed indexes from our `maps()`
$$),('8623cebd-30ee-4f73-aff7-6c559de785b1', '6cbe9315-ac7f-424e-902e-15b61a5d2004', 03, 'Checking win', $$

== Checking win

There just one more very important thing to do. That is acutally being able to win this game. We have to make function that checks if all the lights are off. We want to do that every time the array `grid` has changed. That means there is one hooks that is great for this.

But first of all we need to make a variable that we can change if all the lights are out.
[source, jsx]
----
const [hasWon, setHasWon] = useState(false);
----

I hope you were thinking about `useState()` too.

Now actually checking if we have won or not.
[source, jsx]
----
useEffect(() => {
    // check if every cell in every row is disabled (it is equal to false)
    setHasWon(grid.every((row) => row.every((cell) => !cell)));
  }, [grid]);
----

The hook `useEffect()` was an obvious choice here as we need to check every time `grid` changes. `grid.every()` checks if every cell in row is disabled. Then `row.every()` does the same but inside `row`. And then returns `!cell`. So if `cell` is turned of it is equal to `false`. That means `!cell` is true and `setHasWon()` gets set to true.

Great we have a variable that has the necessary information if we won. So now we can display congratulations text and restart button based on this variable.

We will add this to the `return()` of `Board`. This is how the `return()` looks now.
[source, jsx]
----
return (
    <div className="container">
      <h1 className="title">
        <span className="title1">LIGHTS</span>
        &nbsp;
        <span className="title2">OUT</span>
      </h1>

      <div className="board">
        {grid.map((row, rowIndex) => (
          <div className="row" key={rowIndex}>
            {row.map((cell, columnIndex) => (
              /*cell*/
              <button
                className={"cell " + (cell ? "cellOn" : "cellOff")}
                onClick={() => toggleLights(rowIndex, columnIndex)}
                key={columnIndex}
              ></button>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
----

And now the part that happens when we win.

[source, jsx]
----
return (
    <div className="container">
      <h1 className="title">
        <span className="title1">LIGHTS</span>
        &nbsp;
        <span className="title2">OUT</span>
      </h1>

      <div className="board">
        {grid.map((row, rowIndex) => (
          <div className="row" key={rowIndex}>
            {row.map((cell, columnIndex) => (
              /*cell*/
              <button
                className={"cell " + (cell ? "cellOn" : "cellOff")}
                onClick={() => toggleLights(rowIndex, columnIndex)}
                key={columnIndex}
              ></button>
            ))}
          </div>
        ))}
      </div>

      {/* if the player has won, show the reset button and congratulations text*/}
      {hasWon ? (
        <div className="winContainer">
          <p className="winText"> CONGRATULATIONS!!!</p>
          <button onClick={() => setGrid(makeGrid())} className="restartButton">
            RESTART
          </button>
        </div>
      ) : undefined}
    </div>
  );
----

We just check if `hasWon` is true and then show the text and restart button. And the button has `onClick()` on it that calls `setGrid(makeGrid())` this creates new ranodom variation of lights and we can play again. As you can see there are some new css classes so let's make them in `Board.css`.

[source, css]
----
.winContainer {
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
}

.winText {
  margin: 1rem;
  color: rgb(255, 187, 0);
}

.restartButton {
  border-radius: 0.5rem;
  font-size: 3rem;
  border: none;
  background-color: rgb(50, 50, 50);
  color: rgb(255, 187, 0);
}

.restartButton:hover {
  background-color: rgb(40, 40, 40);
}
----

There you have it we are almost done, but we can do one last thing and that is to make the font of the title look nicer and make the background gray. 

You should have file called `index.css` it folder `src`. If you have found it, replace the code inside with this.

[source, css]
----
@import url("https://fonts.googleapis.com/css2?family=Bungee&display=swap");
body {
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  background-color: rgb(40, 40, 40);
  font-family: "Bungee";
}

code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New",
    monospace;
}
----

And with this there is only one thing I can say. 

Congratulations! You've just made your first game in react.
$$),('8ee9c76e-98de-48c9-a636-d167225c1051', '6cbe9315-ac7f-424e-902e-15b61a5d2004', 04, 'The completed game', $$

== The completed game

The game you have should look something like this.
[%interactive%by-id ,source]
----
react-ts-9j8nnw
----
$$)
on conflict (id) do update set
	"course_order" = EXCLUDED."course_order",
	"name"         = EXCLUDED."name",
	"content"      = EXCLUDED."content";

