use {
	hyper::Uri,
	serde::Deserialize,
	std::{self, net::SocketAddr},
	thiserror::Error,
	tracing::{debug, instrument},
};

#[derive(Deserialize, Debug)]
pub struct Config {
	pub(crate) api_address: SocketAddr,
	pub(crate) database_url: String,
	pub(crate) s3_bucket: String,
	#[serde(deserialize_with = "deserialize_uri")]
	pub(crate) s3_endpoint: Uri,
	pub(crate) s3_region: String,
	pub(crate) s3_access_key: String,
	pub(crate) s3_secret_key: String,
}

fn deserialize_uri<'de, D>(deserializer: D) -> Result<Uri, D::Error>
where
	D: serde::Deserializer<'de>,
{
	use serde::de::Error;

	<String as serde::de::Deserialize>::deserialize(deserializer)?
		.try_into()
		.map_err(|e| D::Error::custom(format!("invalid URI: {e}")))
}

#[derive(Debug, Error)]
pub enum Error {
	#[error("loading the configuration from environment variables")]
	Env(#[from] envy::Error),
}

#[instrument("load app configuration", level = "debug")]
pub fn read() -> Result<Config, Error> {
	debug!("reading env variables....");

	let config = envy::prefixed("RECOURSE_BACKEND_").from_env::<Config>()?;

	debug!("successfully parsed.");

	Ok(config)
}
