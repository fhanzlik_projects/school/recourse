use std::sync::Arc;

pub trait With<T, Args> {
	type Output: Clone;

	fn with(self, value: Arc<T>) -> Self::Output;
}

macro impl_with($($ident:ident),*) {
	impl<T, F, R, $($ident),*> With<T, ($($ident,)*)> for F
	where
		F: Clone + Fn(Arc<T>, $($ident),*) -> R,
	{
		type Output = impl Clone + Fn($($ident),*) -> R;

		#[allow(non_snake_case)]
		fn with(self, value: Arc<T>) -> Self::Output {
			move |$($ident),*| self(value.clone(), $($ident),*)
		}
	}
}

impl_with!();
all_the_tuples!(impl_with);

macro all_the_tuples($name:ident) {
	$name!(T1);
	$name!(T1, T2);
	$name!(T1, T2, T3);
	$name!(T1, T2, T3, T4);
	$name!(T1, T2, T3, T4, T5);
	$name!(T1, T2, T3, T4, T5, T6);
	$name!(T1, T2, T3, T4, T5, T6, T7);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8, T9);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13);
	$name!(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14);
	$name!(
		T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15
	);
	$name!(
		T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16
	);
}
