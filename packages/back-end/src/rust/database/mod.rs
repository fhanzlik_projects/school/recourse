mod methods;
pub mod schema;

pub use methods::*;
use {
	diesel::r2d2,
	diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness},
	thiserror::Error,
	tracing::{debug, instrument},
};

pub type Db = diesel::pg::Pg;
pub type Connection = diesel::PgConnection;
type ConnectionManager = r2d2::ConnectionManager<Connection>;
pub type Pool = r2d2::Pool<ConnectionManager>;
pub type PooledConnection = r2d2::PooledConnection<ConnectionManager>;

#[derive(Debug, Error)]
pub enum CreatePoolError {
	#[error("retrieving a connection from the connection pool")]
	Pool(#[from] r2d2::PoolError),

	#[error("connecting to the database")]
	Diesel(#[from] diesel::result::Error),
}

#[derive(Debug, Error)]
pub enum RunMigrationsError {
	#[error("unknown error")]
	Diesel(#[source] Box<dyn std::error::Error + Send + Sync>),
}

const MIGRATIONS: EmbeddedMigrations = embed_migrations!("src/sql/migrations");

#[instrument(level = "debug", skip(connection))]
pub fn run_migrations(
	connection: &mut impl MigrationHarness<Db>,
) -> Result<(), RunMigrationsError> {
	debug!("running migrations...");
	connection
		.run_pending_migrations(MIGRATIONS)
		.map_err(RunMigrationsError::Diesel)?;
	debug!("migrations ran successfully.");

	Ok(())
}

#[instrument(level = "debug")]
pub fn create_pool(url: String) -> Result<Pool, CreatePoolError> {
	Ok(r2d2::Pool::builder().build(ConnectionManager::new(url))?)
}
