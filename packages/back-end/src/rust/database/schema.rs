// @generated automatically by Diesel CLI.

diesel::table! {
    course (id) {
        id -> Uuid,
        name -> Text,
        description -> Text,
        cover_image -> Text,
    }
}

diesel::table! {
    lesson (id) {
        id -> Uuid,
        course -> Uuid,
        name -> Text,
        content -> Text,
        course_order -> Int2,
    }
}

diesel::table! {
    lesson_user_state (user, lesson) {
        user -> Uuid,
        lesson -> Uuid,
        completed -> Bool,
    }
}

diesel::table! {
    role (id) {
        id -> Uuid,
        name -> Text,
    }
}

diesel::table! {
    session (id) {
        id -> Uuid,
        user -> Uuid,
    }
}

diesel::table! {
    user (id) {
        id -> Uuid,
        username -> Text,
        password_hash -> Text,
        display_name -> Text,
    }
}

diesel::joinable!(lesson -> course (course));
diesel::joinable!(lesson_user_state -> lesson (lesson));
diesel::joinable!(session -> user (user));

diesel::allow_tables_to_appear_in_same_query!(
    course,
    lesson,
    lesson_user_state,
    role,
    session,
    user,
);
