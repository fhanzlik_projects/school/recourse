use {
	crate::database::{
		schema::{course, lesson, lesson_user_state},
		Db, PooledConnection,
	},
	diesel::{
		helper_types::{AsSelect, Find, InnerJoin, Select},
		AsChangeset, Associations, Identifiable, Insertable, QueryDsl, Queryable, RunQueryDsl,
		Selectable, SelectableHelper,
	},
	uuid::Uuid,
};

#[derive(Queryable, Selectable, Identifiable, AsChangeset, Debug)]
#[diesel(table_name = course)]
pub struct Course {
	pub id: Uuid,
	pub name: String,
	pub description: String,
	pub cover_image: String,
}
impl Course {
	pub fn by_id(id: Uuid) -> Find<Select<course::table, AsSelect<Self, Db>>, Uuid> {
		course::table.select(Self::as_select()).find(id)
	}

	pub fn by_lesson_id(
		id: Uuid,
	) -> Select<InnerJoin<Find<lesson::table, Uuid>, course::table>, AsSelect<Self, Db>> {
		lesson::table
			.find(id)
			.inner_join(course::table)
			.select(Self::as_select())
	}

	pub fn all() -> Select<course::table, AsSelect<Self, Db>> {
		course::table.select(Self::as_select())
	}
}
#[derive(Identifiable, AsChangeset, Debug)]
#[diesel(table_name = course)]
#[allow(clippy::module_name_repetitions)]
pub struct CourseUpdate {
	pub id: Uuid,
	pub name: Option<String>,
	pub description: Option<String>,
	pub cover_image: Option<String>,
}
impl CourseUpdate {
	pub fn apply(&self, connection: &mut PooledConnection) -> Result<(), diesel::result::Error> {
		diesel::update(&self).set(self).execute(connection)?;
		Ok(())
	}
}

#[derive(Queryable, Selectable, Identifiable, Associations, Debug)]
#[diesel(table_name = lesson, belongs_to(Course, foreign_key = course))]
pub struct Lesson {
	pub id: Uuid,
	pub name: String,
	pub content: String,
	pub course_order: i16,
	pub course: Uuid,
}
impl Lesson {
	pub fn by_id(id: Uuid) -> Find<Select<lesson::table, AsSelect<Self, Db>>, Uuid> {
		lesson::table.select(Self::as_select()).find(id)
	}

	pub fn all() -> Select<lesson::table, AsSelect<Self, Db>> {
		lesson::table.select(Self::as_select())
	}
}
#[derive(Identifiable, AsChangeset, Debug)]
#[diesel(table_name = lesson)]
pub struct LessonUpdate {
	pub id: Uuid,
	pub name: Option<String>,
	pub content: Option<String>,
}
impl LessonUpdate {
	pub fn apply(&self, connection: &mut PooledConnection) -> Result<(), diesel::result::Error> {
		diesel::update(&self).set(self).execute(connection)?;
		Ok(())
	}
}

#[derive(Queryable, Selectable, Associations, Identifiable, Insertable, AsChangeset)]
#[diesel(
	table_name = lesson_user_state,
	primary_key(lesson, user),
	belongs_to(Lesson, foreign_key = lesson),
)]
pub struct LessonUserState {
	pub lesson: Uuid,
	pub user: Uuid,
	pub completed: bool,
}
impl LessonUserState {
	pub fn apply(&self, connection: &mut PooledConnection) -> Result<(), diesel::result::Error> {
		diesel::insert_into(lesson_user_state::table)
			.values(self)
			.on_conflict((lesson_user_state::lesson, lesson_user_state::user))
			.do_update()
			.set(self)
			.execute(connection)?;
		Ok(())
	}
}
