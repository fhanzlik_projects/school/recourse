//---------------//
// feature gates //
//---------------//
#![feature(macro_metavar_expr)]
#![feature(type_alias_impl_trait)]
#![feature(async_closure)]
#![feature(decl_macro)]
#![feature(io_error_more)]
#![feature(try_blocks)]
#![feature(never_type)]
//-------//
// lints //
//-------//
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// actix requires all request handlers to be `async`
#![allow(clippy::unused_async)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

//---------//
// modules //
//---------//

use {
	self::macros::With,
	axum::{routing::get, Router},
	logging::print_errors,
	std::sync::Arc,
	thiserror::Error,
	tracing::warn,
};

mod config;
mod database;
mod logging;
mod macros;
mod rpc;
mod storage;

#[tokio::main]
async fn main() {
	print_errors(run().await).ok();
}

#[allow(clippy::future_not_send)]
async fn run() -> Result<(), Error> {
	// .env must be read before everything else, because some things, e.g. logging, might depend on
	// env already being properly set.
	//
	// "not found" errors are considered fine because there will be no .env file in prod, but .env
	// file with errors is a problem and should be reported
	#[cfg(debug_assertions)]
	match dotenv::dotenv() {
		Ok(_) => Ok(()),
		Err(dotenv::Error::Io(io_err)) if io_err.kind() == std::io::ErrorKind::NotFound => Ok(()),
		Err(e) => Err(e),
	}?;

	logging::init()?;

	let config = config::read()?;

	let db_pool = Arc::new(database::create_pool(config.database_url)?);
	let aws_config = storage::load_aws_config(
		config.s3_endpoint,
		config.s3_region,
		&config.s3_access_key,
		&config.s3_secret_key,
	)
	.await;
	let s3_client = Arc::new(storage::connect(&aws_config, &config.s3_bucket).await?);

	database::run_migrations(&mut db_pool.get()?)?;

	let app = Router::new()
		.route("/", get(|| async { "Hello API!" }))
		.route(
			"/api_socket",
			get((|db_pool, s3_client, upload_bucket, socket| async {
				rpc::handler(db_pool, s3_client, socket, upload_bucket)
			})
			.with(db_pool)
			.with(s3_client)
			.with(Arc::new(config.s3_bucket))),
		);

	axum::Server::bind(&config.api_address)
		.serve(app.into_make_service())
		.await
		.map_err(Error::RunningTheServer)?;

	Ok(())
}

#[derive(Debug, Error)]
enum Error {
	#[error("error applying a .env file")]
	DotEnv(#[from] dotenv::Error),

	#[error("initializing logging")]
	LoggingInit(#[from] logging::InitError),

	#[error("reading the app configuration")]
	Config(#[from] config::Error),

	#[error("creating database pool")]
	CreateDatabasePool(#[from] database::CreatePoolError),

	#[error("running database migrations")]
	RunDbMigrations(#[from] database::RunMigrationsError),

	#[error("acquiring pooled connection")]
	AcquirePooledConnection(#[from] diesel::r2d2::PoolError),

	#[error("connecting to S3")]
	S3Connect(#[from] storage::ConnectError),

	#[error("running web server")]
	RunningTheServer(#[source] hyper::Error),
}
