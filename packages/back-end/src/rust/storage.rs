use {
	aws_sdk_s3::{
		error::{HeadBucketError, HeadBucketErrorKind},
		types::SdkError,
		Credentials, Endpoint, Region,
	},
	tracing::{debug, instrument},
};

pub async fn load_aws_config(
	endpoint: hyper::Uri,
	region: String,
	access_key: &str,
	secret_key: &str,
) -> aws_config::SdkConfig {
	aws_config::from_env()
		.region(Region::new(region))
		.endpoint_resolver(Endpoint::immutable(endpoint))
		.credentials_provider(Credentials::new(
			access_key, secret_key, None, None, "back-end",
		))
		.load()
		.await
}

#[instrument("connect to S3", level = "debug", skip(aws_config))]
pub async fn connect(
	aws_config: &aws_config::SdkConfig,
	bucket: &str,
) -> Result<aws_sdk_s3::Client, ConnectError> {
	let client = aws_sdk_s3::Client::new(aws_config);

	debug!("trying to access bucket...");
	match client.head_bucket().bucket(bucket).send().await {
		Ok(_) => {
			debug!("bucket exists. continuing.");
		},
		Err(SdkError::ServiceError {
			err: HeadBucketError {
				kind: HeadBucketErrorKind::NotFound(_),
				..
			},
			..
		}) => {
			debug!("bucket does not exist. creating...");
			client.create_bucket().bucket(bucket).send().await?;
		},
		Err(e) => Err(e)?,
	};

	Ok(client)
}

#[derive(Debug, thiserror::Error)]
pub enum ConnectError {
	#[error("error while crating a bucket")]
	CreateBucket(#[from] SdkError<aws_sdk_s3::error::CreateBucketError>),

	#[error("error while accessing the bucket")]
	AccessingBucket(#[from] SdkError<aws_sdk_s3::error::HeadBucketError>),
}
