use {
	crate::database::{
		course::{self, Lesson, LessonUserState},
		schema::{self, lesson_user_state},
		Pool, PooledConnection,
	},
	aws_sdk_s3::{model::ObjectCannedAcl, presigning::config::PresigningConfig},
	diesel::{
		r2d2, BelongingToDsl, ExpressionMethods, GroupedBy, QueryDsl, RunQueryDsl, SelectableHelper,
	},
	futures::{
		future::{ready, Ready},
		Future,
	},
	recourse_api::{
		models,
		models::{
			AppCourseListCourse, AppCourseListCourseLesson, AppLessonDetailCourse,
			AppLessonDetailCourseLesson, AppLessonDetailLesson, CmsCourseListCourse,
			CmsLessonListLesson,
		},
		service::{WasmUuid, World},
		ApiError, ApiResult, IntoApiResult,
	},
	recourse_commons::error_formatting::FormattedError,
	serde::Serialize,
	std::{pin::Pin, sync::Arc, time::Duration},
	thiserror::Error,
	tracing::{info, instrument},
};

#[derive(Clone)]
pub struct WorldHandler {
	pub pool: Arc<Pool>,
	pub s3_client: Arc<aws_sdk_s3::Client>,
	pub upload_bucket: Arc<String>,
}

impl WorldHandler {
	fn connection(&self) -> Result<PooledConnection, r2d2::PoolError> {
		self.pool.get()
	}
}

// TODO: make the DB access async
impl World for WorldHandler {
	// app //
	type AppCourseListFut = Ready<ApiResult<Vec<models::AppCourseListCourse>>>;
	type AppLessonDetailFut = Ready<ApiResult<models::AppLessonDetail>>;
	type AppLessonSetCompletedFut = Ready<ApiResult<()>>;
	// cms //
	type CmsCourseGetFut = Ready<ApiResult<models::CmsCourseGet>>;
	type CmsCourseListFut = Ready<ApiResult<Vec<models::CmsCourseListCourse>>>;
	type CmsCourseUpdateFut = Ready<ApiResult<()>>;
	type CmsLessonGetFut = Ready<ApiResult<models::CmsLessonGet>>;
	type CmsLessonListFut = Ready<ApiResult<Vec<models::CmsLessonListLesson>>>;
	type CmsLessonUpdateFut = Ready<ApiResult<()>>;
	// misc //
	type FilesRequestUploadFut = Pin<Box<dyn Future<Output = ApiResult<String>> + Send>>;

	// app //

	fn app_course_list(self, _: tarpc::context::Context) -> Self::AppCourseListFut {
		let res: Result<_, Error> = try {
			let courses = course::Course::all().load(&mut self.connection()?)?;
			let lessons = Lesson::belonging_to(&courses)
				.select(Lesson::as_select())
				.order(schema::lesson::course_order)
				.load(&mut self.connection()?)?;

			lessons
				.grouped_by(&courses)
				.into_iter()
				.zip(courses)
				.map(|(lessons, course)| AppCourseListCourse {
					id: WasmUuid::new(course.id),
					name: course.name,
					description: course.description,
					cover_image: course.cover_image,
					first_lesson: WasmUuid::new(
						lessons
							.get(0)
							.expect("every course should have at least one lesson!")
							.id,
					),
					lessons: lessons
						.into_iter()
						.map(|Lesson { name, .. }| AppCourseListCourseLesson { name })
						.collect(),
				})
				.collect()
		};
		ready(res.into_api_result())
	}

	fn app_lesson_detail(
		self,
		_: tarpc::context::Context,
		id: WasmUuid,
		hack_user: Option<WasmUuid>,
	) -> Self::AppLessonDetailFut {
		let res: Result<_, Error> = try {
			let lesson = course::Lesson::by_id(*id).first(&mut self.connection()?)?;
			let course = course::Course::by_lesson_id(*id).first(&mut self.connection()?)?;
			let lessons = course::Lesson::belonging_to(&course)
				.select(course::Lesson::as_select())
				.order(schema::lesson::course_order)
				.load(&mut self.connection()?)?;
			let lesson_states = hack_user
				.map(|hack_user| -> Result<_, Error> {
					Ok(course::LessonUserState::belonging_to(&lessons)
						.filter(lesson_user_state::user.eq(*hack_user))
						.select(LessonUserState::as_select())
						.load(&mut self.connection()?)?)
				})
				.transpose()?;

			models::AppLessonDetail {
				lesson: AppLessonDetailLesson {
					id: WasmUuid::new(lesson.id),
					name: lesson.name,
					content: lesson.content,
				},
				course: AppLessonDetailCourse {
					id: WasmUuid::new(course.id),
					name: course.name,
					lessons: match lesson_states {
						None => lessons
							.into_iter()
							.map(|lesson| AppLessonDetailCourseLesson {
								id: WasmUuid::new(lesson.id),
								name: lesson.name,
								completed: false,
							})
							.collect(),
						Some(states) => states
							.grouped_by(&lessons)
							.into_iter()
							.zip(lessons)
							.map(|(states, lesson)| AppLessonDetailCourseLesson {
								id: WasmUuid::new(lesson.id),
								name: lesson.name,
								completed: states.get(0).map_or(false, |state| state.completed),
							})
							.collect(),
					},
				},
			}
		};
		ready(res.into_api_result())
	}

	fn app_lesson_set_completed(
		self,
		_: tarpc::context::Context,
		lesson: WasmUuid,
		completed: bool,
		hack_user: WasmUuid,
	) -> Self::AppLessonSetCompletedFut {
		let res: Result<_, Error> = try {
			course::LessonUserState {
				user: *hack_user,
				lesson: *lesson,
				completed,
			}
			.apply(&mut self.connection()?)?
		};
		ready(res.into_api_result())
	}

	// cms //

	fn cms_course_list(self, _: tarpc::context::Context) -> Self::CmsCourseListFut {
		let res: Result<_, Error> = try {
			course::Course::all()
				.load(&mut self.connection()?)?
				.into_iter()
				.map(|item| CmsCourseListCourse {
					id: WasmUuid::new(item.id),
					name: item.name,
					description: item.description,
					cover_image: item.cover_image,
				})
				.collect()
		};
		ready(res.into_api_result())
	}

	fn cms_course_get(self, _: tarpc::context::Context, id: WasmUuid) -> Self::CmsCourseGetFut {
		let res: Result<_, Error> = try {
			let course::Course {
				id,
				name,
				description,
				cover_image,
			} = course::Course::by_id(*id).first(&mut self.connection()?)?;

			models::CmsCourseGet {
				id: WasmUuid::new(id),
				name,
				description,
				cover_image,
			}
		};
		ready(res.into_api_result())
	}

	fn cms_course_update(
		self,
		_: tarpc::context::Context,
		course: models::CmsCourseUpdate,
	) -> Self::CmsCourseUpdateFut {
		let models::CmsCourseUpdate {
			id,
			name,
			description,
			cover_image,
		} = course;
		let res: Result<_, Error> = try {
			(course::CourseUpdate {
				id: *id,
				name,
				description,
				cover_image,
			})
			.apply(&mut self.connection()?)?;
		};
		ready(res.into_api_result())
	}

	fn cms_lesson_list(self, _context: tarpc::context::Context) -> Self::CmsLessonListFut {
		let res: Result<_, Error> = try {
			let courses = course::Course::all().load(&mut self.connection()?)?;

			let lessons = course::Lesson::belonging_to(&courses)
				.select(course::Lesson::as_select())
				.load(&mut self.connection()?)?;

			lessons
				.grouped_by(&courses)
				.into_iter()
				.zip(courses)
				.flat_map(|(lessons, course)| {
					lessons
						.into_iter()
						.map(|lesson| CmsLessonListLesson {
							id: WasmUuid::new(lesson.id),
							name: lesson.name,
							course_name: course.name.clone(),
						})
						.collect::<Vec<_>>()
				})
				.collect()
		};
		ready(res.into_api_result())
	}

	fn cms_lesson_get(self, _: tarpc::context::Context, id: WasmUuid) -> Self::CmsLessonGetFut {
		let res: Result<_, Error> = try {
			let course::Lesson {
				id, name, content, ..
			} = course::Lesson::by_id(*id).first(&mut self.connection()?)?;

			models::CmsLessonGet {
				id: WasmUuid::new(id),
				name,
				content,
			}
		};
		ready(res.into_api_result())
	}

	fn cms_lesson_update(
		self,
		_: tarpc::context::Context,
		course: models::CmsLessonUpdate,
	) -> Self::CmsCourseUpdateFut {
		let models::CmsLessonUpdate { id, name, content } = course;
		let res: Result<_, Error> = try {
			(course::LessonUpdate {
				id: *id,
				name,
				content,
			})
			.apply(&mut self.connection()?)?;
		};
		ready(res.into_api_result())
	}

	// misc //

	#[instrument(skip(self))]
	fn files_request_upload(
		self,
		_: tarpc::context::Context,
		id: WasmUuid,
		mime_type: String,
	) -> Self::FilesRequestUploadFut {
		Box::pin(async move {
			info!("generating presigned upload URL");

			let presigning_config = PresigningConfig::expires_in(Duration::from_secs(5 * 60))
				.expect("presigned config should be verified by the developer to be OK");

			match mime_type.as_str() {
				"image/png" | "image/jpeg" => {},
				_ => return Err(Error::UploadIllegalMimeType { mime_type })?,
			}

			Ok(self
				.s3_client
				.put_object()
				.bucket(&*self.upload_bucket)
				.key(format!("uploads/{}", id.as_hyphenated()))
				.acl(ObjectCannedAcl::PublicRead)
				.content_type(mime_type)
				.presigned(presigning_config)
				.await
				.map_err(Error::S3PresignedObjectPut)?
				.uri()
				.to_string())
		})
	}
}

#[derive(Debug, Error)]
pub enum Error {
	#[error("error while acquiring a database connection")]
	Pool(#[from] r2d2::PoolError),
	#[error("error communicating with the database")]
	Database(#[from] diesel::result::Error),
	#[error("requesting pre-signed object put URI")]
	S3PresignedObjectPut(#[source] aws_sdk_s3::types::SdkError<aws_sdk_s3::error::PutObjectError>),
	#[error("attempted to upload illegal mime type: {mime_type}")]
	UploadIllegalMimeType { mime_type: String },
}
impl From<Error> for ApiError {
	fn from(e: Error) -> Self {
		// TODO: try to return more sensible errors for known error values
		let res = match e {
			Error::Pool(_) | Error::Database(_) | Error::S3PresignedObjectPut(_) => {
				Self::InternalServerError
			},
			Error::UploadIllegalMimeType { .. } => Self::MalformedRequest,
		};

		tracing::warn!("{}", FormattedError::new(e));

		res
	}
}

impl Serialize for Error {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		serializer.serialize_str(&self.to_string())
	}
}
