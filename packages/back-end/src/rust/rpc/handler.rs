use {
	crate::{database, rpc::service::WorldHandler},
	axum::{body::BoxBody, extract::WebSocketUpgrade},
	recourse_api::service::World,
	std::sync::Arc,
	tarpc::server::{BaseChannel, Channel},
	tarpc_transport_postcard::TransportPostcard,
	tarpc_transport_websocket_axum::TransportWebsocket,
};

pub fn handler(
	pool: Arc<database::Pool>,
	s3_client: Arc<aws_sdk_s3::Client>,
	socket: WebSocketUpgrade,
	upload_bucket: Arc<String>,
) -> axum::http::Response<BoxBody> {
	socket.on_upgrade(|socket| async {
		let transport = TransportPostcard::new(TransportWebsocket::new(socket));
		let server = BaseChannel::with_defaults(transport);

		server
			.execute(
				WorldHandler {
					pool,
					s3_client,
					upload_bucket,
				}
				.serve(),
			)
			.await;
	})
}
