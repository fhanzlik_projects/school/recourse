use {
	recourse_commons::{error_formatting::FormattedError, tracing_formats},
	thiserror::Error,
	tracing_error::ErrorLayer,
	tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, registry, EnvFilter},
};

pub mod error_span;

#[derive(Debug, Error)]
pub enum InitError {
	#[error("setting tracing handler")]
	TracingSetHandler(#[from] tracing::dispatcher::SetGlobalDefaultError),

	#[error("parsing tracing env variables")]
	TracingReadEnv(#[from] tracing_subscriber::filter::ParseError),
}

pub fn init() -> Result<(), InitError> {
	tracing::subscriber::set_global_default(
		registry()
			.with(
				EnvFilter::try_from_default_env()
					.or_else(|_| EnvFilter::try_new("warn,main[]=info"))?,
			)
			.with(tracing_subscriber::fmt::layer().event_format(tracing_formats::Normal::default()))
			.with(ErrorLayer::default()),
	)?;

	Ok(())
}

pub fn print_errors<T, E: std::error::Error + 'static>(res: Result<T, E>) -> Result<T, ()> {
	match res {
		Err(error) => {
			tracing::error!("{}", FormattedError::new(error));

			Err(())
		},
		Ok(t) => Ok(t),
	}
}
