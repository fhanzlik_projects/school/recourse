use tracing_error::InstrumentResult;

pub trait ErrorSpan<T> {
	type Instrumented;

	fn err_span(self) -> Result<T, Self::Instrumented>;
}

impl<T, R> ErrorSpan<T> for R
where
	R: InstrumentResult<T>,
{
	type Instrumented = <R as InstrumentResult<T>>::Instrumented;

	fn err_span(self) -> Result<T, Self::Instrumented> {
		InstrumentResult::in_current_span(self)
	}
}
