import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";
import topLevelAwait from "vite-plugin-top-level-await";
import wasm from "vite-plugin-wasm";

export default defineConfig({
	plugins: [
		wasm(),
		topLevelAwait(),
		solidPlugin({
			babel: {
				plugins: [
					[
						`@locator/babel-jsx/dist`,
						{
							env: `development`,
						},
					],
				],
			},
		}),
	],
});
