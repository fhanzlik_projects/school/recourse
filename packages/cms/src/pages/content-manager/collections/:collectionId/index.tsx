import {
	CmsCourseListCourse,
	CmsLessonListLesson,
} from "@recourse/api/build/wasm";
import {
	background,
	colour,
	getProperty,
	shadow,
	text,
	uuid,
} from "@recourse/commons";
import { Link, useParams } from "@solidjs/router";
import { createResource, For, Show, Suspense } from "solid-js";
import { styled } from "solid-styled-components";
import { useApiClient } from "../../../../contexts/api";

const collections = {
	"app::course": () => {
		const api = useApiClient();
		return {
			listItems: () => api.cms.course.list(),
			renderItem: (item: CmsCourseListCourse) => item.name,
		};
	},
	"app::lesson": () => {
		const api = useApiClient();
		return {
			listItems: () => api.cms.lesson.list(),
			renderItem: (item: CmsLessonListLesson) =>
				`${item.course_name} - ${item.name}`,
		};
	},
};

export default () => {
	const routerParams = useParams<{
		collectionId: string;
	}>();

	return (
		<Show
			when={getProperty(collections, routerParams.collectionId)}
			fallback={<Error message="no such collection" />}
			keyed
		>
			{(collectionHook) => {
				const collection = collectionHook();
				const [items] = createResource(
					() => routerParams.collectionId,
					async () => await collection.listItems(),
				);

				return (
					<Middle>
						<Suspense
							fallback={
								<div
									style={{
										"justify-content": `center`,
										"align-items": `center`,
										height: `100%`,
									}}
								>
									<img
										width={64}
										height={64}
										alt=""
										src="https://raw.githubusercontent.com/SamHerbert/SVG-Loaders/master/svg-loaders/rings.svg"
									/>
								</div>
							}
						>
							<For each={items()}>
								{(item) => (
									<ItemLink
										href={`/content-manager/collections/${
											routerParams.collectionId
										}/items/${uuid.debugStringify(
											uuid(item.id.to_slice()),
										)}`}
									>
										{collection.renderItem(
											// this cast is technically wrong, but I suspect it's
											// the cleanest way to make the types work here
											item as CmsCourseListCourse &
												CmsLessonListLesson,
										)}
									</ItemLink>
								)}
							</For>
						</Suspense>
					</Middle>
				);
			}}
		</Show>
	);
};

const Error = (props: { message: string }) => <div>Error: {props.message}</div>;

const Middle = styled.main`
	flex-grow: 1;

	padding: 1rem;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	gap: 1rem;

	${background(`normal`)}
`;

const ItemLink = styled(Link)`
	width: 24rem;

	display: flex;
	align-items: center;
	padding: 1rem 2rem;
	gap: 1rem;

	background: ${colour(`surfaceContainerLow.elevated.1`)};
	border-radius: 1rem;
	${text({ weight: `bold` })}
	${shadow(`normal`)}
`;
