import { JSX } from "solid-js";
import { styled } from "solid-styled-components";

export const FilePicker = (props: {
	setFile: (file: File) => void;
	children: JSX.Element;
}) => {
	return (
		<Label>
			<OpaqueInput
				type="file"
				name=""
				id=""
				accept="image/png,image/jpeg,image/webp,image/avif"
				onChange={(e) => {
					const files = e.currentTarget.files;
					if (files === null || files.length !== 1) return;

					props.setFile(files[0]);
				}}
				onDragOver={(e) => {
					e.preventDefault();
				}}
				onDrop={(e) => {
					e.preventDefault();

					if (
						e.dataTransfer?.files === undefined ||
						e.dataTransfer.files.length !== 1
					) {
						return;
					}

					props.setFile(e.dataTransfer.files[0]);
				}}
			/>
			{props.children}
		</Label>
	);
};

const OpaqueInput = styled.input`
	position: absolute;
	width: 1px;
	height: 1px;
	clip: rect(0 0 0 0);
	clip-path: inset(50%);
	overflow: hidden;
	white-space: nowrap;
`;

const Label = styled.label`
	display: flex;
	justify-content: center;
`;
