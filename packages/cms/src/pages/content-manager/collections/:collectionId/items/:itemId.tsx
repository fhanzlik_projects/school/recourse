import { Configuration, FrontendApi } from "@ory/client";
import { ApiClient } from "@recourse/api";
import {
	background,
	button,
	css,
	fetchStrict,
	focusOutline,
	getProperty,
	runBackgroundAsyncTask,
	shadow,
	text,
	Theme,
	Uuid,
	uuid,
} from "@recourse/commons";
import { useParams } from "@solidjs/router";
import {
	createEffect,
	createResource,
	createSignal,
	Show,
	Suspense,
} from "solid-js";
import { styled } from "solid-styled-components";
import { toast } from "solid-toast";
import { AsciidocEditor } from "../../../../../components/asciidoc editor";
import { useApiClient } from "../../../../../contexts/api";
import { FilePicker } from "./file picker";

const collections = {
	"app::course": ({ id }: { id: Uuid }) => {
		const api = useApiClient();
		const [item] = createResource(() => api.cms.course.get(id));

		const [localName, setName] = createSignal<string>();
		const name = () => localName() ?? item()?.name;
		const [localDescription, setDescription] = createSignal<string>();
		const description = () => localDescription() ?? item()?.description;
		const [localImage, setImage] = createSignal<string>();
		const image = () => localImage() ?? item()?.cover_image;

		return {
			getSave: () => {
				const payload = {
					id,
					name: localName(),
					coverImage: localImage(),
				};

				return async () => await api.cms.course.update(payload);
			},
			content: () => (
				<>
					<FilePicker
						setFile={(file) =>
							runBackgroundAsyncTask(async () => {
								setImage(await uploadFile(api, file));
							})
						}
					>
						<Image src={image() ?? ``} />
					</FilePicker>

					<Caption>Title</Caption>

					<TextInputLine
						value={name()}
						onChange={(e) => setName(e.currentTarget.value)}
					/>

					<Caption>Description</Caption>

					<TextInputLine
						value={description()}
						onChange={(e) => setDescription(e.currentTarget.value)}
					/>
				</>
			),
		};
	},
	"app::lesson": ({ id }: { id: Uuid }) => {
		const api = useApiClient();
		const [item] = createResource(() => api.cms.lesson.get(id));

		const [localName, setName] = createSignal<string>();
		const name = () => localName() ?? item()?.name;
		const [localContent, setContent] = createSignal<string>();
		const content = () => localContent() ?? item()?.content;

		return {
			getSave: () => {
				const payload = {
					id,
					name: localName(),
					content: localContent(),
				};

				return async () => await api.cms.lesson.update(payload);
			},
			content: () => (
				<>
					<Caption>Title</Caption>

					<TextInputLine
						value={name()}
						onChange={(e) => setName(e.currentTarget.value)}
					/>

					<Caption>Content</Caption>
					<AsciidocEditor
						initialValue={content() ?? ``}
						setValue={setContent}
					/>
				</>
			),
		};
	},
};

const Error = (props: { message: string }) => <div>Error: {props.message}</div>;

const basePath = `http://localhost:4433`;
const ory = new FrontendApi(
	new Configuration({
		basePath,
		baseOptions: {
			withCredentials: true,
		},
	}),
);

const userLoggedIn = () => {
	const [value, setValue] = createSignal<boolean>();

	createEffect(() => {
		runBackgroundAsyncTask(async () => {
			try {
				await ory.toSession();
				// the use already has a session! no need to log in again.
				setValue(true);
				return;
			} catch {
				setValue(false);
				// the user doesn't have a session. this is usually expected
				console.log(`no session, continuing to login...`);
			}
		});
	});

	return value;
};

export default () => {
	const { itemId, collectionId } = useParams<{
		itemId: string;
		collectionId: string;
	}>();
	const id = uuid.parse(itemId);

	return (
		<Show
			when={getProperty(collections, collectionId)}
			fallback={<Error message="no such collection" />}
			keyed
		>
			{(collection) => {
				const item = collection({ id });
				return (
					<>
						<Middle>
							<Suspense
								fallback={
									<div
										style={{
											"justify-content": `center`,
											"align-items": `center`,
											height: `100%`,
										}}
									>
										<img
											width={64}
											height={64}
											alt=""
											src="https://raw.githubusercontent.com/SamHerbert/SVG-Loaders/master/svg-loaders/rings.svg"
										/>
									</div>
								}
							>
								<ActionPanel>
									<Button
										colour="primary"
										onClick={() => {
											const save = item.getSave();
											runBackgroundAsyncTask(async () => {
												await save();
												toast.success(
													`Successfully saved!`,
												);
											});
										}}
									>
										Save
									</Button>
								</ActionPanel>
								<Form>{item.content()}</Form>
							</Suspense>
						</Middle>
						<Relations />
					</>
				);
			}}
		</Show>
	);
};

const Middle = styled.main`
	max-width: 100%;
	flex-grow: 1;
	display: flex;
	flex-direction: column;
	align-items: center;
	${background(`normal`)}
`;

const Form = styled.div`
	width: min(100%, 64rem);
	padding: 3rem 2rem;
	max-width: 100%;
	display: flex;
	flex-direction: column;
	gap: 1rem;
`;

const Image = styled.img`
	width: 100%;
	max-width: min(100%, 48rem);
	height: 16rem;
	object-fit: cover;
	border-radius: 1.5rem;
	box-shadow: 0 0 0.5rem hsla(0, 0%, 0%, 0.7);

	margin-bottom: 1rem;
`;

const Caption = styled.h1`
	margin: 0 2rem;
	${text({ weight: `bold` })}
`;

const inputStyles = ({ theme }: { theme?: Theme }) => css`
	margin: 0 3rem;
	padding: 0.5rem 1rem;
	${text({ family: `montserrat` })({ theme })}
	${background(`elevated.0`)({ theme })}
	${focusOutline()({ theme })}
	border-radius: 0.5rem;
`;

const TextInputLine = styled.input`
	${inputStyles}
`;

const TextInputBlock = styled.textarea`
	${inputStyles}
	flex-basis: 20rem;
`;

const Relations = styled.nav`
	flex-shrink: 0;
	flex-basis: 12rem;
	padding: 2rem;
	display: flex;
	flex-direction: column;
	${background(`elevated.0`)}
	${shadow(`normal`)}
`;

const ActionPanel = styled.section`
	padding: 1rem;
	align-self: stretch;
	display: flex;
	justify-content: flex-end;
`;

const Button = styled.button<{ colour: "primary" }>`
	${({ colour, theme }) => button({ colour })({ theme })}
`;

const uploadFile = async (api: ApiClient, file: File) => {
	const id = uuid.random();
	const url = await api.files.request_upload({ id, mimeType: file.type });

	await fetchStrict(url, {
		method: `PUT`,
		headers: {
			"x-amz-acl": `public-read`,
		},
		body: file,
	});

	const objectUrl = new URL(url);
	objectUrl.search = ``;

	return objectUrl.toString();
};
