import { background, shadow, text } from "@recourse/commons";
import { Link, Outlet } from "@solidjs/router";
import { For, Suspense } from "solid-js";
import { Dynamic } from "solid-js/web";
import { styled } from "solid-styled-components";
import { collections } from "..";
import { screens } from "../../../../components/screen";

export default () => {
	return (
		<screens.Common>
			<Container>
				<ContentTypeNav>
					<For each={collections}>
						{(collection) => (
							<ContentTypeNavLink
								href={`/content-manager/collections/${collection.id}`}
							>
								<Dynamic component={collection.icon} />
								{collection.name}
							</ContentTypeNavLink>
						)}
					</For>
				</ContentTypeNav>

				<Suspense
					fallback={
						<div
							style={{
								"justify-content": `center`,
								"align-items": `center`,
								height: `100%`,
							}}
						>
							<img
								width={64}
								height={64}
								alt=""
								src="https://raw.githubusercontent.com/SamHerbert/SVG-Loaders/master/svg-loaders/rings.svg"
							/>
						</div>
					}
				>
					<Outlet />
				</Suspense>
			</Container>
		</screens.Common>
	);
};

const Container = styled.div`
	display: flex;
	flex-grow: 1;
	align-items: stretch;

	${background(`normal`)}
`;

const ContentTypeNav = styled.nav`
	/* required to keep the shadow above the content that follows */
	/* TODO: evaluate whether setting a background for the whole container would be a better idea */
	position: relative;

	flex-basis: 12rem;
	flex-shrink: 0;
	padding: 2rem;
	${background(`elevated.0`)}
	${shadow(`normal`)}

	display: flex;
	flex-direction: column;
	gap: 0.5rem;
`;

const ContentTypeNavLink = styled(Link)`
	display: flex;
	gap: 0.5rem;
	align-items: center;

	${text({ weight: `bold` })}
`;
