import { background, colour, shadow, text } from "@recourse/commons";
import { Link } from "@solidjs/router";
import { BiRegularBookContent, BiRegularLibrary } from "solid-icons/bi";
import { For } from "solid-js";
import { Dynamic } from "solid-js/web";
import { styled } from "solid-styled-components";
import { screens } from "../../../components/screen";

export const collections = [
	{
		id: `app::course`,
		name: `Courses`,
		icon: BiRegularLibrary,
	},
	{
		id: `app::lesson`,
		name: `Lessons`,
		icon: BiRegularBookContent,
	},
];

export default () => {
	return (
		<Screen>
			<For each={collections}>
				{(collection) => (
					<ContentTypeLink
						href={`/content-manager/collections/${collection.id}`}
					>
						<Dynamic component={collection.icon} size={64} />
						{collection.name}
					</ContentTypeLink>
				)}
			</For>
		</Screen>
	);
};

const Screen = styled(screens.Common)`
	flex-grow: 1;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	gap: 1rem;

	${background(`normal`)}
`;

const ContentTypeLink = styled(Link)`
	width: 24rem;

	display: flex;
	align-items: center;
	padding: 1rem 2rem;
	gap: 1rem;

	background: ${colour(`surfaceContainerLow.elevated.1`)};
	border-radius: 1rem;
	${text({ weight: `bold` })}
	${shadow(`normal`)}
`;
