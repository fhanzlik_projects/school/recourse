import { background, text } from "@recourse/commons";
import { createResource, For } from "solid-js";
import { styled } from "solid-styled-components";
import { screens } from "../components/screen";
import { useApiClient } from "../contexts/api";

export default () => {
	const api = useApiClient();

	const [courses] = createResource(() => api.cms.course.list());

	return (
		<Screen>
			<ScreenHeader>
				<ScreenTitle>Courses</ScreenTitle>
				<ScreenSubtitle>
					Step by step tutorials to guide you through your react
					journey
				</ScreenSubtitle>
			</ScreenHeader>
			<SearchBar placeholder="Search..." />
			<CourseList role="list">
				<For each={courses()}>
					{(course, i) => (
						<>
							{i() > 0 && <CourseSeparator />}
							<CourseListItem>
								<Course>
									<CourseImgWrapper>
										<CourseImg src={course.cover_image} />
									</CourseImgWrapper>
									<CourseDetails>
										<CourseTitle>{course.name}</CourseTitle>
										<CourseSubtitle>
											{course.description}
										</CourseSubtitle>
									</CourseDetails>
								</Course>
							</CourseListItem>
						</>
					)}
				</For>
			</CourseList>
		</Screen>
	);
};

const Screen = styled(screens.Common)`
	padding: 0 16rem;
	gap: 1rem;
`;
const ScreenHeader = styled.header`
	display: flex;
	flex-direction: column;
	padding: 4rem;
	gap: 1rem;
	align-items: center;
`;
const ScreenTitle = styled.h1`
	${text({ size: 2 })}
`;
const ScreenSubtitle = styled.p`
	${text({ size: 2, family: `montserrat`, weight: `bold` })}
`;
const SearchBar = styled.input`
	padding: 1.25rem 2rem;
	${text({ family: `montserrat` })}
	${background(`elevated.1`)}
	border-radius: 0.5rem;
`;
const CourseList = styled.ul`
	list-style: none;
	padding: 3rem 4rem;
	display: flex;
	flex-direction: column;
	gap: 1rem;
`;
const CourseSeparator = styled.div`
	height: 0.125rem;
	display: flex;
	flex-direction: column;
	margin: 0 3rem;
	${background(`elevated.0`)}
`;
const CourseListItem = styled.li`
	height: 8rem;
`;
const Course = styled.article`
	width: 100%;
	height: 100%;
	display: flex;
	border-radius: 1rem;

	/* the image wouldn't respect the border-radius otherwise */
	overflow: hidden;
`;
// the wrapper is needed because for whatever reason `img`s grow bigger than their flex basis, even
// though they have `flex-grow: 0`
const CourseImgWrapper = styled.div`
	flex-basis: 12rem;
	flex-shrink: 0;
`;
const CourseImg = styled.img`
	width: 100%;
	height: 100%;
	object-fit: cover;
`;
const CourseDetails = styled.div`
	flex-grow: 1;
	padding: 1rem;
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
`;
const CourseTitle = styled.h2`
	${text({ weight: `bold`, size: 2 })}
`;
const CourseSubtitle = styled.p`
	${text({ family: `montserrat` })}
`;
