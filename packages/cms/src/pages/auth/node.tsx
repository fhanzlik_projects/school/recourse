import {
	UiNode,
	UiNodeAnchorAttributes,
	UiNodeImageAttributes,
	UiNodeInputAttributes,
	UiNodeScriptAttributes,
	UiNodeTextAttributes,
} from "@ory/client";
import {
	isUiNodeAnchorAttributes,
	isUiNodeImageAttributes,
	isUiNodeInputAttributes,
	isUiNodeScriptAttributes,
	isUiNodeTextAttributes,
} from "@ory/integrations/ui";
import { Match, Switch } from "solid-js";
import { NodeAnchor } from "./NodeAnchor";
import { NodeImage } from "./NodeImage";
import { NodeInput } from "./NodeInput";
import { NodeScript } from "./NodeScript";
import { NodeText } from "./NodeText";

type Props<T> = {
	node: UiNode;
	disabled: boolean;
	value: T;
	setValue: (value: T) => void;
	dispatchSubmit: () => Promise<void>;
};

export const Node = <T,>(props: Props<T>) => {
	return (
		<Switch>
			<Match when={isUiNodeImageAttributes(props.node.attributes)}>
				<NodeImage
					node={props.node}
					attributes={props.node.attributes as UiNodeImageAttributes}
				/>
			</Match>
			<Match when={isUiNodeScriptAttributes(props.node.attributes)}>
				<NodeScript
					node={props.node}
					attributes={props.node.attributes as UiNodeScriptAttributes}
				/>
			</Match>
			<Match when={isUiNodeTextAttributes(props.node.attributes)}>
				<NodeText
					node={props.node}
					attributes={props.node.attributes as UiNodeTextAttributes}
				/>
			</Match>
			<Match when={isUiNodeAnchorAttributes(props.node.attributes)}>
				<NodeAnchor
					node={props.node}
					attributes={props.node.attributes as UiNodeAnchorAttributes}
				/>
			</Match>
			<Match when={isUiNodeInputAttributes(props.node.attributes)}>
				<NodeInput
					dispatchSubmit={props.dispatchSubmit}
					value={props.value}
					setValue={props.setValue}
					node={props.node}
					disabled={props.disabled}
					attributes={props.node.attributes as UiNodeInputAttributes}
				/>
			</Match>
		</Switch>
	);
};
