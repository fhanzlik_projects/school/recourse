import { UiText } from "@ory/client";
import { For } from "solid-js";

export const Message = (props: { message: UiText }) => {
	return (
		<div>
			{props.message.type === `error` ? `error` : `info`}
			<h3>{props.message.text}</h3>
		</div>
	);
};

export const Messages = (props: { messages?: UiText[] }) => {
	return (
		<div>
			<For each={props.messages}>
				{(message) => <Message message={message} />}
			</For>
		</div>
	);
};
