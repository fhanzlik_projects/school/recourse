import { UiNode, UiNodeTextAttributes, UiText } from "@ory/client";
import { For, Match, Switch } from "solid-js";
import { styled } from "solid-styled-components";

type Props = {
	node: UiNode;
	attributes: UiNodeTextAttributes;
};

export const NodeText = (props: Props) => {
	return (
		<>
			<p>{props.node.meta.label?.text}</p>
			<Content node={props.node} attributes={props.attributes} />
		</>
	);
};

const Content = (props: Props) => {
	return (
		<Switch
			fallback={
				<ScrollableCodeBox>
					<code innerText={props.attributes.text.text} />
				</ScrollableCodeBox>
			}
		>
			<Match when={props.attributes.text.id === 1050015}>
				{/* This text node contains lookup secrets. Let's make them a bit more beautiful! */}
				<div class="row">
					<For
						each={
							(
								props.attributes.text.context as {
									secrets: UiText[];
								}
							).secrets
						}
					>
						{(text) => (
							<div
								data-testid={`node/text/${props.attributes.id}/lookup_secret`}
								class="col-xs-3"
							>
								{/* Used lookup_secret has ID 1050014 */}
								<code>
									{text.id === 1050014 ? `Used` : text.text}
								</code>
							</div>
						)}
					</For>
				</div>
			</Match>
		</Switch>
	);
};

const ScrollableCodeBox = styled.pre`
	overflow-x: auto;
`;
