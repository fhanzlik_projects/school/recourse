import { getNodeLabel } from "@ory/integrations/ui";
import {
	colour,
	easing,
	outlineColour,
	runBackgroundAsyncTask,
	stateLayer,
	text,
} from "@recourse/commons";
import { styled } from "solid-styled-components";
import Zod from "zod";
import { NodeInputProps } from "./NodeInput";

export function NodeInputSubmit(props: NodeInputProps<string>) {
	return (
		<Submit
			name={props.attributes.name}
			onClick={(e) => {
				e.stopPropagation();
				e.preventDefault();

				runBackgroundAsyncTask(async () => {
					// On click, we set this value, and once set, dispatch the submission!
					props.setValue(Zod.string().parse(props.attributes.value));
					await props.dispatchSubmit();
				});
			}}
			value={Zod.string().parse(props.attributes.value ?? ``)}
			disabled={props.attributes.disabled || props.disabled}
		>
			{getNodeLabel(props.node)}
		</Submit>
	);
}

const Submit = styled.button`
	border-radius: 50rem;
	padding: 1rem 2rem;
	flex-grow: 1;
	max-width: 12rem;

	${text({ family: `montserrat`, weight: `bold`, colour: `onPrimary` })}
	background: ${colour(`primary`)};
	transition: background-color 0.2s ease-out,
		outline-color 0.2s ${easing.outQuart};
	&:hover,
	&:focus-within {
		background: ${stateLayer(`primary`, `onPrimary`, `hover`)};
	}

	outline: 0.15rem solid transparent;
	outline-offset: 0.2rem;
	&:focus {
		${outlineColour(`focus`)}
	}
`;
