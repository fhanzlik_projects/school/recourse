import {
	background,
	colour,
	css,
	easing,
	stateLayer,
	text,
} from "@recourse/commons";
import { IconProps } from "solid-icons";
import {
	BiRegularCheckCircle,
	BiRegularEnvelope,
	BiRegularKey,
	BiRegularUser,
	BiRegularXCircle,
} from "solid-icons/bi";
import { Component, createMemo, For } from "solid-js";
import { Dynamic } from "solid-js/web";
import { styled, useTheme } from "solid-styled-components";
import { BreakPointName, widthAtLeast } from "../../utils/breakpoints";
import { nested, nestedWithProps } from "../../utils/styles";
import { NodeInputProps } from "./NodeInput";

export const NodeInputDefault = (props: NodeInputProps<string | undefined>) => {
	// Some attributes have dynamic JavaScript - this is for example required for WebAuthn.
	const onClick = () => {
		// This section is only used for WebAuthn. The script is loaded via a <script> node
		// and the functions are available on the global window level. Unfortunately, there
		// is currently no better way than executing eval / function here at this moment.
		if (props.attributes.onclick) {
			// eslint-disable-next-line @typescript-eslint/no-implied-eval
			const run = new Function(props.attributes.onclick);
			run();
		}
	};

	const invalid = createMemo(() =>
		props.node.messages.some(({ type }) => type === `error`),
	);

	// Render a generic text input field.
	return (
		<InputWrapper invalid={invalid()}>
			<InputLabel invalid={invalid()}>
				<Input
					placeholder={props.node.meta.label?.text}
					title={props.node.meta.label?.text}
					onClick={onClick}
					onChange={(e) => props.setValue(e.currentTarget.value)}
					type={props.attributes.type}
					name={props.attributes.name}
					value={
						props.value ??
						// sigh. https://github.com/solidjs/solid/issues/1041
						``
					}
					disabled={props.attributes.disabled || props.disabled}
				/>

				<InputIcon
					component={iconTypes[props.attributes.name]}
					color={useTheme().colours.onSurfaceVariant}
				/>
			</InputLabel>
			<Anchor>
				<ProblemList role="list">
					<For each={props.node.messages}>
						{(rule) => (
							<Rule>
								<RuleIndicator>
									<RuleIndicatorIcon
										visible={rule.type === `success`}
										component={BiRegularCheckCircle}
										color={
											useTheme().colours.foreground
												.success
										}
									/>

									<RuleIndicatorIcon
										visible={rule.type === `error`}
										component={BiRegularXCircle}
										color={
											useTheme().colours.foreground.error
										}
									/>
								</RuleIndicator>
								<RuleText>{rule.text}</RuleText>
							</Rule>
						)}
					</For>
				</ProblemList>
			</Anchor>
		</InputWrapper>
	);
};

const iconTypes: Record<string, Component<IconProps>> = {
	identifier: BiRegularUser,
	email: BiRegularEnvelope,
	password: BiRegularKey,
};

const problemListVisibility = ({ invalid }: { invalid: boolean }) =>
	invalid
		? css`
				visibility: visible;
				opacity: 1;
				transform: initial;
		  `
		: css`
				visibility: hidden;
				opacity: 0;
				transform: translateY(-0.25rem);
		  `;

const tooltipShiftBp: BreakPointName = `medium`;

const ProblemList = styled.ul`
	position: absolute;

	/* needed to keep the tooltip above other inputs, since they are also positioned */
	z-index: 1;

	inset: 0;
	top: 0.5rem;
	bottom: auto;

	display: flex;
	flex-direction: column;
	padding: 0.5rem 1.25rem;
	border-radius: 1rem;
	list-style: none;
	gap: 0.5rem;
	${background(`elevated.0`)}

	${widthAtLeast(tooltipShiftBp)} {
		inset: auto;
		left: 1rem;
		bottom: 0;
		width: 24rem;
	}

	${problemListVisibility({ invalid: false })}
	/*
	 * visibility is not animatable, but needs to be a part of the transition anyway or the element
	 * will be made invisible before the opacity animation finishes, cutting off the animation
	 */
	transition: visibility 0.5s, opacity 0.5s ${easing.outQuart},
		0.5s ${easing.outQuart};
`;

const InputWrapper = styled.div<{
	invalid: boolean;
}>`
	/* on small devices the anchor uses the wrapper as an anchor, so it needs to be positioned too */
	position: relative;

	display: flex;
	flex-direction: row;

	&:hover,
	&:focus-within {
		${nested(ProblemList)} {
			${problemListVisibility}
		}
	}
`;

export const FormFields = styled.div`
	display: flex;
	flex-direction: column;

	&:focus-within {
		${nestedWithProps(InputWrapper, {
			invalid: true,
		})}:not(:focus-within) ${nested(ProblemList)} {
			transition: visibility 0s, opacity 0s, transform 0s;
			${problemListVisibility({ invalid: false })}
		}
	}
`;

const inputHeight = `1.5rem`;
const inputVertPadding = `0.75rem`;

const InputLabel = styled.label<{
	invalid: boolean;
}>`
	flex-grow: 1;
	height: calc(2 * ${inputVertPadding} + ${inputHeight});

	box-shadow: 0 0 0.5rem 0 hsla(0, 0%, 0%, 0.1);
	border-radius: 1rem;
	padding: 0.75rem 2rem;

	outline: 0.15rem solid transparent;
	outline-color: ${({ invalid, theme }) =>
		invalid ? theme?.colours.outline.error : `transparent`};

	${text({ family: `montserrat`, weight: `bold`, colour: `bright` })}
	background: ${colour(`surfaceVariant`)};
	transition: background-color 0.1s ease-out,
		outline-color 0.2s ${easing.outQuart};
	&:hover,
	&:focus-within {
		background: ${stateLayer(`surfaceVariant`, `onSurface`, `hover`)};
	}

	&:focus-within {
		outline-color: ${({ theme }) => theme?.colours.background.primary};
	}

	display: flex;
	align-items: center;
	gap: 0.5rem;
`;

const Input = styled.input`
	flex: 1;
	width: 0;

	height: 1.5em;

	background-color: transparent;
	&:focus {
		outline: none;
	}
`;

const InputIcon = styled(Dynamic<Component<IconProps>>)`
	width: auto;
	height: 100%;
`;

const Anchor = styled.div`
	position: absolute;
	inset: 0;
	top: auto;

	${widthAtLeast(tooltipShiftBp)} {
		inset: 0;
		left: auto;
	}
`;

const Rule = styled.li`
	display: flex;
	align-items: center;
	gap: 0.5rem;
	padding: 0.25rem 0;
`;

const RuleIndicator = styled.div`
	position: relative;
	width: 1.5rem;
	height: 1.5rem;
	display: flex;
	flex-direction: column;
`;
const RuleIndicatorIcon = styled(Dynamic<Component<IconProps>>)<{
	visible: boolean;
}>`
	position: absolute;
	width: 100%;
	height: 100%;

	transition: opacity 0.3s ${easing.outQuart},
		transform 0.3s ${easing.outQuart};
	${({ visible }) =>
		visible
			? `
				opacity: 1;
			`
			: `
				opacity: 0;
				transform: rotate(-90deg);
			`}
`;

const RuleText = styled.span`
	flex-grow: 1;
	${text({ family: `montserrat`, weight: `bold`, colour: `bright` })}
`;
