import {
	LoginFlow,
	RecoveryFlow,
	RegistrationFlow,
	SettingsFlow,
	UiNode,
	UiNodeInputAttributes,
	UpdateLoginFlowBody,
	UpdateRecoveryFlowBody,
	UpdateRegistrationFlowBody,
	UpdateSettingsFlowBody,
	UpdateVerificationFlowBody,
	VerificationFlow,
} from "@ory/client";
import { getNodeId, isUiNodeInputAttributes } from "@ory/integrations/ui";
import { runBackgroundAsyncTask } from "@recourse/commons";
import { createWritableMemo } from "@solid-primitives/memo";
import _ from "lodash";
import { createMemo, createSignal, For, JSX, Show } from "solid-js";
import { styled } from "solid-styled-components";
import Zod from "zod";
import {
	InputWrapper,
	ProblemList,
	problemListVisibility,
} from "../../utils/forms";
import { nested, nestedWithProps } from "../../utils/styles";
import { Node } from "./node";

export type Values = Partial<
	| UpdateLoginFlowBody
	| UpdateRegistrationFlowBody
	| UpdateRecoveryFlowBody
	| UpdateSettingsFlowBody
	| UpdateVerificationFlowBody
>;

export type Methods =
	| "oidc"
	| "password"
	| "profile"
	| "totp"
	| "webauthn"
	| "link"
	| "lookup_secret";

export type Props<T> = {
	// The flow
	flow?:
		| LoginFlow
		| RegistrationFlow
		| SettingsFlow
		| VerificationFlow
		| RecoveryFlow;
	// Only show certain nodes. We will always render the default nodes for CSRF tokens.
	only?: Methods;
	// Is triggered on submission
	onSubmit: (values: T) => Promise<void>;
	// Do not show the global messages. Useful when rendering them elsewhere.
	hideGlobalMessages?: boolean;
};

const zValue = Zod.union([
	Zod.string(),
	Zod.number(),
	Zod.boolean(),
	Zod.undefined(),
]);
const zValueObject = Zod.object({}).catchall(zValue);

export const Flow = <T extends Values>(props: Props<T>) => {
	const [isLoading, setIsLoading] = createSignal(false);

	const nodesFiltered = createMemo((): UiNode[] => {
		const { flow, only } = props;
		if (!flow) {
			return [];
		}
		return flow.ui.nodes.filter(({ group }) => {
			if (!only) {
				return true;
			}
			return group === `default` || group === only;
		});
	});

	const nodesPartitioned = createMemo(() =>
		_(nodesFiltered()).groupBy((e) =>
			e.type === `input` &&
			(e.attributes as UiNodeInputAttributes).type === `submit`
				? `actions`
				: `fields`,
		),
	);

	const [values, setValues] = createWritableMemo<T>(() => {
		const newValues = {} as T & Zod.infer<typeof zValueObject>;

		nodesFiltered().forEach((node) => {
			// This only makes sense for text nodes
			if (isUiNodeInputAttributes(node.attributes)) {
				if (
					node.attributes.type === `button` ||
					node.attributes.type === `submit`
				) {
					// In order to mimic real HTML forms, we need to skip setting the value
					// for buttons as the button value will (in normal HTML forms) only trigger
					// if the user clicks it.
					return;
				}

				newValues[node.attributes.name] = zValue.parse(
					node.attributes.value,
				);
			}
		});

		return newValues;
	});

	// Handles form submission
	const handleSubmit = async () => {
		// Prevent double submission!
		if (isLoading()) {
			return;
		}

		setIsLoading(true);

		await props.onSubmit(values()).finally(() => {
			// We wait for reconciliation and update the state after 50ms
			// Done submitting - update loading status
			setIsLoading(false);
		});
	};

	return (
		<Show
			when={props.flow}
			fallback={
				// No flow was set yet? It's probably still loading...
				//
				// Nodes have only one element? It is probably just the CSRF Token
				// and the filter did not match any elements!
				null
			}
		>
			<Form
				action={props.flow?.ui.action}
				method={props.flow?.ui.method as JSX.HTMLFormMethod}
				onSubmit={(e) => {
					e.stopPropagation();
					e.preventDefault();
					runBackgroundAsyncTask(handleSubmit);
				}}
			>
				{/* {!props.hideGlobalMessages ? (
					<Messages messages={props.flow?.ui.messages} />
				) : null} */}

				<Fields>
					<For each={nodesPartitioned().get(`fields`)}>
						{(node) => {
							const id = getNodeId(node) as Extract<
								keyof T,
								string
							>;
							return (
								<Node
									disabled={isLoading()}
									node={node}
									value={values()[id]}
									dispatchSubmit={handleSubmit}
									setValue={(value) => {
										setValues((values) => ({
											...values,
											[getNodeId(node)]: value,
										}));
									}}
								/>
							);
						}}
					</For>
				</Fields>
				<ButtonRow>
					<For each={nodesPartitioned().get(`actions`)}>
						{(node) => {
							const id = getNodeId(node) as Extract<
								keyof T,
								string
							>;
							return (
								<Node
									disabled={isLoading()}
									node={node}
									value={values()[id]}
									dispatchSubmit={handleSubmit}
									setValue={(value) => {
										setValues((values) => ({
											...values,
											[getNodeId(node)]: value,
										}));
									}}
								/>
							);
						}}
					</For>
				</ButtonRow>
			</Form>
		</Show>
	);
};

const Form = styled.form`
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
`;

const Fields = styled.div`
	display: flex;
	flex-direction: column;
	gap: 0.5rem;

	&:focus-within {
		${nestedWithProps(InputWrapper, {
			invalid: true,
		})}:not(:focus-within) ${nested(ProblemList)} {
			transition: visibility 0s, opacity 0s, transform 0s;
			${problemListVisibility({ invalid: false })}
		}
	}
`;

const ButtonRow = styled.div`
	padding-top: 2rem;
	display: flex;
	justify-content: flex-end;
`;
