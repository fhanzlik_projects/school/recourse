import { getNodeLabel } from "@ory/integrations/ui";
import { logAsyncFailure } from "@recourse/commons";
import Zod from "zod";
import { NodeInputProps } from "./NodeInput";

export function NodeInputButton(props: NodeInputProps<string>) {
	// Some attributes have dynamic JavaScript - this is for example required for WebAuthn.
	const onClick = async (e: Event) => {
		e.stopPropagation();
		e.preventDefault();

		// This section is only used for WebAuthn. The script is loaded via a <script> node
		// and the functions are available on the global window level. Unfortunately, there
		// is currently no better way than executing eval / function here at this moment.
		//
		// Please note that we also need to prevent the default action from happening.
		if (props.attributes.onclick) {
			// eslint-disable-next-line @typescript-eslint/no-implied-eval
			const run = new Function(props.attributes.onclick);
			run();
			return;
		}

		props.setValue(Zod.string().parse(props.attributes.value));
		await props.dispatchSubmit();
	};

	return (
		<>
			<button
				name={props.attributes.name}
				onClick={(e) => {
					logAsyncFailure(onClick(e));
				}}
				value={Zod.string().parse(props.attributes.value ?? ``)}
				disabled={props.attributes.disabled || props.disabled}
			>
				{getNodeLabel(props.node)}
			</button>
		</>
	);
}
