import { UiNode, UiNodeScriptAttributes } from "@ory/client";
import { createEffect, onCleanup } from "solid-js";

type Props = {
	node: UiNode;
	attributes: UiNodeScriptAttributes;
};

export const NodeScript = (props: Props) => {
	createEffect(() => {
		const script = document.createElement(`script`);

		script.async = true;
		script.setAttribute(
			`data-testid`,
			`node/script/${props.attributes.id}`,
		);
		script.src = props.attributes.src;
		script.async = props.attributes.async;
		script.crossOrigin = props.attributes.crossorigin;
		script.integrity = props.attributes.integrity;
		script.referrerPolicy = props.attributes.referrerpolicy;
		script.type = props.attributes.type;

		document.body.appendChild(script);

		onCleanup(() => {
			document.body.removeChild(script);
		});
	});

	return null;
};
