import { UiNode, UiNodeInputAttributes } from "@ory/client";
import { Match, Switch } from "solid-js";
import { NodeInputButton } from "./NodeInputButton";
import { NodeInputCheckbox } from "./NodeInputCheckbox";
import { NodeInputDefault } from "./NodeInputDefault";
import { NodeInputHidden } from "./NodeInputHidden";
import { NodeInputSubmit } from "./NodeInputSubmit";

export type NodeInputProps<T> = {
	node: UiNode;
	attributes: UiNodeInputAttributes;
	value: T;
	disabled: boolean;
	dispatchSubmit: () => Promise<void>;
	setValue: (value: T) => void;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function NodeInput(props: NodeInputProps<any>) {
	return (
		<Switch
			fallback={
				// Render a generic text input field.
				<NodeInputDefault {...props} />
			}
		>
			<Match when={props.attributes.type === `hidden`}>
				{/* Render a hidden input field */}
				<NodeInputHidden {...props} />
			</Match>
			<Match when={props.attributes.type === `checkbox`}>
				{/* Render a checkbox. We have one hidden element which is the real value (true/false), and one display element which is the toggle value (true)! */}
				<NodeInputCheckbox {...props} />;
			</Match>
			<Match when={props.attributes.type === `button`}>
				{/* Render a button */}
				<NodeInputButton {...props} />
			</Match>
			<Match when={props.attributes.type === `submit`}>
				{/* Render the submit button */}
				<NodeInputSubmit {...props} />
			</Match>
		</Switch>
	);
}
