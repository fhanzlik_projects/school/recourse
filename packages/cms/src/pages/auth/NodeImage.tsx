import { UiNode, UiNodeImageAttributes } from "@ory/client";

export const NodeImage = (props: {
	node: UiNode;
	attributes: UiNodeImageAttributes;
}) => {
	return <img src={props.attributes.src} alt={props.node.meta.label?.text} />;
};
