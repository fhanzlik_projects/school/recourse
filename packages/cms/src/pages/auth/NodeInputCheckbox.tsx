import { getNodeLabel } from "@ory/integrations/ui";
import { NodeInputProps } from "./NodeInput";

export function NodeInputCheckbox(props: NodeInputProps<boolean>) {
	// Render a checkbox.s
	return (
		<label>
			{getNodeLabel(props.node)}
			<input
				type="checkbox"
				name={props.attributes.name}
				checked={props.attributes.value === true}
				onChange={(e) => props.setValue(e.currentTarget.checked)}
				disabled={props.attributes.disabled || props.disabled}
			/>
			{props.node.messages.find(({ type }) => type === `error`)
				? `error`
				: undefined}
			{props.node.messages.map(({ text }) => text).join(`\n`)}
		</label>
	);
}
