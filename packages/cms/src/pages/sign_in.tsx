import {
	Configuration,
	FrontendApi,
	LoginFlow,
	UpdateLoginFlowBody,
} from "@ory/client";
import {
	css,
	error,
	logAsyncFailure,
	runBackgroundAsyncTask,
	text,
} from "@recourse/commons";
import { Navigator, useNavigate, useSearchParams } from "@solidjs/router";
import { AxiosError } from "axios";
import { darken, lighten, transparentize } from "polished";
import { createEffect, createSignal, Setter, Show } from "solid-js";
import { styled } from "solid-styled-components";
import Zod from "zod";
import { appUrls } from "../App";
import { imageLogin } from "../assets";
import { screens } from "../components/screen";
import { Flow } from "./auth/flow";

const basePath = `http://localhost:4433`;
const ory = new FrontendApi(
	new Configuration({
		basePath,
		baseOptions: {
			withCredentials: true,
		},
	}),
);

export default () => {
	const [flow, setFlow] = createSignal<LoginFlow>();

	const navigate = useNavigate();
	const [queryParams, setQueryParams] = useSearchParams();
	const logout = getLogoutFn(navigate);

	const [loggedIn, setLoggedIn] = createSignal(false);

	createEffect(() => {
		// if we already have a flow, do nothing.
		if (flow() !== undefined) {
			return;
		}

		const {
			return_to: returnTo,
			flow: flowId,
			// Refresh means we want to refresh the session. This is needed, for example, when we want to update the password
			// of a user.
			refresh,
			// AAL = Authorization Assurance Level. This implies that we want to upgrade the AAL, meaning that we want
			// to perform two-factor authentication/verification.
			aal,
		} = queryParams;

		runBackgroundAsyncTask(async () => {
			try {
				await ory.toSession();
				// the use already has a session! no need to log in again.
				setLoggedIn(true);
				setTimeout(() => {
					console.log(`logging out`);
					logAsyncFailure(logout());
				}, 1000);
				return;
			} catch {
				// the user doesn't have a session. this is usually expected
				console.log(`no session, continuing to login...`);
			}

			if (flowId) {
				try {
					const { data } = await ory.getLoginFlow({
						id: String(flowId),
					});
					setFlow(data);
				} catch (e) {
					handleFlowError(navigate, appUrls.signIn(), setFlow)(e);
				}
				return;
			}

			try {
				const { data } = await ory.createBrowserLoginFlow({
					refresh: Boolean(refresh),
					aal: aal ? String(aal) : undefined,
					returnTo: returnTo ? String(returnTo) : undefined,
				});
				setFlow(data);
			} catch (e) {
				handleFlowError(navigate, appUrls.signIn(), setFlow)(e);
			}
		});
	});

	const submit = async (values: UpdateLoginFlowBody) => {
		const flowData = flow();

		if (flowData === undefined) {
			console.error(
				`can't submit the login form because the login flow was not (yet?) initialised!`,
			);
			return;
		}

		// On submission, add the flow ID to the URL but do not navigate. This prevents the user loosing
		// his data when she/he reloads the page.
		setQueryParams({ flow: flowData.id });

		try {
			await ory.updateLoginFlow({
				flow: String(flowData.id),
				updateLoginFlowBody: values,
			});

			// We logged in successfully! Let's bring the user home.
			if (flowData.return_to) {
				window.location.href = flowData.return_to;
				return;
			}
			navigate(`/`);
		} catch (e) {
			try {
				handleFlowError(navigate, appUrls.signIn(), setFlow)(e);
			} catch (e) {
				const axiosErrorParsed = Zod.object({
					response: Zod.object({
						status: Zod.number(),
						data: Zod.any(),
					}).optional(),
				}).safeParse(e);

				// If the previous handler did not catch the error it's most likely a form validation error
				if (
					axiosErrorParsed.success &&
					axiosErrorParsed.data.response?.status === 400
				) {
					// Yup, it is!
					setFlow(axiosErrorParsed.data.response.data as LoginFlow);
					return;
				}

				throw e;
			}
		}
	};

	return (
		<Container>
			<Show
				when={!loggedIn()}
				fallback={<Heading>Logging out..</Heading>}
			>
				<Heading>Welcome back!</Heading>
				<FormContainer>
					<Flow onSubmit={submit} flow={flow()} />
				</FormContainer>
			</Show>
		</Container>
	);
};

const handleFlowError =
	<S,>(
		navigate: Navigator,
		flowUrl: string,
		resetFlow: Setter<S | undefined>,
	) =>
	(err: unknown) => {
		const errAxios = err as AxiosError<{
			error?: { id: string };
			redirect_browser_to?: string;
		}>;
		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
		switch (errAxios.response?.data.error?.id) {
			case `session_aal2_required`:
				// 2FA is enabled and enforced, but user did not perform 2fa yet!
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
				window.location.href =
					errAxios.response.data.redirect_browser_to ??
					error.unexpectedNullish();
				return;
			case `session_already_available`:
				// User is already signed in, let's redirect them home!
				navigate(flowUrl);
				return;
			case `session_refresh_required`:
				// We need to re-authenticate to perform this action
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
				window.location.href =
					errAxios.response.data.redirect_browser_to ??
					error.unexpectedNullish();
				return;
			case `self_service_flow_return_to_forbidden`:
				// The flow expired, let's request a new one.
				console.error(`The return_to address is not allowed.`);
				resetFlow(undefined);
				navigate(flowUrl);
				return;
			case `self_service_flow_expired`:
				// The flow expired, let's request a new one.
				console.error(
					`Your interaction expired, please fill out the form again.`,
				);
				resetFlow(undefined);
				navigate(flowUrl);
				return;
			case `security_csrf_violation`:
				// A CSRF violation occurred. Best to just refresh the flow!
				console.error(
					`A security violation was detected, please fill out the form again.`,
				);
				resetFlow(undefined);
				navigate(flowUrl);
				return;
			case `security_identity_mismatch`:
				// The requested item was intended for someone else. Let's request a new flow...
				resetFlow(undefined);
				navigate(flowUrl);
				return;
			case `browser_location_change_required`:
				// Ory Kratos asked us to point the user to this URL.
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access
				window.location.href =
					errAxios.response.data.redirect_browser_to ??
					error.unexpectedNullish();
				return;
		}

		switch (errAxios.response?.status) {
			case 410:
				// The flow expired, let's request a new one.
				resetFlow(undefined);
				navigate(flowUrl);
				return;
		}

		// We are not able to handle the error? Return it.
		throw errAxios;
	};

// Returns a function which will log the user out
const getLogoutFn = (navigate: Navigator) => {
	let logoutToken: string | undefined;

	ory.createBrowserLogoutFlow()
		.then(({ data }) => {
			logoutToken = data.logout_token;
		})
		.catch((err: AxiosError) => {
			switch (err.response?.status) {
				case 401:
					// do nothing, the user is not logged in
					return;
			}

			// Something else happened!
			return Promise.reject(err);
		});

	return async () => {
		if (logoutToken === undefined) {
			error(
				`attempted to log out, but the logout token is not (yet?) available.`,
			);
		}

		await ory.updateLogoutFlow({ token: logoutToken });
		navigate(`/`);
	};
};

const Container = styled(screens.Common)`
	padding: 3rem;

	${({
		theme: { colours: { background } } = error(`theme not provided`),
	}) => css`
		background: linear-gradient(
				to right,
				${background.normal} 30%,
				${transparentize(0.3, background.normal)}
			),
			/* trickery to make the gradient appear less banded (my attempts at dithering looked bad) */
				linear-gradient(
					30deg,
					${transparentize(0.9, lighten(1, background.normal))},
					${transparentize(0.9, darken(1, background.normal))}
				),
			linear-gradient(
				60deg,
				${transparentize(0.9, lighten(1, background.normal))},
				${transparentize(0.9, darken(1, background.normal))}
			),
			url(${imageLogin}) center/cover;
	`}

	justify-content: center;
`;

const Heading = styled.h1`
	${text({ family: `montserrat`, weight: `bold`, colour: `bright`, size: 2 })}

	padding-bottom: 3rem;
`;

const FormContainer = styled.div`
	max-width: 25rem;
`;
