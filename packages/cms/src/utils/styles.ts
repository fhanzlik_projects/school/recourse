import { error, Theme } from "@recourse/commons";

/**
 * helper for using styled components as part of selectors in other styled components
 *
 * this usage is under normal circumstances natively supported by `solid-styled-components`[^1], but due to a bug in the typings, this helper is needed when:
 * - the containing component takes custom props, and
 * - the containing component wraps a built-in tag, and
 * - the nested component wraps a different tag than the containing one
 *
 * [^1]: https://github.com/solidjs/solid-styled-components#nesting-styled-components
 *
 * @example
 *
 * // doesn't compile:
 * const Wrapper = styled.div<{ myProp: string }>`
 * 	color: red;
 *
 * 	${styled.span`color: blue;`.class} {}
 * `
 *
 * // does
 * const Wrapper = styled.div<{ myProp: string }>`
 * 	color: red;
 *
 * 	${nested(styled.span`color: blue;`)} {}
 * `
 */
export const nested =
	(Component: { class: (props: Record<never, never>) => string }) =>
	({ theme }: { theme?: Theme }) =>
		Component.class({ theme });

/**
 * variant of {@link nested} which can supply additional props to the nested component apart from the theme
 *
 * @example
 *
 * const Nested = styled.span<{ c: string }>`
 * 	color: ${({ c }) => c};
 * `
 *
 * const Wrapper = styled.div<{ myProp: string }>`
 * 	color: red;
 *
 * 	${nested(Nested, { c: "blue" })} {
 * 		color: green;
 * 	}
 * `
 */
export const nestedWithProps =
	<T>(
		Component: {
			class: ({ theme }: { theme: Theme } & T) => string;
		},
		props: T,
	) =>
	({ theme = error(`theme not provided`) }) =>
		Component.class({ theme, ...props });
