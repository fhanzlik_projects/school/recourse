const lowerBounds = new Map([
	[`small`, 800],
	[`medium`, 1200],
	[`large`, 1900],
] as const);

export type BreakPointName = Parameters<typeof lowerBounds["has"]>[0];

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const breakPointSize = (name: BreakPointName) => lowerBounds.get(name)!;

export const widthAtLeast = (bp: BreakPointName) =>
	`@media (min-width: ${breakPointSize(bp)}px)`;
