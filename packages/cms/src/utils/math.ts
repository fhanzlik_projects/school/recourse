import { Bezier } from "bezier-js";

export const splitMultiple = (curve: Bezier, splits: number[]) =>
	splits.reduce<Bezier[]>(
		(acc, split) => {
			const last = acc.splice(-1, 1)[0];
			const sides = last.split(split);
			return [...acc, sides.left, sides.right];
		},
		[curve],
	);

export const elipsisWidthAt = (
	origin: { x: number; y: number },
	point: { x: number; y: number },
	ratio: number,
) => {
	const a = Math.ceil(
		Math.sqrt(
			(point.x - origin.x) ** 2 + (point.y - origin.y) ** 2 / ratio ** 2,
		),
	);
	const b = a * ratio;

	return `ellipse(${a}% ${b}% at ${origin.x}% ${origin.y}%)`;
};

export const bezier2css = (c: Bezier) =>
	`cubic-bezier(${c.point(1).x}, ${c.point(1).y}, ${c.point(2).x}, ${
		c.point(2).y
	})`;

export const bezier2svg = (c: Bezier) =>
	`M ${c.points[0].x},${c.points[0].y} C ${c.points[1].x},${c.points[1].y} ${c.points[2].x},${c.points[2].y} ${c.points[3].x},${c.points[3].y}`;
