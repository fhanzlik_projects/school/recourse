import { background, css, easing, text } from "@recourse/commons";
import { IconProps, IconTypes } from "solid-icons";
import { BiRegularCheckCircle, BiRegularXCircle } from "solid-icons/bi";
import { Component, createEffect, For, JSX, Signal, untrack } from "solid-js";
import { createStore } from "solid-js/store";
import { Dynamic } from "solid-js/web";
import { styled, useTheme } from "solid-styled-components";
import { BreakPointName, widthAtLeast } from "./breakpoints";
import { nested, nestedWithProps } from "./styles";

// wow, as speeling misteik

type Rule<T> = readonly [name: string, validator: (value: T) => boolean];
type ValidatedRule = { name: string; satisfied: boolean };

type Field = () => JSX.Element;

type FormBuilderDataConstraint = Record<PropertyKey, Field>;

type BuildUpstream<T> = () => { validators: (() => boolean)[]; fields: T };

export class FormBuilder<T extends FormBuilderDataConstraint> {
	private buildUpstream: BuildUpstream<T>;

	constructor(buildUpstream: BuildUpstream<T>) {
		this.buildUpstream = buildUpstream;
	}

	public static readonly create = () =>
		new FormBuilder(() => ({ validators: [], fields: {} }));

	public readonly string = <Name extends PropertyKey>(
		name: Name,
		label: string,
		icon: IconTypes,
		[value, setValue]: Signal<string>,
		rules: Rule<T & Record<Name, string>>[],
	) => {
		const [rulesValidated, setRulesValidated] = createStore<{
			rules: ValidatedRule[] | undefined;
		}>({ rules: undefined });
		const { validators, fields } = this.buildUpstream();

		const elemenetRef: { current: HTMLInputElement | undefined } = {
			current: undefined,
		};

		const invalid = () =>
			rulesValidated.rules?.some((rule) => !rule.satisfied) ?? false;

		const valid = () =>
			rulesValidated.rules?.every((rule) => rule.satisfied) ?? false;

		const validateBound = (): boolean => {
			if (elemenetRef.current === undefined) return true;

			const data = { ...fields, [name]: elemenetRef.current.value };

			// if we haven't yet validated any rules (e.g. because the user hasn't touched the input yet)
			if (rulesValidated.rules === undefined) {
				setRulesValidated(
					`rules`,
					rules.map(([name, validate]) => ({
						name,
						satisfied: validate(data),
					})),
				);
			} else {
				rules.forEach(([_, validate], i) => {
					setRulesValidated(`rules`, i, {
						satisfied: validate(data),
					});
				});
			}

			return valid();
		};

		createEffect(() => {
			if (value().length > 0) untrack(validateBound);
		});

		const field: Field = () => (
			<InputWrapper invalid={invalid()}>
				<InputLabel invalid={invalid()}>
					<Input
						ref={elemenetRef.current}
						value={value()}
						onInput={(e) => {
							setValue(e.currentTarget.value);
						}}
						placeholder={label}
					/>
					<InputIcon
						component={icon}
						color={useTheme().colours.foreground.bright}
					/>
				</InputLabel>
				<Anchor>
					<ProblemList role="list">
						<For each={rulesValidated.rules}>
							{(rule) => (
								<Rule>
									<RuleIndicator>
										<RuleIndicatorIcon
											visible={rule.satisfied}
											component={BiRegularCheckCircle}
											color={
												useTheme().colours.foreground
													.success
											}
										/>

										<RuleIndicatorIcon
											visible={!rule.satisfied}
											component={BiRegularXCircle}
											color={
												useTheme().colours.foreground
													.error
											}
										/>
									</RuleIndicator>
									<RuleText>{rule.name}</RuleText>
								</Rule>
							)}
						</For>
						{/* </Transition> */}
					</ProblemList>
				</Anchor>
			</InputWrapper>
		);

		return new FormBuilder<
			T & {
				[Key in Name]: Field;
			}
		>(() => ({
			validators: [...validators, validateBound],
			fields: { ...fields, [name]: field },
		}));
	};

	public readonly build = (submit: () => void) => {
		const { fields, validators } = this.buildUpstream();

		return {
			fields,
			Root: (props: { children: JSX.Element }) => {
				return (
					<Form
						noValidate
						onSubmit={(e) => {
							e.preventDefault();

							if (validators.every((v) => v())) {
								submit();
							}
						}}
					>
						{props.children}
					</Form>
				);
			},
		};
	};
}

export const problemListVisibility = ({ invalid }: { invalid: boolean }) =>
	invalid
		? css`
				visibility: visible;
				opacity: 1;
				transform: initial;
		  `
		: css`
				visibility: hidden;
				opacity: 0;
				transform: translateY(-0.25rem);
		  `;

const tooltipShiftBp: BreakPointName = `medium`;

export const ProblemList = styled.ul`
	position: absolute;

	/* needed to keep the tooltip above other inputs, since they are also positioned */
	z-index: 1;

	inset: 0;
	top: 0.5rem;
	bottom: auto;

	padding: 0.5rem 1.25rem;
	border-radius: 1rem;
	list-style: none;
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
	${background(`elevated.0`)}

	${widthAtLeast(tooltipShiftBp)} {
		inset: auto;
		left: 1rem;
		bottom: 0;
		width: 24rem;
	}

	${problemListVisibility({ invalid: false })}
	/*
	 * visibility is not animatable, but needs to be a part of the transition anyway or the element
	 * will be made invisible before the opacity animation finishes, cutting off the animation
	 */
	transition: visibility 0.5s, opacity 0.5s ${easing.outQuart},
		0.5s ${easing.outQuart};
`;

export const InputWrapper = styled.div<{
	invalid: boolean;
}>`
	/* on small devices the anchor uses the wrapper as an anchor, so it needs to be positioned too */
	position: relative;
	display: flex;

	&:hover,
	&:focus-within {
		${nested(ProblemList)} {
			${problemListVisibility}
		}
	}
`;

export const FormFields = styled.div`
	display: flex;
	flex-direction: column;

	&:focus-within {
		${nestedWithProps(InputWrapper, {
			invalid: true,
		})}:not(:focus-within) ${nested(ProblemList)} {
			transition: visibility 0s, opacity 0s, transform 0s;
			${problemListVisibility({ invalid: false })}
		}
	}
`;

const Form = styled.form`
	display: flex;
	flex-direction: column;
	gap: 0.5rem;
`;

const inputHeight = `1.5rem`;
const inputVertPadding = `0.75rem`;

const InputLabel = styled.label<{
	invalid: boolean;
}>`
	flex-grow: 1;
	height: calc(2 * ${inputVertPadding} + ${inputHeight});

	${text({ family: `montserrat`, weight: `bold`, colour: `bright` })}
	${background(`elevated.1`)}
	box-shadow: 0 0 0.5rem 0 hsla(0, 0%, 0%, 0.1);
	border-radius: 1rem;
	padding: 0.75rem 2rem;

	outline: 0.15rem solid transparent;
	outline-color: ${({ invalid, theme }) =>
		invalid ? theme?.colours.outline.error : `transparent`};

	transition: background-color 0.1s ease-out,
		outline-color 0.2s ${easing.outQuart};
	&:hover,
	&:focus-within {
		${background(`highlight.elevated.1`)}
	}

	&:focus-within {
		outline-color: ${({ theme }) => theme?.colours.background.primary};
	}

	display: flex;
	align-items: center;
	gap: 0.5rem;
`;

const Input = styled.input`
	flex: 1;
	width: 0;

	height: 1.5em;

	color: inherit;
	background-color: inherit;
	&:focus {
		outline: none;
	}
`;

const InputIcon = styled(Dynamic<Component<IconProps>>)`
	width: auto;
	height: 100%;
`;

const Anchor = styled.div`
	position: absolute;
	inset: 0;
	top: auto;

	${widthAtLeast(tooltipShiftBp)} {
		inset: 0;
		left: auto;
	}
`;

const Rule = styled.li`
	display: flex;
	align-items: center;
	gap: 0.5rem;
	padding: 0.25rem 0;
`;

const RuleIndicator = styled.div`
	position: relative;
	width: 1.5rem;
	height: 1.5rem;
`;
const RuleIndicatorIcon = styled(Dynamic<Component<IconProps>>)<{
	visible: boolean;
}>`
	position: absolute;
	width: 100%;
	height: 100%;

	transition: opacity 0.3s ${easing.outQuart},
		transform 0.3s ${easing.outQuart};
	${({ visible }) =>
		visible
			? `
				opacity: 1;
			`
			: `
				opacity: 0;
				transform: rotate(-90deg);
			`}
`;

const RuleText = styled.span`
	flex-grow: 1;
	${text({ family: `montserrat`, weight: `bold`, colour: `bright` })}
`;
