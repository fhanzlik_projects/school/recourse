import "./global_styles";

import { button, globalStyles, themes } from "@recourse/commons";
import { MetaProvider } from "@solidjs/meta";
import { Link, Route, Router, Routes } from "@solidjs/router";
import { JSX, lazy } from "solid-js";
import {
	createGlobalStyles,
	styled,
	ThemeProvider,
	useTheme,
} from "solid-styled-components";
import { Toaster } from "solid-toast";
import { screens } from "./components/screen/index.jsx";
import { ApiClientProvider } from "./contexts/api.jsx";

const swapAnimation = async (prev: Element, _next: Element): Promise<void> => {
	if (prev instanceof HTMLElement) prev.style.zIndex = `1`;

	const cover = (
		<div
			style={{
				position: `fixed`,
				inset: `0`,
				"background-color": useTheme().colours.background.normal,
			}}
		/>
	) as HTMLDivElement;
	prev.parentElement!.appendChild(cover);

	// await next.animate(
	// 	{
	// 		clipPath: [
	// 			elipsisWidthAt({ x: 0, y: 0 }, { x: 0, y: 0 }, 1),
	// 			elipsisWidthAt({ x: 0, y: 0 }, { x: 100, y: 100 }, 1),
	// 		],
	// 		easing: bezier2css(baseCurve),
	// 	},
	// 	5000,
	// ).finished;
	await Promise.all([
		// prev.animate(
		// 	{
		// 		clipPath: [
		// 			elipsisWidthAt({ x: 50, y: 0 }, { x: 100, y: 100 }, 1 / 2),
		// 			elipsisWidthAt({ x: 50, y: 0 }, { x: 0, y: 0 }, 1 / 2),
		// 		],
		// 		offset: [0, 1],
		// 		easing: bezier2css(baseCurve),
		// 	},
		// 	5000,
		// ).finished,
		prev
			.animate(
				{
					transform: [
						`initial`,
						`translateX(-10%) scaleX(120%)`,
						`translateX(100%) scaleX(100%)`,
					],

					offset: [0, 0.6, 1],

					easing: [
						`cubic-bezier(0.76, 0, 0.24, 1)`,
						`cubic-bezier(0.5, 0, 0.75, 0)`,
					],
				},
				500,
			)
			.finished.then(async () => {
				prev.remove();
				await cover.animate(
					{
						opacity: [1, 0],

						easing: `ease`,
					},
					1000,
				).finished;

				cover.remove();
			}),
		prev.animate(
			{
				opacity: [1, 0],
				offset: [0.8, 1],

				easing: `ease`,
			},
			2000,
		),
	]);
};

export const appUrls = {
	signIn: () => `/auth/sign_in`,
};

const pages = {
	// vite dislikes backticks
	/* eslint-disable @typescript-eslint/quotes */
	signIn: lazy(() => import("./pages/sign_in")),
	courses: lazy(() => import("./pages/courses")),
	contentManager: {
		collections: {
			index: lazy(() => import("./pages/content-manager/collections")),
			$collection: {
				layout: lazy(
					() =>
						import(
							"./pages/content-manager/collections/:collectionId/layout"
						),
				),
				index: lazy(
					() =>
						import(
							"./pages/content-manager/collections/:collectionId"
						),
				),
				items: {
					$item: lazy(
						() =>
							import(
								"./pages/content-manager/collections/:collectionId/items/:itemId"
							),
					),
				},
			},
		},
	},
	/* eslint-enable @typescript-eslint/quotes */
};

export const App = () => {
	// Solid.createEffect(async () => {
	// 	const api = await initializeApi();
	// 	console.log((await api.getCourses()).map((item) => item.name));
	// });

	return (
		<Providers>
			<OurToaster />
			{/* <Observer swap={swapAnimation}> */}
			<Routes>
				<Route
					path="/"
					component={() => (
						<XScreen>
							<XLink href={appUrls.signIn()}>sign in</XLink>
							<XLink href="/courses">courses</XLink>
						</XScreen>
					)}
				/>
				<Route path="/content-manager/collections">
					<Route
						path="/"
						component={pages.contentManager.collections.index}
					/>
					<Route
						path="/:collectionId"
						component={
							pages.contentManager.collections.$collection.layout
						}
					>
						<Route
							path="/"
							component={
								pages.contentManager.collections.$collection
									.index
							}
						/>
						<Route
							path="/items/:itemId"
							component={
								pages.contentManager.collections.$collection
									.items.$item
							}
						/>
					</Route>
				</Route>
				<Route path={appUrls.signIn()} component={pages.signIn} />
				<Route path="/courses" component={pages.courses} />
			</Routes>
			{/* </Observer> */}
		</Providers>
	);
};

const GlobalStyles = () => {
	const Styles = createGlobalStyles(globalStyles({ theme: useTheme() }));
	return <Styles />;
};

const Providers = (props: { children: JSX.Element }) => {
	return (
		<MetaProvider>
			<Router>
				<ApiClientProvider>
					<ThemeProvider theme={themes.dark}>
						<GlobalStyles />
						{props.children}
					</ThemeProvider>
				</ApiClientProvider>
			</Router>
		</MetaProvider>
	);
};

const XLink = styled(Link)`
	${button({ colour: `primary` })}
	width: 20rem;
`;
const XScreen = styled(screens.Common)`
	padding: 5rem;
	gap: 1rem;
`;

const OurToaster = () => {
	const theme = useTheme();

	return (
		<Toaster
			toastOptions={{
				style: {
					background: theme.colours[`inverseSurface.elevated.3`],
					color: theme.colours.inverseOnSurface,
				},
			}}
		/>
	);
};

// const Observer = (props: {
// 	children: JSX.Element;
// 	swap?: (prev: Element, next: Element) => Promise<void>;
// }) => {
// 	const children = Solid.children(() => props.children);

// 	const [elems, setElems] = createStore<JSX.Element[]>([]);

// 	createComputed(
// 		on<ResolvedChildren, Element>(children, (next, _, prev) => {
// 			const onExit = props.swap ?? (() => Promise.resolve());

// 			if (!(next === undefined || next instanceof Element)) {
// 				return error("child must be regular element!");
// 			}

// 			if (next != prev) {
// 				setElems((e) => [...e, next]);

// 				if (prev !== undefined && next !== undefined) {
// 					onExit(prev, next).then(() => {
// 						setElems((e) => e.filter((e) => e !== prev));
// 					}, errorLogging);
// 				}
// 			}

// 			return next;
// 		}),
// 	);

// 	return <For each={elems}>{(elem) => elem}</For>;
// };
