/* @refresh reload */
import { error } from "@recourse/commons";
import { render } from "solid-js/web";
import { App } from "./App";

render(
	App,
	document.getElementById(`root`) ??
		error(`the root element does not exist!`),
);
