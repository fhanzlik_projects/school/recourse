import { ApiClient, initializeApi } from "@recourse/api";
import { error } from "@recourse/commons";
import { createContext, JSX, useContext } from "solid-js";

const apiContext = createContext<ApiClient>();

export const useApiClient = () => {
	return (
		useContext(apiContext) ??
		error(
			`attempted to retrieve an API client even though its provider did not render at this component depth.`,
		)
	);
};

export const ApiClientProvider = (props: { children: JSX.Element }) => {
	const client = initializeApi();

	return (
		<apiContext.Provider value={client}>
			{props.children}
		</apiContext.Provider>
	);
};
