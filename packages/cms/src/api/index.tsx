/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-argument */

import { error } from "@recourse/commons";
import { For } from "solid-js";

type CacheKey = string;

const entity = <T,>() => {
	const items = new Map<CacheKey, T>();

	const that = {
		store: (key: CacheKey, item: T) => items.set(key, item),
		storeAll: (newItems: Map<CacheKey, T>) =>
			newItems.forEach((value, key) => that.store(key, value)),
		get: (key: CacheKey) => items.get(key) ?? error(`no such item!`),
	};

	return that;
};

const user = entity<{ name: string; posts: CacheKey[] }>();
const post = entity<{ title: string }>();

const fetchUsers = async (): Promise<CacheKey[]> => {
	// @ts-expect-error todo
	const { results, users, posts } = await fetch(`/endpoint`);

	user.storeAll(users);
	post.storeAll(posts);

	return results;
};

const MyComp = async () => {
	const users = await fetchUsers();

	return (
		<For each={users.map(user.get)}>
			{(user) => (
				<div>
					{user.name}
					<ul>
						<For each={user.posts.map(post.get)}>
							{(post) => <li>{post.title}</li>}
						</For>
					</ul>
				</div>
			)}
		</For>
	);
};
