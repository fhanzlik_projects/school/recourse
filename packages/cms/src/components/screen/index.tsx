import { background } from "@recourse/commons";
import { JSX } from "solid-js";
import { styled } from "solid-styled-components";
import { Header } from "./header";

export const screens = {
	Custom: (props: {
		children: JSX.Element;
		class?: string;
		style?: string | JSX.CSSProperties;
	}) => (
		<Container class={props.class} style={props.style}>
			{props.children}
		</Container>
	),
	Common: (props: { children: JSX.Element; class?: string }) => (
		<screens.Custom>
			<Main class={props.class}>{props.children}</Main>
		</screens.Custom>
	),
	HeaderOnly: (props: { children: JSX.Element }) => (
		<screens.Custom>
			<Header />

			{props.children}
		</screens.Custom>
	),
};

const Container = styled.div`
	min-width: 100vw;
	min-height: 100vh;

	display: flex;
	flex-direction: column;
`;

const Main = styled.div`
	flex-grow: 1;

	display: flex;
	flex-direction: column;

	box-shadow: inset 0 1rem 1rem -1rem hsla(0, 0%, 0%, 0.1);
	${background(`normal`)}
`;
