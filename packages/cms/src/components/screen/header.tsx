import { background, error, text } from "@recourse/commons";
import { Link as UnstyledLink } from "@solidjs/router";
import { styled } from "solid-styled-components";
import logoIconSrc from "../../assets/logos/icon.svg?raw";

export const Header = () => (
	<HeaderContainer>
		<Nav>
			{/* eslint-disable-next-line solid/no-innerhtml */}
			<LogoIcon href="/" innerHTML={logoIconSrc} />

			<Link href="/courses">Courses</Link>
		</Nav>
		<Link href="/auth/sign_in">Sign in</Link>
	</HeaderContainer>
);

const headerHeight = `4rem`;

const HeaderContainer = styled.header`
	position: sticky;
	top: 0;
	box-shadow: 0 0rem 0.5rem 0 hsla(0, 0%, 0%, 0.5);
	justify-content: space-between;
	flex-basis: ${headerHeight};

	display: flex;
	width: 100%;
	${background(`normal`)}
`;

const Nav = styled.nav`
	display: flex;
`;

const Link = styled(UnstyledLink)`
	${text({ weight: `bold` })}

	display: flex;
	align-items: center;

	padding: 1rem;
`;

const LogoIcon = styled(UnstyledLink)`
	padding: 0.5rem;
	color: ${(p) =>
		p.theme?.colours.background.primary ?? error.unexpectedNullish()};
`;
