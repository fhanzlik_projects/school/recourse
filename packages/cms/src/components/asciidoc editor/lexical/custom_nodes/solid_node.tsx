import { assert } from "@recourse/commons";
import { EditorConfig, ElementNode } from "lexical";
import { createRoot, JSX } from "solid-js";

export abstract class SolidNode extends ElementNode {
	#disposer: undefined | (() => void);
	private readonly dispose = () => this.getLatest().#disposer?.();

	public abstract render(config: EditorConfig): JSX.Element;

	override createDOM(config: EditorConfig): HTMLElement {
		this.dispose();
		return createRoot((disposer) => {
			this.getWritable().#disposer = disposer;
			const element = this.render(config);
			assert(
				element instanceof HTMLElement,
				"the render method should only return `HTMLElement`s!",
			);
			return element;
		});
	}

	override remove(preserveEmptyParent?: boolean | undefined): void {
		this.dispose();
		super.remove(preserveEmptyParent);
	}
}
