import { assert } from "@recourse/commons";
import {
	$applyNodeReplacement,
	$getSelection,
	$isElementNode,
	$isRangeSelection,
	$isRootOrShadowRoot,
	EditorConfig,
	ElementNode,
	LexicalNode,
	RangeSelection,
	SerializedElementNode,
	SerializedLexicalNode,
	Spread,
} from "lexical";
import { EditorThemeClassName } from "lexical/LexicalEditor";
import { createRoot } from "solid-js";
import { customElementPrefix } from ".";
import { $createQuizNode, $isQuizNode, QuizNode } from "./quiz";
import { $createQuizAnswerNode, QuizAnswerNode } from "./quiz_answer";

const nodeVersion = 1;
const nodeType = `${customElementPrefix}/quiz/question` as const;

export class QuizQuestionNode extends ElementNode {
	static readonly classes = {
		container: `${nodeType}-container`,
	} as const;

	static override getType(): typeof nodeType {
		return nodeType;
	}

	static override clone(node: QuizQuestionNode): QuizQuestionNode {
		return new QuizQuestionNode(node.__key);
	}

	private dispose: undefined | (() => void);
	override createDOM(config: EditorConfig): HTMLElement {
		this.dispose?.();
		return createRoot((dispose) => {
			this.dispose = dispose;
			return (
				<div class={config.theme[QuizQuestionNode.classes.container]} />
			) as HTMLElement;
		});
	}

	override updateDOM(
		_prevNode: QuizQuestionNode,
		_dom: HTMLElement,
		_config: EditorConfig,
	): boolean {
		return false;
	}

	override remove(preserveEmptyParent?: boolean | undefined): void {
		this.dispose?.();
		super.remove(preserveEmptyParent);
	}

	public getParentQuiz(): QuizNode {
		const parent = this.getParentOrThrow();

		assert(
			$isQuizNode(parent),
			`${QuizQuestionNode.getType()} must always be a child of ${QuizNode.getType()}`,
		);

		return parent;
	}

	override insertNewAfter(
		_: RangeSelection,
		restoreSelection = true,
	): QuizAnswerNode {
		const newElement = $createQuizAnswerNode();

		this.insertAfter(newElement, restoreSelection);

		return newElement;
	}

	override isParentRequired(): true {
		return true;
	}

	override createParentElementNode(): ElementNode {
		return $createQuizNode();
	}

	static override importJSON(
		serializedNode: SerializedLexicalNode,
	): QuizQuestionNode {
		assert(
			serializedNode.type === nodeType,
			`we shouldn't be asked to deserialize nodes with incorrect types`,
		);
		assert(
			serializedNode.version > nodeVersion,
			`can't import future nodes`,
		);

		return $createQuizQuestionNode();
	}

	override exportJSON(): SerializedQuizQuestionNode {
		return {
			...super.exportJSON(),
			type: nodeType,
			version: nodeVersion,
		};
	}
}
export const $createQuizQuestionNode = (): QuizQuestionNode =>
	$applyNodeReplacement(new QuizQuestionNode());

export const $isQuizQuestionNode = (
	node: LexicalNode | undefined | null,
): node is QuizQuestionNode => node instanceof QuizQuestionNode;

export type SerializedQuizQuestionNode = Spread<
	{
		type: typeof nodeType;
		version: typeof nodeVersion;
	},
	SerializedElementNode
>;
export type QuizQuestionNodeThemeClasses = {
	[QuizQuestionNode.classes.container]: EditorThemeClassName;
};

export const $handleQuizQuestionInsertParagraph = (): boolean => {
	const selection = $getSelection();

	if (!$isRangeSelection(selection) || !selection.isCollapsed()) {
		return false;
	}

	const question = selection.anchor.getNode();

	// only run this code on empty quiz questions
	if (!$isQuizQuestionNode(question) || question.getTextContent() !== ``) {
		return false;
	}

	const quiz = question.getParentQuiz();

	if (!$isRootOrShadowRoot(quiz.getParent())) {
		return false;
	}

	const nextSibling = question.getNextSibling();
	if ($isElementNode(nextSibling)) nextSibling.select();

	return true;
};
