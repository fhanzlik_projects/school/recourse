import { assert } from "@recourse/commons";
import {
	$applyNodeReplacement,
	EditorConfig,
	ElementNode,
	LexicalNode,
	SerializedElementNode,
	SerializedLexicalNode,
	Spread,
} from "lexical";
import { EditorThemeClassName } from "lexical/LexicalEditor";
import { createRoot } from "solid-js";
import { customElementPrefix } from ".";
import { findEditor } from "../utils";
import {
	$createQuizAnswerNode,
	$isQuizAnswerNode,
	QuizAnswerNode,
} from "./quiz_answer";

const nodeVersion = 1;
const nodeType = `${customElementPrefix}/quiz/answer/correct` as const;

export class QuizAnswerCorrectNode extends ElementNode {
	static readonly classes = {
		checkbox: `${nodeType}-checkbox`,
	} as const;

	static override getType(): typeof nodeType {
		return nodeType;
	}

	static override clone(node: QuizAnswerCorrectNode): QuizAnswerCorrectNode {
		return new QuizAnswerCorrectNode(node.__key);
	}

	public getParentAnswer(): QuizAnswerNode {
		const parent = this.getParentOrThrow();

		assert(
			$isQuizAnswerNode(parent),
			`${this.getType()} must always be a child of ${QuizAnswerNode.getType()}, but the parent is ${parent.getType()} instead`,
		);

		return parent;
	}

	private readonly getValue = () => this.getParentAnswer().getCorrect();
	private readonly setValue = (value: boolean) =>
		this.getParentAnswer().setCorrect(value);

	private dispose: undefined | (() => void);
	private correctInputRef!: HTMLInputElement;
	override createDOM(config: EditorConfig): HTMLElement {
		this.dispose?.();
		return createRoot((dispose) => {
			this.dispose = dispose;
			return (
				<span contentEditable={false}>
					<input
						ref={this.correctInputRef}
						class={
							config.theme[QuizAnswerCorrectNode.classes.checkbox]
						}
						type="checkbox"
						checked={this.getValue()}
						onChange={(e) =>
							findEditor(this.correctInputRef)?.update(() =>
								this.setValue(e.currentTarget.checked),
							)
						}
					/>
				</span>
			) as HTMLElement;
		});
	}

	override updateDOM(
		prevNode: QuizAnswerCorrectNode,
		_dom: HTMLElement,
		_config: EditorConfig,
	): boolean {
		if (prevNode.getValue() !== this.getValue()) {
			this.correctInputRef.checked = this.getValue();
			return true;
		}
		return false;
	}
	override remove(preserveEmptyParent?: boolean | undefined): void {
		this.dispose?.();
		super.remove(preserveEmptyParent);
	}

	override isParentRequired(): true {
		return true;
	}

	override createParentElementNode(): ElementNode {
		return $createQuizAnswerNode();
	}

	static override importJSON(
		serializedNode: SerializedLexicalNode,
	): QuizAnswerCorrectNode {
		assert(
			serializedNode.type === nodeType,
			`we shouldn't be asked to deserialize nodes with incorrect types`,
		);
		assert(
			serializedNode.version > nodeVersion,
			`can't import future nodes`,
		);

		return $createQuizAnswerCorrectNode();
	}

	override exportJSON(): SerializedQuizAnswerCorrectNode {
		return {
			...super.exportJSON(),
			type: nodeType,
			version: nodeVersion,
		};
	}
}

export const $createQuizAnswerCorrectNode = (): QuizAnswerCorrectNode =>
	$applyNodeReplacement(new QuizAnswerCorrectNode());

export const $isQuizAnswerCorrectNode = (
	node: LexicalNode | undefined | null,
): node is QuizAnswerCorrectNode => node instanceof QuizAnswerCorrectNode;

export type SerializedQuizAnswerCorrectNode = Spread<
	{
		type: typeof nodeType;
		version: typeof nodeVersion;
	},
	SerializedElementNode
>;

export type QuizAnswerCorrectNodeThemeClasses = {
	[QuizAnswerCorrectNode.classes.checkbox]: EditorThemeClassName;
};
