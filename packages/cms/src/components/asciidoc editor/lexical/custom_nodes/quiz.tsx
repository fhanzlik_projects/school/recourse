import { assert } from "@recourse/commons";
import {
	$applyNodeReplacement,
	$createTextNode,
	$isElementNode,
	EditorConfig,
	ElementNode,
	GridSelection,
	LexicalNode,
	NodeSelection,
	RangeSelection,
	SerializedElementNode,
	SerializedLexicalNode,
	Spread,
} from "lexical";
import { EditorThemeClassName } from "lexical/LexicalEditor";
import { createRoot } from "solid-js";
import { css } from "solid-styled-components";
import { customElementPrefix } from ".";
import { $createQuizAnswerNode, $isQuizAnswerNode } from "./quiz_answer";
import { $isQuizQuestionNode } from "./quiz_question";

const nodeVersion = 1;
const nodeType = `${customElementPrefix}/quiz` as const;

export class QuizNode extends ElementNode {
	static readonly classes = {
		container: `${nodeType}-container`,
	} as const;

	static override getType(): typeof nodeType {
		return nodeType;
	}

	static override clone(node: QuizNode): QuizNode {
		return new QuizNode(node.__key);
	}

	private dispose: undefined | (() => void);
	override createDOM(config: EditorConfig): HTMLElement {
		this.dispose?.();
		return createRoot((dispose) => {
			this.dispose = dispose;
			return (
				<form
					class={`${config.theme[QuizNode.classes.container]} ${css`
						display: block;
					`}`}
				/>
			) as HTMLElement;
		});
	}
	override updateDOM(
		_prevNode: QuizNode,
		_dom: HTMLElement,
		_config: EditorConfig,
	): boolean {
		return false;
	}
	override remove(preserveEmptyParent?: boolean | undefined) {
		this.dispose?.();
		super.remove(preserveEmptyParent);
	}

	override append(...nodesToAppend: LexicalNode[]): this {
		for (const currentNode of nodesToAppend) {
			if (
				$isQuizQuestionNode(currentNode) ||
				$isQuizAnswerNode(currentNode)
			) {
				super.append(currentNode);
			} else {
				const listItemNode = $createQuizAnswerNode();

				if ($isElementNode(currentNode)) {
					const textNode = $createTextNode(
						currentNode.getTextContent(),
					);
					listItemNode.append(textNode);
				} else {
					listItemNode.append(currentNode);
				}

				super.append(listItemNode);
			}
		}

		return this;
	}

	override canBeEmpty(): false {
		return false;
	}

	override extractWithChild(
		child: LexicalNode,
		_selection: RangeSelection | NodeSelection | GridSelection | null,
		_destination: "clone" | "html",
	): boolean {
		return $isQuizQuestionNode(child) || $isQuizQuestionNode(child);
	}

	static override importJSON(
		serializedNode: SerializedLexicalNode,
	): QuizNode {
		assert(
			serializedNode.type === nodeType,
			`we shouldn't be asked to deserialize nodes with incorrect types`,
		);
		assert(
			serializedNode.version > nodeVersion,
			`can't import future nodes`,
		);

		return $createQuizNode();
	}

	override exportJSON(): SerializedQuizNode {
		return {
			...super.exportJSON(),
			type: nodeType,
			version: nodeVersion,
		};
	}
}

export const $createQuizNode = (): QuizNode =>
	$applyNodeReplacement(new QuizNode());

export const $isQuizNode = (
	node: LexicalNode | undefined | null,
): node is QuizNode => node instanceof QuizNode;

export type SerializedQuizNode = Spread<
	{
		type: typeof nodeType;
		version: typeof nodeVersion;
	},
	SerializedElementNode
>;

export type QuizNodeThemeClasses = {
	[QuizNode.classes.container]: EditorThemeClassName;
};
