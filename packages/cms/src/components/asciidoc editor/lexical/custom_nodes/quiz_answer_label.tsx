import { assert } from "@recourse/commons";
import {
	$applyNodeReplacement,
	EditorConfig,
	ElementNode,
	LexicalNode,
	RangeSelection,
	SerializedElementNode,
	SerializedLexicalNode,
	Spread,
} from "lexical";
import { createRoot } from "solid-js";
import { customElementPrefix } from ".";
import { $createQuizNode } from "./quiz";
import { $isQuizAnswerNode, QuizAnswerNode } from "./quiz_answer";

const nodeVersion = 1;
const nodeType = `${customElementPrefix}/quiz/answer/label` as const;

export class QuizAnswerLabelNode extends ElementNode {
	static override getType(): typeof nodeType {
		return nodeType;
	}

	static override clone(node: QuizAnswerLabelNode): QuizAnswerLabelNode {
		return new QuizAnswerLabelNode(node.__key);
	}

	public getParentAnswer(): QuizAnswerNode {
		const parent = this.getParentOrThrow();

		assert(
			$isQuizAnswerNode(parent),
			`${this.getType()} must always be a child of ${QuizAnswerNode.getType()}, but the parent is ${parent.getType()} instead`,
		);

		return parent;
	}

	private dispose: undefined | (() => void);
	override createDOM(config: EditorConfig): HTMLElement {
		this.dispose?.();
		return createRoot((dispose) => {
			this.dispose = dispose;
			return (<p />) as HTMLElement;
		});
	}
	override updateDOM(
		_prevNode: unknown,
		_dom: HTMLElement,
		_config: EditorConfig,
	): boolean {
		return false;
	}
	override remove(preserveEmptyParent?: boolean | undefined): void {
		this.dispose?.();
		super.remove(preserveEmptyParent);
	}

	override insertAfter(
		nodeToInsert: LexicalNode,
		restoreSelection?: boolean | undefined,
	): LexicalNode {
		return this.getParentAnswer().insertAfter(
			nodeToInsert,
			restoreSelection,
		);
	}

	override insertNewAfter(
		selection: RangeSelection,
		restoreSelection = true,
	): LexicalNode | null {
		return this.getParentAnswer().insertNewAfter(
			selection,
			restoreSelection,
		);
	}

	override canBeEmpty(): boolean {
		return true;
	}

	override isParentRequired(): true {
		return true;
	}

	override createParentElementNode(): ElementNode {
		return $createQuizNode();
	}

	static override importJSON(
		serializedNode: SerializedLexicalNode,
	): QuizAnswerLabelNode {
		assert(
			serializedNode.type === nodeType,
			`we shouldn't be asked to deserialize nodes with incorrect types`,
		);
		assert(
			serializedNode.version > nodeVersion,
			`can't import future nodes`,
		);

		return $createQuizAnswerLabelNode();
	}

	override exportJSON(): SerializedQuizAnswerLabelNode {
		return {
			...super.exportJSON(),
			type: nodeType,
			version: nodeVersion,
		};
	}
}

export const $createQuizAnswerLabelNode = (): QuizAnswerLabelNode =>
	$applyNodeReplacement<QuizAnswerLabelNode>(new QuizAnswerLabelNode());

export const $isQuizAnswerLabelNode = (
	node: LexicalNode | undefined | null,
): node is QuizAnswerLabelNode => node instanceof QuizAnswerLabelNode;

export type SerializedQuizAnswerLabelNode = Spread<
	{
		type: typeof nodeType;
		version: typeof nodeVersion;
	},
	SerializedElementNode
>;
