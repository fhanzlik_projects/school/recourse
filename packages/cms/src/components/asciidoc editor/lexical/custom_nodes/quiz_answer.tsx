import { assert } from "@recourse/commons";
import {
	$applyNodeReplacement,
	$createParagraphNode,
	$createTextNode,
	$getSelection,
	$isElementNode,
	$isRangeSelection,
	$isRootOrShadowRoot,
	EditorConfig,
	ElementNode,
	LexicalNode,
	NodeKey,
	RangeSelection,
	SerializedElementNode,
	SerializedLexicalNode,
	Spread,
} from "lexical";
import { EditorThemeClassName } from "lexical/LexicalEditor";
import { createRoot } from "solid-js";
import { css } from "solid-styled-components";
import { customElementPrefix } from ".";
import { $createQuizNode, $isQuizNode, QuizNode } from "./quiz";
import {
	$createQuizAnswerCorrectNode,
	$isQuizAnswerCorrectNode,
} from "./quiz_answer_correct";
import {
	$createQuizAnswerLabelNode,
	$isQuizAnswerLabelNode,
	QuizAnswerLabelNode,
} from "./quiz_answer_label";

const nodeVersion = 1;
const nodeType = `${customElementPrefix}/quiz/answer` as const;

export class QuizAnswerNode extends ElementNode {
	static readonly classes = {
		container: `${nodeType}-container`,
	} as const;

	private __correct = false;
	public setCorrect = (value: boolean) => {
		this.getWritable().__correct = value;
	};
	public getCorrect = () => this.getLatest().__correct;

	static override getType(): typeof nodeType {
		return nodeType;
	}

	static override clone(node: QuizAnswerNode): QuizAnswerNode {
		return new QuizAnswerNode(node.__correct, node.__key);
	}

	constructor(correct: boolean, key?: NodeKey) {
		super(key);
		this.__correct = correct;
	}

	public getParentQuiz(): QuizNode {
		const parent = this.getParentOrThrow();

		assert(
			$isQuizNode(parent),
			`${QuizAnswerNode.getType()} must always be a child of ${QuizNode.getType()}, but the parent is ${parent.getType()} instead`,
		);

		return parent;
	}

	public getLabel(): QuizAnswerLabelNode | undefined {
		const label = this.getChildAtIndex(1);
		if (label === null) return undefined;
		assert(
			$isQuizAnswerLabelNode(label),
			`invalid child: ${label.getType()}`,
		);
		return label;
	}

	override append(...nodesToAppend: LexicalNode[]): this {
		for (const currentNode of nodesToAppend) {
			if (
				$isQuizAnswerCorrectNode(currentNode) ||
				$isQuizAnswerLabelNode(currentNode)
			) {
				super.append(currentNode);
			} else {
				const label = $createQuizAnswerLabelNode();

				if ($isElementNode(currentNode)) {
					const textNode = $createTextNode(
						currentNode.getTextContent(),
					);
					label.append(textNode);
				} else {
					label.append(currentNode);
				}

				super.append(label);
			}
		}

		return this;
	}

	private dispose: undefined | (() => void);
	override createDOM(config: EditorConfig): HTMLElement {
		this.dispose?.();
		return createRoot((dispose) => {
			this.dispose = dispose;
			return (
				<div
					class={`${
						config.theme[QuizAnswerNode.classes.container]
					} ${css`
						display: flex;
						flex-direction: row;
					`}`}
				/>
			) as HTMLElement;
		});
	}
	override updateDOM(
		_prevNode: unknown,
		_dom: HTMLElement,
		_config: EditorConfig,
	): boolean {
		return false;
	}
	override remove(preserveEmptyParent?: boolean | undefined): void {
		this.dispose?.();
		super.remove(preserveEmptyParent);
	}

	override insertNewAfter(
		_: RangeSelection,
		restoreSelection = true,
	): QuizAnswerNode {
		const newElement = $createQuizAnswerNode();

		this.insertAfter(newElement, restoreSelection);

		return newElement;
	}

	override insertAfter(
		nodeToInsert: LexicalNode,
		restoreSelection?: boolean | undefined,
	): LexicalNode {
		if ($isQuizAnswerNode(nodeToInsert)) {
			return super.insertAfter(nodeToInsert, restoreSelection);
		}

		const quiz = this.getParentQuiz();

		return quiz.insertAfter(nodeToInsert, restoreSelection);
	}

	override selectStart(): RangeSelection {
		return this.getLastDescendant()?.selectNext() ?? this.select();
	}

	override canBeEmpty(): boolean {
		return true;
	}

	override isParentRequired(): true {
		return true;
	}

	override createParentElementNode(): ElementNode {
		return $createQuizNode();
	}

	static override importJSON(
		serializedNode: SerializedLexicalNode,
	): QuizAnswerNode {
		assert(
			serializedNode.type === nodeType,
			`we shouldn't be asked to deserialize nodes with incorrect types`,
		);
		assert(
			serializedNode.version > nodeVersion,
			`can't import future nodes`,
		);
		const data = serializedNode as SerializedQuizAnswerNode;

		return $createQuizAnswerNode(data.correct);
	}

	override exportJSON(): SerializedQuizAnswerNode {
		return {
			...super.exportJSON(),
			type: nodeType,
			version: nodeVersion,
			correct: this.__correct,
		};
	}
}

export const $createQuizAnswerNode = (correct = false) =>
	$applyNodeReplacement<QuizAnswerNode>(new QuizAnswerNode(correct)).append(
		$createQuizAnswerCorrectNode(),
	);

export const $isQuizAnswerNode = (
	node: LexicalNode | undefined | null,
): node is QuizAnswerNode => node instanceof QuizAnswerNode;

export type SerializedQuizAnswerNode = Spread<
	{
		type: typeof nodeType;
		version: typeof nodeVersion;
		correct: boolean;
	},
	SerializedElementNode
>;

export type QuizAnswerNodeThemeClasses = {
	[QuizAnswerNode.classes.container]: EditorThemeClassName;
};

export const $handleQuizAnswerInsertParagraph = (): boolean => {
	const selection = $getSelection();

	if (!$isRangeSelection(selection) || !selection.isCollapsed()) {
		return false;
	}

	const answer = selection.anchor.getNode();

	// only run this code on empty quiz answers
	if (!$isQuizAnswerNode(answer) || answer.getTextContent() !== ``) {
		return false;
	}

	const quiz = answer.getParentQuiz();

	if (!$isRootOrShadowRoot(quiz.getParent())) {
		return false;
	}

	const replacementNode = $createParagraphNode();
	quiz.insertAfter(replacementNode);

	replacementNode.select();

	// delete the empty answer
	answer.remove();

	return true;
};
