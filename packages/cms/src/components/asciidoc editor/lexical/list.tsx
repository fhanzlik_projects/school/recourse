// this is nearly character-for-character stolen from https://github.com/facebook/lexical/blob/8c4e9ddf5b5ef93833f1e074953a943c0ca8bde7/packages/lexical-react/src/shared/useList.ts

import type { LexicalEditor } from "lexical";

import {
	$handleListInsertParagraph,
	insertList,
	INSERT_ORDERED_LIST_COMMAND,
	INSERT_UNORDERED_LIST_COMMAND,
	removeList,
	REMOVE_LIST_COMMAND,
} from "@lexical/list";
import { mergeRegister } from "@lexical/utils";
import { COMMAND_PRIORITY_LOW, INSERT_PARAGRAPH_COMMAND } from "lexical";

export function registerListCommands(editor: LexicalEditor) {
	return mergeRegister(
		editor.registerCommand(
			INSERT_ORDERED_LIST_COMMAND,
			() => {
				insertList(editor, `number`);
				return true;
			},
			COMMAND_PRIORITY_LOW,
		),
		editor.registerCommand(
			INSERT_UNORDERED_LIST_COMMAND,
			() => {
				insertList(editor, `bullet`);
				return true;
			},
			COMMAND_PRIORITY_LOW,
		),
		editor.registerCommand(
			REMOVE_LIST_COMMAND,
			() => {
				removeList(editor);
				return true;
			},
			COMMAND_PRIORITY_LOW,
		),
		editor.registerCommand(
			INSERT_PARAGRAPH_COMMAND,
			() => {
				const hasHandledInsertParagraph = $handleListInsertParagraph();

				if (hasHandledInsertParagraph) {
					return true;
				}

				return false;
			},
			COMMAND_PRIORITY_LOW,
		),
	);
}
