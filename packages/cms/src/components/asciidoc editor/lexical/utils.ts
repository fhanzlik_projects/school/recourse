import { LexicalEditor } from "lexical";

// stolen almost verbatim from Lexical docs
//
// let's hope it doesn't break.
export function findEditor(target: Node) {
	let node: ParentNode | Node | null = target;

	while (node) {
		// @ts-expect-error internal field
		if (node.__lexicalEditor) {
			// @ts-expect-error internal field
			return node.__lexicalEditor as LexicalEditor;
		}

		node = node.parentNode;
	}

	return null;
}
