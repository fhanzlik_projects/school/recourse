/**
 * Copyright (c) Meta Platforms, Inc. and affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

import type {
	EditorConfig,
	EditorState,
	ElementNode,
	GridSelection,
	LexicalEditor,
	LexicalNode,
	NodeSelection,
	RangeSelection,
	TextNode,
} from "lexical";

import { $isLinkNode, LinkNode } from "@lexical/link";
import { mergeRegister } from "@lexical/utils";
import { text } from "@recourse/commons";
import {
	$getRoot,
	$getSelection,
	$isElementNode,
	$isRangeSelection,
	$isTextNode,
	DEPRECATED_$isGridSelection,
} from "lexical";
import { createSignal, onCleanup, onMount } from "solid-js";
import { styled } from "solid-styled-components";

const NON_SINGLE_WIDTH_CHARS_REPLACEMENT: Readonly<Record<string, string>> =
	Object.freeze({
		"\t": `\\t`,
		"\n": `\\n`,
	});
const NON_SINGLE_WIDTH_CHARS_REGEX = new RegExp(
	Object.keys(NON_SINGLE_WIDTH_CHARS_REPLACEMENT).join(`|`),
	`g`,
);
const SYMBOLS = {
	ancestorHasNextSibling: `|`,
	ancestorIsLastChild: ` `,
	hasNextSibling: `├`,
	isLastChild: `└`,
	selectedChar: `^`,
	selectedLine: `>`,
};

export const TreeView = (props: { editor: LexicalEditor }) => {
	const [content, setContent] = createSignal<string>(``);

	const generateTree = () => {
		const treeText = generateContent(
			props.editor.getEditorState(),
			props.editor._config,
			props.editor._compositionKey,
			props.editor._editable,
		);
		setContent(treeText);
	};

	onMount(() => {
		onCleanup(
			mergeRegister(
				props.editor.registerUpdateListener(({ editorState }) => {
					generateTree();
				}),
				props.editor.registerEditableListener(() => {
					generateTree();
				}),
			),
		);
	});

	return <Container>{content()}</Container>;
};

function printRangeSelection(selection: RangeSelection): string {
	let res = ``;

	const formatText = printFormatProperties(selection);

	res += `: range ${formatText !== `` ? `{ ${formatText} }` : ``} ${
		selection.style !== `` ? `{ style: ${selection.style} } ` : ``
	}`;

	const anchor = selection.anchor;
	const focus = selection.focus;
	const anchorOffset = anchor.offset;
	const focusOffset = focus.offset;

	res += `\n  ├ anchor { key: ${anchor.key}, offset: ${
		anchorOffset === null ? `null` : anchorOffset
	}, type: ${anchor.type} }`;
	res += `\n  └ focus { key: ${focus.key}, offset: ${
		focusOffset === null ? `null` : focusOffset
	}, type: ${focus.type} }`;

	return res;
}

function printObjectSelection(selection: NodeSelection): string {
	return `: node\n  └ [${Array.from(selection._nodes).join(`, `)}]`;
}

function printGridSelection(selection: GridSelection): string {
	return `: grid\n  └ { grid: ${selection.gridKey}, anchorCell: ${selection.anchor.key}, focusCell: ${selection.focus.key} }`;
}

function generateContent(
	editorState: EditorState,
	editorConfig: EditorConfig,
	compositionKey: null | string,
	editable: boolean,
): string {
	let res = ` root\n`;

	const selectionString = editorState.read(() => {
		const selection = $getSelection();

		visitTree($getRoot(), (node: LexicalNode, indent: string[]) => {
			const nodeKey = node.getKey();
			const nodeKeyDisplay = `(${nodeKey})`;
			const typeDisplay = node.getType() || ``;
			const isSelected = node.isSelected();

			res += `${isSelected ? SYMBOLS.selectedLine : ` `} ${indent.join(
				` `,
			)} ${nodeKeyDisplay} ${typeDisplay} ${printNode(node)}\n`;

			res += printSelectedCharsLine({
				indent,
				isSelected,
				node,
				nodeKeyDisplay,
				selection,
				typeDisplay,
			});
		});

		return selection === null
			? `: null`
			: $isRangeSelection(selection)
			? printRangeSelection(selection)
			: DEPRECATED_$isGridSelection(selection)
			? printGridSelection(selection)
			: printObjectSelection(selection);
	});

	res += `\n selection` + selectionString;

	res += `\n\n editor:`;
	res += `\n  └ namespace ${editorConfig.namespace}`;
	if (compositionKey !== null) {
		res += `\n  └ compositionKey ${compositionKey}`;
	}
	res += `\n  └ editable ${String(editable)}`;

	return res;
}

function visitTree(
	currentNode: ElementNode,
	visitor: (node: LexicalNode, indentArr: string[]) => void,
	indent: string[] = [],
) {
	const childNodes = currentNode.getChildren();
	const childNodesLength = childNodes.length;

	childNodes.forEach((childNode, i) => {
		visitor(
			childNode,
			indent.concat(
				i === childNodesLength - 1
					? SYMBOLS.isLastChild
					: SYMBOLS.hasNextSibling,
			),
		);

		if ($isElementNode(childNode)) {
			visitTree(
				childNode,
				visitor,
				indent.concat(
					i === childNodesLength - 1
						? SYMBOLS.ancestorIsLastChild
						: SYMBOLS.ancestorHasNextSibling,
				),
			);
		}
	});
}

function normalize(text: string) {
	return Object.entries(NON_SINGLE_WIDTH_CHARS_REPLACEMENT).reduce(
		(acc, [key, value]) => acc.replace(new RegExp(key, `g`), String(value)),
		text,
	);
}

// TODO Pass via props to allow customizability
function printNode(node: LexicalNode) {
	if ($isTextNode(node)) {
		const text = node.getTextContent();
		const title = text.length === 0 ? `(empty)` : `"${normalize(text)}"`;
		const properties = printAllTextNodeProperties(node);
		return [title, properties.length !== 0 ? `{ ${properties} }` : null]
			.filter(Boolean)
			.join(` `)
			.trim();
	} else if ($isLinkNode(node)) {
		const link = node.getURL();
		const title = link.length === 0 ? `(empty)` : `"${normalize(link)}"`;
		const properties = printAllLinkNodeProperties(node);
		return [title, properties.length !== 0 ? `{ ${properties} }` : null]
			.filter(Boolean)
			.join(` `)
			.trim();
	} else {
		return ``;
	}
}

const FORMAT_PREDICATES = [
	(node: TextNode | RangeSelection) => node.hasFormat(`bold`) && `Bold`,
	(node: TextNode | RangeSelection) => node.hasFormat(`code`) && `Code`,
	(node: TextNode | RangeSelection) => node.hasFormat(`italic`) && `Italic`,
	(node: TextNode | RangeSelection) =>
		node.hasFormat(`strikethrough`) && `Strikethrough`,
	(node: TextNode | RangeSelection) =>
		node.hasFormat(`subscript`) && `Subscript`,
	(node: TextNode | RangeSelection) =>
		node.hasFormat(`superscript`) && `Superscript`,
	(node: TextNode | RangeSelection) =>
		node.hasFormat(`underline`) && `Underline`,
];

const DETAIL_PREDICATES = [
	(node: TextNode) => node.isDirectionless() && `Directionless`,
	(node: TextNode) => node.isUnmergeable() && `Unmergeable`,
];

const MODE_PREDICATES = [
	(node: TextNode) => node.isToken() && `Token`,
	(node: TextNode) => node.isSegmented() && `Segmented`,
];

function printAllTextNodeProperties(node: TextNode) {
	return [
		printFormatProperties(node),
		printDetailProperties(node),
		printModeProperties(node),
	]
		.filter(Boolean)
		.join(`, `);
}

function printAllLinkNodeProperties(node: LinkNode) {
	return [printTargetProperties(node), printRelProperties(node)]
		.filter(Boolean)
		.join(`, `);
}

function printDetailProperties(nodeOrSelection: TextNode) {
	let str = DETAIL_PREDICATES.map((predicate) => predicate(nodeOrSelection))
		.filter(Boolean)
		.join(`, `)
		.toLocaleLowerCase();

	if (str !== ``) {
		str = `detail: ` + str;
	}

	return str;
}

function printModeProperties(nodeOrSelection: TextNode) {
	let str = MODE_PREDICATES.map((predicate) => predicate(nodeOrSelection))
		.filter(Boolean)
		.join(`, `)
		.toLocaleLowerCase();

	if (str !== ``) {
		str = `mode: ` + str;
	}

	return str;
}

function printFormatProperties(nodeOrSelection: TextNode | RangeSelection) {
	let str = FORMAT_PREDICATES.map((predicate) => predicate(nodeOrSelection))
		.filter(Boolean)
		.join(`, `)
		.toLocaleLowerCase();

	if (str !== ``) {
		str = `format: ` + str;
	}

	return str;
}

function printTargetProperties(node: LinkNode) {
	let str = node.getTarget();
	// TODO Fix nullish on LinkNode
	if (str != null) {
		str = `target: ` + str;
	}
	return str;
}

function printRelProperties(node: LinkNode) {
	let str = node.getRel();
	// TODO Fix nullish on LinkNode
	if (str != null) {
		str = `rel: ` + str;
	}
	return str;
}

function printSelectedCharsLine({
	indent,
	isSelected,
	node,
	nodeKeyDisplay,
	selection,
	typeDisplay,
}: {
	indent: string[];
	isSelected: boolean;
	node: LexicalNode;
	nodeKeyDisplay: string;
	selection: GridSelection | NodeSelection | RangeSelection | null;
	typeDisplay: string;
}) {
	// No selection or node is not selected.
	if (
		!$isTextNode(node) ||
		!$isRangeSelection(selection) ||
		!isSelected ||
		$isElementNode(node)
	) {
		return ``;
	}

	// No selected characters.
	const anchor = selection.anchor;
	const focus = selection.focus;

	if (
		node.getTextContent() === `` ||
		(anchor.getNode() === selection.focus.getNode() &&
			anchor.offset === focus.offset)
	) {
		return ``;
	}

	const [start, end] = $getSelectionStartEnd(node, selection);

	if (start === end) {
		return ``;
	}

	const selectionLastIndent =
		indent[indent.length - 1] === SYMBOLS.hasNextSibling
			? SYMBOLS.ancestorHasNextSibling
			: SYMBOLS.ancestorIsLastChild;

	const indentionChars = [
		...indent.slice(0, indent.length - 1),
		selectionLastIndent,
	];
	const unselectedChars = Array<string>(start + 1).fill(` `);
	const selectedChars = Array<string>(end - start).fill(SYMBOLS.selectedChar);
	const paddingLength = typeDisplay.length + 3; // 2 for the spaces around + 1 for the double quote.

	const nodePrintSpaces = Array<string>(
		nodeKeyDisplay.length + paddingLength,
	).fill(` `);

	return (
		[
			SYMBOLS.selectedLine,
			indentionChars.join(` `),
			[...nodePrintSpaces, ...unselectedChars, ...selectedChars].join(``),
		].join(` `) + `\n`
	);
}

function $getSelectionStartEnd(
	node: LexicalNode,
	selection: RangeSelection | GridSelection,
): [number, number] {
	const anchor = selection.anchor;
	const focus = selection.focus;
	const textContent = node.getTextContent();
	const textLength = textContent.length;

	let start = -1;
	let end = -1;

	// Only one node is being selected.
	if (anchor.type === `text` && focus.type === `text`) {
		const anchorNode = anchor.getNode();
		const focusNode = focus.getNode();

		if (
			anchorNode === focusNode &&
			node === anchorNode &&
			anchor.offset !== focus.offset
		) {
			[start, end] =
				anchor.offset < focus.offset
					? [anchor.offset, focus.offset]
					: [focus.offset, anchor.offset];
		} else if (node === anchorNode) {
			[start, end] = anchorNode.isBefore(focusNode)
				? [anchor.offset, textLength]
				: [0, anchor.offset];
		} else if (node === focusNode) {
			[start, end] = focusNode.isBefore(anchorNode)
				? [focus.offset, textLength]
				: [0, focus.offset];
		} else {
			// Node is within selection but not the anchor nor focus.
			[start, end] = [0, textLength];
		}
	}

	// Account for non-single width characters.
	const numNonSingleWidthCharBeforeSelection = (
		textContent.slice(0, start).match(NON_SINGLE_WIDTH_CHARS_REGEX) ?? []
	).length;
	const numNonSingleWidthCharInSelection = (
		textContent.slice(start, end).match(NON_SINGLE_WIDTH_CHARS_REGEX) ?? []
	).length;

	return [
		start + numNonSingleWidthCharBeforeSelection,
		end +
			numNonSingleWidthCharBeforeSelection +
			numNonSingleWidthCharInSelection,
	];
}

const Container = styled.pre`
	${text({ size: 0.75 })}
	font-family: monospace;
	line-height: 1;
	padding: 1rem 0;

	overflow-x: auto;
	max-width: 100%;
	height: 100%;
`;
