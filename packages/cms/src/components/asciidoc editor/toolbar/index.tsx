import ADoc from "@asciidoctor/core";
import { $isCodeNode } from "@lexical/code";
import { $isListNode } from "@lexical/list";
import { $isHeadingNode } from "@lexical/rich-text";
import { colour, css, error, foreground, stateLayer } from "@recourse/commons";
import {
	$getRoot,
	$getSelection,
	$isParagraphNode,
	$isRangeSelection,
	FORMAT_TEXT_COMMAND,
	LexicalEditor,
} from "lexical";
import {
	BiRegularBold,
	BiRegularCode,
	BiRegularExport,
	BiRegularItalic,
	BiRegularStrikethrough,
	BiRegularUnderline,
} from "solid-icons/bi";
import { Component, createSignal, onCleanup, onMount } from "solid-js";
import { Dynamic } from "solid-js/web";
import { styled } from "solid-styled-components";
import { $serializeNode } from "../export";
import { $isQuizNode } from "../lexical/custom_nodes/quiz";
import { BlockType, BlockTypeSelect } from "./block_type_select";

const toolbarButtonSize = `2rem`;

export const Toolbar = (props: { editor: LexicalEditor }) => {
	const [bold, assumeBold] = createSignal(false);
	const [italic, assumeItalic] = createSignal(false);
	const [underline, assumeUnderline] = createSignal(false);
	const [strikethrough, assumeStrikethrough] = createSignal(false);
	const [code, assumeCode] = createSignal(false);
	const [blockType, assumeBlockType] = createSignal<BlockType | undefined>(
		`paragraph`,
	);

	const updateToolbar = () => {
		const selection = $getSelection();

		if ($isRangeSelection(selection)) {
			const anchorNode = selection.anchor.getNode();
			const element =
				anchorNode.getKey() === `root`
					? anchorNode
					: anchorNode.getTopLevelElementOrThrow();

			assumeBold(selection.hasFormat(`bold`));
			assumeItalic(selection.hasFormat(`italic`));
			assumeUnderline(selection.hasFormat(`underline`));
			assumeStrikethrough(selection.hasFormat(`strikethrough`));
			assumeCode(selection.hasFormat(`code`));

			assumeBlockType(
				$isHeadingNode(element)
					? (
							{
								h1: `heading/1`,
								h2: `heading/2`,
								h3: `heading/3`,
								h4: undefined,
								h5: undefined,
								h6: undefined,
							} as const
					  )[element.getTag()]
					: $isParagraphNode(element)
					? `paragraph`
					: $isCodeNode(element)
					? `code`
					: $isListNode(element)
					? `list/bullet`
					: $isQuizNode(element)
					? `quiz`
					: undefined,
			);
		}
	};

	onMount(() => {
		const unregister = props.editor.registerUpdateListener(
			({ editorState }) => {
				editorState.read(() => {
					updateToolbar();
				});
			},
		);
		onCleanup(unregister);
	});

	return (
		<Container>
			<BlockTypeSelect
				editor={props.editor}
				blockType={blockType}
				assumeBlockType={assumeBlockType}
			/>
			<Button
				active={bold()}
				aria-label="Format bold"
				onClick={() =>
					props.editor.dispatchCommand(FORMAT_TEXT_COMMAND, `bold`)
				}
				Icon={BiRegularBold}
			/>
			<Button
				active={italic()}
				aria-label="Format italic"
				onClick={() =>
					props.editor.dispatchCommand(FORMAT_TEXT_COMMAND, `italic`)
				}
				Icon={BiRegularItalic}
			/>
			<Button
				active={underline()}
				aria-label="Format underline"
				onClick={() =>
					props.editor.dispatchCommand(
						FORMAT_TEXT_COMMAND,
						`underline`,
					)
				}
				Icon={BiRegularUnderline}
			/>
			<Button
				active={strikethrough()}
				aria-label="Format strikethrough"
				onClick={() =>
					props.editor.dispatchCommand(
						FORMAT_TEXT_COMMAND,
						`strikethrough`,
					)
				}
				Icon={BiRegularStrikethrough}
			/>
			<Button
				active={code()}
				aria-label="Format code"
				onClick={() =>
					props.editor.dispatchCommand(FORMAT_TEXT_COMMAND, `code`)
				}
				Icon={BiRegularCode}
			/>
			<Button
				active={false}
				aria-label="Print AsciiDoc"
				onClick={() => {
					props.editor.getEditorState().read(() => {
						const source =
							$serializeNode($getRoot()) ??
							error.unexpectedNullish();

						const html = ADoc().load(source).convert();
						window
							.open(undefined, undefined, `popup`)
							?.document.write(
								`<textarea rows=30 cols=120>${source}</textarea>${html}`,
							);
					});
				}}
				Icon={BiRegularExport}
			/>
		</Container>
	);
};

const Container = styled.div`
	flex-basis: ${toolbarButtonSize};
	box-sizing: content-box;
	padding: 0.5rem 1rem;
	gap: 0.25rem;
	display: flex;
	overflow-x: auto;
`;

const Button = (props: {
	"aria-label": string;
	active: boolean;
	onClick: () => void;
	Icon: Component<{ size: number }>;
}) => (
	<ButtonContainer
		active={props.active}
		onClick={() => props.onClick()}
		aria-label={props[`aria-label`]}
	>
		<Dynamic component={props.Icon} size={24} />
	</ButtonContainer>
);

const ButtonContainer = styled.button<{ active: boolean }>`
	flex-basis: ${toolbarButtonSize};
	display: flex;
	align-items: center;
	justify-content: center;
	background: none;
	${foreground(`bright`)}

	border-radius: 0.5rem;

	${({ active, theme }) => css`
		background: ${active
			? colour(`secondaryContainer`)({ theme })
			: `transparent`};

		&:hover {
			background: ${active
				? stateLayer(
						`secondaryContainer`,
						`onSecondaryContainer`,
						`hover`,
				  )({ theme })
				: stateLayer(
						`transparent`,
						`onSurfaceVariant`,
						`hover`,
				  )({ theme })};
		}

		&:focus {
			outline: none;
			background: ${active
				? stateLayer(
						`secondaryContainer`,
						`onSecondaryContainer`,
						`focus`,
				  )({ theme })
				: stateLayer(
						`transparent`,
						`onSurfaceVariant`,
						`focus`,
				  )({ theme })};
		}
	`}
`;
