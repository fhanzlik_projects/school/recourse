import { $createCodeNode } from "@lexical/code";
import {
	INSERT_UNORDERED_LIST_COMMAND,
	REMOVE_LIST_COMMAND,
} from "@lexical/list";
import { $createHeadingNode } from "@lexical/rich-text";
import { $wrapNodes } from "@lexical/selection";
import { foreground, stateLayer } from "@recourse/commons";
import {
	$createParagraphNode,
	$getSelection,
	$isRangeSelection,
	LexicalEditor,
} from "lexical";
import { Accessor, For, Setter } from "solid-js";
import { styled } from "solid-styled-components";
import { $createQuizNode } from "../lexical/custom_nodes/quiz";

export type BlockType =
	| `heading/${1 | 2 | 3 | 4 | 5 | 6}`
	| "paragraph"
	| "code"
	| "list/bullet"
	| "quiz";

export const BlockTypeSelect = (props: {
	editor: LexicalEditor;
	blockType: Accessor<BlockType | undefined>;
	assumeBlockType: Setter<BlockType>;
}) => (
	<Container value={props.blockType() ?? `unknown`}>
		<For
			each={
				[
					{
						tag: `h1`,
						type: `heading/1`,
						label: `Document title`,
					},
					{
						tag: `h2`,
						type: `heading/2`,
						label: `Heading`,
					},
					{
						tag: `h3`,
						type: `heading/3`,
						label: `Subheading`,
					},
				] as const
			}
		>
			{({ tag, type, label }) => (
				<option
					value={type}
					onClick={() => {
						if (props.blockType() !== type) {
							// to prevent races when the editor isn't updated yet, but the user has already managed to somehow pick the option again
							props.assumeBlockType(type);

							props.editor.update(() => {
								const selection = $getSelection();

								if ($isRangeSelection(selection)) {
									$wrapNodes(selection, () =>
										$createHeadingNode(tag),
									);
								}
							});
						}
					}}
				>
					{label}
				</option>
			)}
		</For>
		<option
			value="paragraph"
			onClick={() => {
				if (props.blockType() !== `paragraph`) {
					props.editor.update(() => {
						const selection = $getSelection();

						if ($isRangeSelection(selection)) {
							$wrapNodes(selection, () => $createParagraphNode());
						}
					});
				}
			}}
		>
			Paragraph
		</option>
		<option
			value="code"
			onClick={() => {
				if (props.blockType() !== `code`) {
					props.editor.update(() => {
						const selection = $getSelection();

						if ($isRangeSelection(selection)) {
							$wrapNodes(selection, () => $createCodeNode());
						}
					});
				}
			}}
		>
			Code
		</option>
		<option
			value="list/bullet"
			onClick={() => {
				if (props.blockType() !== `list/bullet`) {
					props.editor.update(() => {
						props.editor.dispatchCommand(
							INSERT_UNORDERED_LIST_COMMAND,
							undefined,
						);
					});
				} else {
					props.editor.update(() => {
						props.editor.dispatchCommand(
							REMOVE_LIST_COMMAND,
							undefined,
						);
					});
				}
			}}
		>
			Bullet List
		</option>
		<option
			value="quiz"
			onClick={() => {
				if (props.blockType() !== `quiz`) {
					props.editor.update(() => {
						const selection = $getSelection();

						if ($isRangeSelection(selection)) {
							$wrapNodes(selection, () => $createQuizNode());
						}
					});
				}
			}}
		>
			Quiz
		</option>
		<option value="unknown" hidden disabled>
			Unknown
		</option>
	</Container>
);

const Container = styled.select`
	${foreground(`bright`)}

	padding: 0 0.5rem;
	border-radius: 0.5rem;
	transition: 0.1s background ease-in-out;

	background: transparent;
	&:hover {
		background: ${stateLayer(`transparent`, `onSurfaceVariant`, `hover`)};
	}

	&:focus {
		outline: none;
		background: ${stateLayer(`transparent`, `onSurfaceVariant`, `focus`)};
	}
`;
