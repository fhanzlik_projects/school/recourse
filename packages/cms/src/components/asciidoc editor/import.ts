import createAsciiDoctor, {
	AbstractBlock,
	Block,
	List,
	ListItem,
	Section,
} from "@asciidoctor/core";
import { $createCodeNode } from "@lexical/code";
import { $generateNodesFromDOM } from "@lexical/html";
import { $createListItemNode, $createListNode } from "@lexical/list";
import { $createHeadingNode, HeadingTagType } from "@lexical/rich-text";
import { assert, error, isDefined } from "@recourse/commons";
import { filter, map, pipe } from "iter-ops";
import {
	$createParagraphNode,
	$createTextNode,
	LexicalEditor,
	LexicalNode,
} from "lexical";
import Zod from "zod";
import { $createQuizNode } from "./lexical/custom_nodes/quiz";
import { $createQuizAnswerNode } from "./lexical/custom_nodes/quiz_answer";
import { $createQuizAnswerLabelNode } from "./lexical/custom_nodes/quiz_answer_label";
import { $createQuizQuestionNode } from "./lexical/custom_nodes/quiz_question";

const asciiDoctor = createAsciiDoctor();

export const $importAsciidoc = (
	editor: LexicalEditor,
	source: string,
): LexicalNode[] => {
	const parsed = asciiDoctor.load(source);

	return parsed
		.getBlocks()
		.flatMap((block) => $importBlockOrThrow(block, editor));
};

type Importer<T, U = LexicalNode[] | undefined> = (
	source: T,
	editor: LexicalEditor,
) => U;

const $importBlock: Importer<AbstractBlock> = (block, editor) =>
	pipe(
		[
			$importQuiz,
			$importSection,
			$importUnorderedList,
			$importListItem,
			$importListing,
			$importParagraph,
			$importOpenBlock,
		],
		map((transform) => transform(block, editor)),
		filter(isDefined),
	).first;

const $importBlockOrThrow: Importer<AbstractBlock, LexicalNode[]> = (
	block,
	editor,
) => {
	return (
		$importBlock(block, editor) ??
		error(
			`encountered unknown block: ${block.getNodeName()}[${block.getStyle()}]`,
		)
	);
};

const $importChildrenOrThrow: Importer<AbstractBlock, LexicalNode[]> = (
	block,
	editor,
) => block.getBlocks().flatMap((child) => $importBlockOrThrow(child, editor));

const blockIsSection = (block: AbstractBlock): block is Section =>
	block.getNodeName() === `section`;
const blockIsUnorderedList = (block: AbstractBlock): block is List =>
	block.getNodeName() === `ulist`;
const blockIsListItem = (block: AbstractBlock): block is ListItem =>
	block.getNodeName() === `list_item`;
const blockIsParagraph = (block: AbstractBlock): block is Block =>
	block.getNodeName() === `paragraph`;
const blockIsListing = (block: AbstractBlock): block is Block =>
	block.getNodeName() === `listing`;
const blockIsOpenBlock = (block: AbstractBlock): block is Block =>
	block.getNodeName() === `open`;

const lexicalHeadingLevels: HeadingTagType[] = [
	`h1`,
	`h2`,
	`h3`,
	`h4`,
	`h5`,
	`h6`,
];
const headingTagType = (level: number) =>
	lexicalHeadingLevels[level - 1] ??
	error(`incoming heading level too large`);

const htmlEntityMap = [
	[`&amp;`, `&`],
	[`&lt;`, `<`],
	[`&gt;`, `>`],
];
const decodeHtmlEntities = (str: string) =>
	htmlEntityMap.reduce(
		(str, [entity, decoded]) => str.replaceAll(entity, decoded),
		str,
	);

const domParser = new DOMParser();
const $importHtml: Importer<string, LexicalNode[]> = (html, editor) => {
	const parsed = domParser.parseFromString(html, `text/html`);

	return $generateNodesFromDOM(editor, parsed);
};

const $importOpenBlock: Importer<AbstractBlock> = (block, editor) => {
	if (!blockIsOpenBlock(block)) return;

	return $importChildrenOrThrow(block, editor);
};

const $importSection: Importer<AbstractBlock> = (section, editor) => {
	if (!blockIsSection(section)) return;

	return [
		$createHeadingNode(headingTagType(section.getLevel())).append(
			$createTextNode(decodeHtmlEntities(section.getTitle() ?? ``)),
		),
		...$importChildrenOrThrow(section, editor),
	];
};

const $importUnorderedList: Importer<AbstractBlock> = (list, editor) => {
	if (!blockIsUnorderedList(list)) return;

	const node = $createListNode(`bullet`);
	node.append(
		...list
			.getItems()
			.map((item) =>
				$createListItemNode().append(
					...$importBlockOrThrow(item, editor),
				),
			),
	);
	return [node];
};

const $importListItem: Importer<AbstractBlock> = (listItem, editor) => {
	if (!blockIsListItem(listItem)) return;

	return [
		...$importHtml(listItem.getText(), editor),
		...$importChildrenOrThrow(listItem, editor),
	];
};

const $importListing: Importer<AbstractBlock> = (listing) => {
	if (!blockIsListing(listing)) return;

	const { style, language } = Zod.object({
		style: Zod.string(),
		language: Zod.string().optional(),
	}).parse(listing.getAttributes());

	return [
		$createCodeNode(style === `source` ? language : undefined).append(
			$createTextNode(listing.getSource()),
		),
	];
};

const $importParagraph: Importer<AbstractBlock> = (block, editor) => {
	if (!blockIsParagraph(block)) return;

	const html = block.applySubstitutions(block.getSource());
	assert(!Array.isArray(html), `we're only replacing a single string`);
	return [$createParagraphNode().append(...$importHtml(html, editor))];
};

const $importQuiz: Importer<AbstractBlock> = (block, editor) => {
	if (block.getStyle() !== `quiz`) return;

	const parts = block
		.getBlocks()
		.reduce<{ question: AbstractBlock; answers?: ListItem[] }[]>(
			(questionAnswerPairs, child) => {
				if (child.getStyle() === `question`) {
					// create a new pair for every encountered question
					questionAnswerPairs.push({ question: child });
				}
				if (child.getStyle() === `answers`) {
					assert(
						blockIsUnorderedList(child),
						`answers block should be a list`,
					);
					const lastPair =
						questionAnswerPairs.at(-1) ??
						error(`encountered answers with no question for them`);
					if (lastPair.answers !== undefined)
						error(
							`encountered multiple answer sets for a question`,
						);
					lastPair.answers = child.getBlocks().map((answer) => {
						assert(
							blockIsListItem(answer),
							`only list items are valid quiz answers!`,
						);
						return answer;
					});
				}

				return questionAnswerPairs;
			},
			[],
		)
		.map(({ question, answers }) => {
			assert(
				answers !== undefined,
				`every question should have an answer list`,
			);
			return { question, answers };
		});

	return [
		$createQuizNode().append(
			...parts.flatMap(({ question, answers }) => [
				$createQuizQuestionNode().append(
					...$importBlockOrThrow(question, editor),
				),
				...answers.map((answer) =>
					$createQuizAnswerNode(
						answer.hasAttribute(`checked`),
					).append(
						$createQuizAnswerLabelNode().append(
							...$importHtml(answer.getText(), editor),
							...$importChildrenOrThrow(answer, editor),
						),
					),
				),
			]),
		),
	];
};
