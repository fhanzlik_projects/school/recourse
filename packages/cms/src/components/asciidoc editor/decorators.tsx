import { LexicalEditor } from "lexical";
import {
	Accessor,
	Component,
	createComponent,
	createMemo,
	createSignal,
	JSX,
	onCleanup,
	onMount,
	Suspense,
} from "solid-js";
import { Portal } from "solid-js/web";

type ErrorBoundaryProps = {
	children: JSX.Element;
	onError: (err: unknown, reset: () => void) => JSX.Element;
};
export type ErrorBoundaryType = Component<ErrorBoundaryProps>;

export function useDecorators(
	editor: LexicalEditor,
	ErrorBoundary: ErrorBoundaryType,
): Accessor<JSX.Element[]> {
	const [decorators, setDecorators] = createSignal<
		Record<string, JSX.Element>
	>(editor.getDecorators<JSX.Element>());

	// Subscribe to changes
	onCleanup(
		editor.registerDecoratorListener<JSX.Element>((nextDecorators) => {
			setDecorators(nextDecorators);
		}),
	);

	onMount(() => {
		// If the content editable mounts before the subscription is added, then
		// nothing will be rendered on initial pass. We can get around that by
		// ensuring that we set the value.
		setDecorators(editor.getDecorators<JSX.Element>());
	});

	// Return decorators defined as React Portals
	const portals = createMemo(() => {
		const decoratedPortals = [];
		const decoratorKeys = Object.keys(decorators());

		for (const nodeKey of decoratorKeys) {
			const decorator = (
				<ErrorBoundary
					onError={(thrown, _reset) => {
						const error =
							thrown instanceof Error
								? thrown
								: new Error(String(thrown));

						editor._onError(error);
						return undefined;
					}}
				>
					<Suspense fallback={null}>{decorators()[nodeKey]}</Suspense>
				</ErrorBoundary>
			);
			const element = editor.getElementByKey(nodeKey);

			if (element !== null) {
				decoratedPortals.push(
					createComponent(Portal, {
						mount: element,
						children: decorator,
					}),
				);
			}
		}

		return decoratedPortals;
	});

	return portals;
}
