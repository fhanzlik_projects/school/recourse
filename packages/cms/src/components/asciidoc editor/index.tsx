import {
	CodeHighlightNode,
	CodeNode,
	registerCodeHighlighting,
} from "@lexical/code";
import { createEmptyHistoryState, registerHistory } from "@lexical/history";
import { AutoLinkNode, LinkNode } from "@lexical/link";
import { ListItemNode, ListNode } from "@lexical/list";
import { HeadingNode, QuoteNode, registerRichText } from "@lexical/rich-text";
import { mergeRegister } from "@lexical/utils";
import {
	background,
	colour,
	error,
	focusOutline,
	text,
} from "@recourse/commons";
import {
	$getRoot,
	COMMAND_PRIORITY_LOW,
	createEditor,
	INSERT_PARAGRAPH_COMMAND,
} from "lexical";
import _ from "lodash";
import { onCleanup, onMount } from "solid-js";
import { styled, useTheme } from "solid-styled-components";
import { useDecorators } from "./decorators";
import { LexicalErrorBoundary } from "./error_boundary";
import { $serializeNode } from "./export";
import { $importAsciidoc } from "./import";
import { QuizNode } from "./lexical/custom_nodes/quiz";
import {
	$handleQuizAnswerInsertParagraph,
	QuizAnswerNode,
} from "./lexical/custom_nodes/quiz_answer";
import { QuizAnswerCorrectNode } from "./lexical/custom_nodes/quiz_answer_correct";
import { QuizAnswerLabelNode } from "./lexical/custom_nodes/quiz_answer_label";
import {
	$handleQuizQuestionInsertParagraph,
	QuizQuestionNode,
} from "./lexical/custom_nodes/quiz_question";
import { TreeView } from "./lexical/debug_view";
import { registerListCommands } from "./lexical/list";
import { makeTheme } from "./theme";
import { Toolbar } from "./toolbar";

export const AsciidocEditor = (props: {
	initialValue: string;
	setValue: (value: string) => void;
}) => {
	const theme = useTheme();

	const saveEditorStateImmediately = () => {
		props.setValue($serializeNode($getRoot()) ?? error.unexpectedNullish());
	};
	const queueEditorStateSave = _.debounce(() => {
		editor.getEditorState().read(() => {
			saveEditorStateImmediately();
		});
	}, 500);

	const editor = createEditor({
		onError: (e) => {
			throw e;
		},
		nodes: [
			HeadingNode,
			ListNode,
			ListItemNode,
			QuoteNode,
			AutoLinkNode,
			CodeNode,
			CodeHighlightNode,
			LinkNode,
			QuizNode,
			QuizQuestionNode,
			QuizAnswerNode,
			QuizAnswerCorrectNode,
			QuizAnswerLabelNode,
		],
		theme: makeTheme({ theme }),
	});

	useDecorators(editor, LexicalErrorBoundary);

	let editorRoot: HTMLDivElement | undefined;

	onMount(() => {
		editor.setRootElement(editorRoot ?? error.unexpectedNullish());

		const unregisterPlugins = mergeRegister(
			registerRichText(editor),
			registerHistory(editor, createEmptyHistoryState(), 1000),
			registerCodeHighlighting(editor),
			registerListCommands(editor),
			editor.registerUpdateListener(queueEditorStateSave),
			editor.registerCommand(
				INSERT_PARAGRAPH_COMMAND,
				() =>
					$handleQuizQuestionInsertParagraph() ||
					$handleQuizAnswerInsertParagraph(),
				COMMAND_PRIORITY_LOW,
			),
		);

		onCleanup(() => {
			unregisterPlugins();
		});

		editor.update(() => {
			const nodes = $importAsciidoc(editor, props.initialValue);

			const root = $getRoot();
			root.clear();
			root.append(...nodes);
		});
	});

	return (
		<Container>
			<Toolbar editor={editor} />
			<Divider />
			<Row>
				<Editor ref={editorRoot} contentEditable />
				<TreeView editor={editor} />
			</Row>
		</Container>
	);
};

const Container = styled.div`
	display: flex;
	flex-direction: column;
	margin: 0 3rem;
	border-radius: 0.5rem;
	overflow: hidden;
	${background(`elevated.0`)}
	${focusOutline({ focusMode: `within` })}
`;

const Divider = styled.div`
	height: 1px;
	margin: 0 0.5rem;
	background: ${colour(`outlineVariant`)};
`;

const Editor = styled.div`
	display: block;
	height: 32rem;
	overflow: auto;
	padding: 0.5rem 1rem;
	outline: 0;
	${text()}
`;

const Row = styled.div`
	display: flex;
	height: 32rem;
	> * {
		/* width: 50%; */
	}
`;
