import { background, colour, text, Theme } from "@recourse/commons";
import { EditorThemeClasses } from "lexical";
import { css } from "solid-styled-components";
import { QuizNode } from "./lexical/custom_nodes/quiz";
import { QuizAnswerNode } from "./lexical/custom_nodes/quiz_answer";
import { QuizAnswerCorrectNode } from "./lexical/custom_nodes/quiz_answer_correct";
import { QuizQuestionNode } from "./lexical/custom_nodes/quiz_question";
import { allPrismTokens } from "./prism";

export const makeTheme = ({ theme }: { theme: Theme }): EditorThemeClasses => {
	const heading = `
		margin-top: 1rem;
	`;
	return {
		heading: {
			h1: css`
				${heading}
				${text({ size: 2 })({ theme })}
			`,
			h2: css`
				${heading}
				${text({ size: 1.75 })({ theme })}
			`,
			h3: css`
				${heading}
				${text({ size: 1.5 })({ theme })}
			`,
		},
		text: {
			base: css`
				${text()({ theme })}
			`,
			bold: css`
				font-weight: bold;
			`,
			italic: css`
				font-style: italic;
			`,
			underline: css`
				text-decoration: underline;
			`,
			strikethrough: css`
				text-decoration: line-through;
			`,
			underlineStrikethrough: css`
				text-decoration: underline line-through;
			`,
			code: css`
				${text({ family: `code` })({ theme })}
			`,
		},
		paragraph: css`
			margin: 0.5rem 0;
		`,
		list: {
			ul: css`
				padding-left: 1.25rem;
				margin: 0.5rem 0;
			`,
			listitem: css`
				display: list-item;
				${text()({ theme })}
			`,
		},
		code: css`
			display: block;
			${text({ family: `code` })({ theme })}
			${background(`elevated.1`)({ theme })}

			padding: 0.25rem 1rem;
			/* needs to be at least the gutter's width */
			padding-left: 3.5em;
			margin: 0.5rem 0;

			position: relative;
			&::before {
				content: attr(data-gutter);
				position: absolute;
				background: inherit;
				left: 0;
				top: 0;
				border-right: 1px solid ${theme.colours.background.elevated[0]};
				padding: 0.5em;
				padding-top: inherit;
				padding-bottom: inherit;

				white-space: pre-wrap;
				text-align: right;
				min-width: 3em;
			}

			span {
				font: inherit;
			}
		`,
		codeHighlight: Object.fromEntries(
			allPrismTokens
				.map((token) => [token, `token ${token}`] as const)
				.concat([[`tag`, `token tag`]]),
		),
		[QuizNode.classes.container]: css`
			${background(`normal`)({ theme })}
			border-radius: 0.5rem;
			padding: 0.5rem;
			margin: 1rem 0.5rem;
		`,
		[QuizAnswerNode.classes.container]: css`
			padding: 0.5rem 0.5rem;
			display: block;
			align-items: baseline;
		`,
		[QuizQuestionNode.classes.container]: css`
			padding: 0 0.5rem;
			&::after {
				content: " ";
				display: block;
				margin: 1rem 0.5rem;
				height: 1px;
				background: ${colour(`outlineVariant`)({ theme })};
			}
		`,
		[QuizAnswerCorrectNode.classes.checkbox]: css`
			border-radius: 50%;
			margin-right: 1rem;
		`,
	};
};
