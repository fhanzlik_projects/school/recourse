import { ErrorBoundary, JSX } from "solid-js";

export type LexicalErrorBoundaryProps = {
	children: JSX.Element;
	onError: (err: unknown, reset: () => void) => JSX.Element;
};

export function LexicalErrorBoundary(
	props: LexicalErrorBoundaryProps,
): JSX.Element {
	return (
		<ErrorBoundary fallback={props.onError}>{props.children}</ErrorBoundary>
	);
}
