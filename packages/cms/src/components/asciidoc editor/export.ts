import { $isCodeNode } from "@lexical/code";
import { $isLinkNode } from "@lexical/link";
import { $isListItemNode, $isListNode, ListNode } from "@lexical/list";
import { $isHeadingNode } from "@lexical/rich-text";
import { error } from "@recourse/commons";
import { filter, map, pipe } from "iter-ops";
import {
	$isParagraphNode,
	$isRootNode,
	$isTextNode,
	ElementNode,
	LexicalNode,
} from "lexical";
import { dedent } from "ts-dedent";
import { $isQuizNode } from "./lexical/custom_nodes/quiz";
import {
	$isQuizAnswerNode,
	QuizAnswerNode,
} from "./lexical/custom_nodes/quiz_answer";
import {
	$isQuizQuestionNode,
	QuizQuestionNode,
} from "./lexical/custom_nodes/quiz_question";

type Transform<T = LexicalNode, U = string | undefined> = (
	node: T,
	transforms: Transform[],
) => U;

const escape = (str: string) => {
	const replacements: [RegExp, string][] = [
		[/[*']/g, `\\$&`],

		// symbol replacements
		[/©/g, `(C)`],
		[/®/g, `(R)`],
		[/™/g, `(TM)`],
		[/\u2009—\u2009/g, ` -- `],
		[/→/g, `->`],
		[/⇒/g, `=>`],
		[/←/g, `<-`],
		[/⇐/g, `<=`],
		[/…\u200b?/g, `...`],
		[/\u2019/g, `'`],
	];

	return replacements.reduce(
		(acc, [from, to]) => acc.replaceAll(from, to),
		str,
	);
};

export const $serializeNode = (
	node: LexicalNode,
	transforms: Transform[] = [
		$root,
		$heading,
		$list,
		$listItem,
		$code,
		$quiz,
		$link,
		$paragraph,
		$text,
	],
): string => $serialize(node, transforms);

const $serialize: Transform<LexicalNode, string> = (
	node,
	transforms,
): string => {
	return (
		pipe(
			transforms,
			map((transform) => transform(node, transforms)),
			filter((res) => res !== undefined),
		).first ?? error(`unknown node: ${node.getType()}`)
	);
};

const $serializeChildren: Transform<ElementNode, string> = (node, transforms) =>
	node
		.getChildren()
		.map((child) => $serialize(child, transforms))
		.join(``);

const $root: Transform = (node, transforms) => {
	if (!$isRootNode(node)) return;

	return $serializeChildren(node, transforms).trimEnd();
};

const $text: Transform = (node) => {
	if (!$isTextNode(node)) return;
	let transformed = escape(node.getTextContent());

	if (node.hasFormat(`code`)) transformed = `\`${transformed}\``;
	if (node.hasFormat(`bold`)) transformed = `**${transformed}**`;
	if (node.hasFormat(`italic`)) transformed = `__${transformed}__`;
	if (node.hasFormat(`strikethrough`)) transformed = `??${transformed}??`; // FIXME: unfinished
	if (node.hasFormat(`subscript`)) transformed = `~~${transformed}~~`; // FIXME: untested
	if (node.hasFormat(`superscript`)) transformed = `^^${transformed}^^`; // FIXME: untested
	if (node.hasFormat(`underline`)) transformed = `??${transformed}??`; // FIXME: unfinished

	return transformed;
};

const $link: Transform = (node, transforms) => {
	if (!$isLinkNode(node)) return;

	return `link:${encodeURI(node.getURL())}[${$serializeChildren(
		node,
		transforms,
	)}]`;
};

const $paragraph: Transform = (node, transforms) => {
	if (!$isParagraphNode(node)) return;

	return `${$serializeChildren(node, transforms)}\n\n`;
};

const $heading: Transform = (node, transforms) => {
	if (!$isHeadingNode(node)) return;

	const headingPrefix =
		(
			{
				h1: `==`,
				h2: `===`,
				h3: `====`,
				h4: undefined,
				h5: undefined,
				h6: undefined,
			} as const
		)[node.getTag()] ?? error(`invalid heading level encountered`);

	return `${headingPrefix} ${$serializeChildren(node, transforms)}\n\n`;
};

const $list: Transform = (node, transforms) => {
	if (!$isListNode(node)) return;

	return `${$listItems(node, transforms)}\n\n`;
};

const $listItems = (
	node: ListNode,
	transforms: Transform[],
	depth = 1,
): string => {
	return node
		.getChildren()
		.filter($isListItemNode)
		.map((item) => {
			if (item.getChildrenSize() === 1) {
				const firstChild = item.getFirstChild();
				if ($isListNode(firstChild)) {
					return $listItems(firstChild, transforms, depth + 1);
				}
			}
			return `${`*`.repeat(depth)} ${$serialize(item, transforms)}`;
		})
		.join(`\n`);
};

const $listItem: Transform = (node, transforms) => {
	if (!$isListItemNode(node)) return;

	return $serializeChildren(node, transforms);
};

const $code: Transform = (node) => {
	if (!$isCodeNode(node)) return;

	const language = node.getLanguage();

	return dedent`
		[source${language ? `,${language}` : ``}]
		----
		${node.getTextContent()}
		----\n\n
	`;
};

const $quiz: Transform = (node, transforms) => {
	if (!$isQuizNode(node)) return;

	const questionAnswerPairs = node
		.getChildren()
		.reduce<{ question: QuizQuestionNode; answers: QuizAnswerNode[] }[]>(
			(questionAnswerPairs, child) => {
				if ($isQuizQuestionNode(child)) {
					questionAnswerPairs.push({
						question: child,
						answers: [],
					});
				}
				if ($isQuizAnswerNode(child)) {
					const lastPair =
						questionAnswerPairs.at(-1) ??
						error(`encountered answer with no question for it`);

					lastPair.answers.push(child);
				}

				return questionAnswerPairs;
			},
			[],
		);

	const content = questionAnswerPairs.map(({ question, answers }) => {
		const answersMapped = answers.map((answer) => {
			const label = answer.getLabel();
			return {
				correct: answer.getCorrect(),
				content:
					label === undefined
						? ``
						: $serializeChildren(label, transforms),
			};
		});

		return dedent`
			[question]
			--
			${$serializeChildren(question, transforms).trimEnd()}
			--

			[answers]
			${answersMapped
				.map(
					({ correct, content }) => dedent`
					* [${correct ? `x` : ` `}] ${content}
					`,
				)
				.join(`\n`)}
		`;
	});

	return dedent`
		[quiz]
		====
		${content.join(`\n\n`)}
		====\n\n
	`;
};
