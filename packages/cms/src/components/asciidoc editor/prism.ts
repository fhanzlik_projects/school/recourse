import Prism from "prismjs";

import "prismjs/components/prism-jsx";
import "prismjs/components/prism-shell-session";
import "prismjs/components/prism-tsx";

import { error } from "@recourse/commons";
import Zod from "zod";

const pattern = Zod.instanceof(RegExp).or(
	Zod.object({ pattern: Zod.instanceof(RegExp) }),
);
const patternOrArray = pattern.or(pattern.array());

export const allPrismTokens = Object.values(Prism.languages).flatMap((lang) =>
	Object.entries(lang).flatMap(([name, value]: [string, unknown]) =>
		patternOrArray.safeParse(value).success
			? [name]
			: error(
					`unknown value for name: ${name}, (${String(
						JSON.stringify(value),
					)})`,
			  ),
	),
);
