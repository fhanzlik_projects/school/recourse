import { EditorThemeClasses as EditorThemeClassesOriginal } from "lexical/LexicalEditor";

import { QuizNodeThemeClasses } from "../../components/asciidoc editor/lexical/custom_nodes/quiz";
import { QuizAnswerNodeThemeClasses } from "../../components/asciidoc editor/lexical/custom_nodes/quiz_answer";
import { QuizAnswerCorrectNodeThemeClasses } from "../../components/asciidoc editor/lexical/custom_nodes/quiz_answer_correct";
import { QuizQuestionNodeThemeClasses } from "../../components/asciidoc editor/lexical/custom_nodes/quiz_question";

declare module "lexical" {
	export interface EditorThemeClasses
		extends EditorThemeClassesOriginal,
			QuizNodeThemeClasses,
			QuizQuestionNodeThemeClasses,
			QuizAnswerNodeThemeClasses,
			QuizAnswerCorrectNodeThemeClasses {}
}
