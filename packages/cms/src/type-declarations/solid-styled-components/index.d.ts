// import original module declarations
import { Theme } from "@recourse/commons";
import "solid-styled-components";

// and extend them!
declare module "solid-styled-components" {
	export interface DefaultTheme extends Theme {}
}
