module.exports = {
	plugins: ["@typescript-eslint", "solid", "jsx-a11y"],
	parserOptions: {
		tsconfigRootDir: __dirname,
		project: [
			"./tsconfig.json",
			"../../tsconfig.settings.json",
			"../../tsconfig.base.json",
		],
	},
	extends: [
		"eslint:recommended",
		"plugin:solid/typescript",
		"plugin:jsx-a11y/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"plugin:@typescript-eslint/strict",
		"prettier",
	],
	rules: {
		"@typescript-eslint/quotes": [
			"warn",
			"backtick",
			{ avoidEscape: true },
		],
		"@typescript-eslint/no-unused-vars": [
			"warn",
			{ argsIgnorePattern: "^_" },
		],
		"@typescript-eslint/consistent-type-definitions": ["warn", "type"],
	},

	overrides: [
		{
			files: ["*.d.ts"],
			rules: {
				// in `.d.ts` files we usually want declaration merging
				"@typescript-eslint/no-empty-interface": ["off"],
				"@typescript-eslint/consistent-type-definitions": ["off"],
			},
		},
	],
};
