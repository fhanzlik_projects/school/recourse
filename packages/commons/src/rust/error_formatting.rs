use {
	super::styled_writer::WriteExt,
	anyhow::Chain,
	owo_colors::{OwoColorize, Style},
	std::{
		fmt,
		fmt::{Display, Write},
	},
	tracing_error::ExtractSpanTrace,
};

pub struct FormattedError<T> {
	pub error: T,
}

impl<T> FormattedError<T> {
	pub const fn new(error: T) -> Self {
		Self { error }
	}
}

impl<T: std::error::Error + 'static> Display for FormattedError<T> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		// beyond ugly macro to simplify iterating over spans in a span trace
		macro try_bool($dest:ident = $e:expr) {{
			match $e {
				Ok(val) => val,
				Err(err) => {
					$dest = Err(err);
					return false;
				},
			}
		}}

		write!(f, "{}", self.error)?;

		let error_chain = Chain::new(&self.error);

		{
			let mut error_chain = error_chain.clone();

			if let Some(cause) = error_chain.next() {
				write!(f, "\n\n{}", "Caused by:".red())?;
				let multiple = cause.source().is_some();

				// throw out span-trace errors as the deepest span trace is logged above
				// separately and if there are shallower ones, they are probably not
				// interesting.
				for (n, error) in error_chain.filter(|e| e.span_trace().is_none()).enumerate() {
					writeln!(f)?;
					let mut indented = Indented {
						inner: f,
						number: if multiple { Some(n) } else { None },
						started: false,
					};
					write!(indented, "{error}")?;
				}
			}
		}

		// find the deepest span trace
		// (we are assuming that only the deepest span trace is of any value)
		if let Some(span_trace) = error_chain.rev().find_map(ExtractSpanTrace::span_trace) {
			writeln!(f, "\n\n{}", "Span trace:".red())?;

			let mut span_index = 0;
			let mut error = Ok(());

			span_trace.with_spans(|metadata, fields| {
				try_bool! { error = writeln!(f)};

				let mut indented = Indented {
					inner: f,
					number: Some(span_index),
					started: false,
				};

				try_bool! {
					error = write!(indented, "{}", format_args!("{}::{}", metadata.target(), metadata.name()).bold())
				};

				if !fields.is_empty() {
					try_bool! {
						error = write!(indented, "{}{}{}", "{".bold(), fields, "}".bold())
					};
				}

				if let Some((file, line)) = metadata
					.file()
					.and_then(|file| metadata.line().map(|line| (file, line)))
				{
					try_bool! {
						error = write!(indented, " {} {}", "at".dimmed(), format_args!("{file}:{line}").bold())
					};
				}

				span_index += 1;

				true
			});

			error?;
		}

		writeln!(f)?;

		Ok(())
	}
}

pub struct Indented<'a, D> {
	pub inner: &'a mut D,
	pub number: Option<usize>,
	pub started: bool,
}

impl<T> Write for Indented<'_, T>
where
	T: Write,
{
	fn write_str(&mut self, s: &str) -> fmt::Result {
		for (i, line) in s.split('\n').enumerate() {
			if !self.started {
				self.started = true;
				match self.number {
					Some(number) => {
						write!(self.styled(Style::new().dimmed())?, "{number: >5}: ")?;
					},
					None => self.inner.write_str("    ")?,
				}
			} else if i > 0 {
				self.inner.write_char('\n')?;

				self.inner.write_str(if self.number.is_some() {
					"        "
				} else {
					"    "
				})?;
			}

			self.inner.write_str(line)?;
		}

		Ok(())
	}
}
