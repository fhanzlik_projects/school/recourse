use {
	owo_colors::Style,
	std::{fmt, fmt::Display},
};

pub struct StylePrefix<'a>(pub &'a Style);

impl Display for StylePrefix<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		self.0.fmt_prefix(f)
	}
}

pub struct StyleSuffix<'a>(pub &'a Style);

impl Display for StyleSuffix<'_> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		self.0.fmt_suffix(f)
	}
}
