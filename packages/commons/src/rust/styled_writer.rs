use {
	crate::owo_colours_utils::{StylePrefix, StyleSuffix},
	owo_colors::Style,
	std::{fmt, fmt::Write},
};

pub struct StyledWrite<'a, T: Write> {
	style: Style,
	inner: &'a mut T,
}

impl<'a, T: Write> StyledWrite<'a, T> {
	fn new(inner: &'a mut T, style: Style) -> Result<Self, std::fmt::Error> {
		write!(inner, "{}", StylePrefix(&style))?;
		Ok(Self { style, inner })
	}
}

impl<T: Write> Drop for StyledWrite<'_, T> {
	fn drop(&mut self) {
		write!(self.inner, "{}", StyleSuffix(&self.style)).unwrap();
	}
}

impl<T: Write> Write for StyledWrite<'_, T> {
	fn write_str(&mut self, s: &str) -> std::fmt::Result {
		self.inner.write_str(s)
	}
}

pub trait WriteExt<T: Write> {
	fn styled(&mut self, style: Style) -> Result<StyledWrite<'_, T>, fmt::Error>;
}

impl<T> WriteExt<T> for T
where
	T: Write,
{
	fn styled(&mut self, style: Style) -> Result<StyledWrite<'_, T>, fmt::Error> {
		StyledWrite::new(self, style)
	}
}
