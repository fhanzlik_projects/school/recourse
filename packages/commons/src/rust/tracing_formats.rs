use {
	crate::{
		owo_colours_utils::{StylePrefix, StyleSuffix},
		styled_writer::WriteExt,
	},
	itertools::Itertools,
	owo_colors::{OwoColorize, Style},
	std::{fmt, fmt::Write},
	tracing::Level,
	tracing_core::{Event, Subscriber},
	tracing_subscriber::{
		fmt::{
			format::{self, FormatEvent, FormatFields},
			time::{FormatTime, SystemTime},
			FmtContext, FormattedFields,
		},
		registry::LookupSpan,
	},
};

pub struct Normal<Time> {
	timer: Time,
	with_filenames: bool,
}

#[derive(Default)]
pub struct Web {
	with_filenames: bool,
}

impl Default for Normal<SystemTime> {
	fn default() -> Self {
		Self {
			timer: SystemTime,
			with_filenames: false,
		}
	}
}

impl<TSub, TFields, TTime> FormatEvent<TSub, TFields> for Normal<TTime>
where
	TSub: Subscriber + for<'a> LookupSpan<'a>,
	TFields: for<'a> FormatFields<'a> + 'static,
	TTime: FormatTime,
{
	fn format_event(
		&self,
		ctx: &FmtContext<'_, TSub, TFields>,
		mut writer: format::Writer<'_>,
		event: &Event<'_>,
	) -> fmt::Result {
		let metadata = event.metadata();

		{
			let style = Style::new().dimmed();
			write!(writer, "{}", StylePrefix(&style))?;

			// If getting the timestamp failed, don't bail --- only bail on
			// formatting errors.
			if self.timer.format_time(&mut writer).is_err() {
				writer.write_str("<unknown time>")?;
			};

			write!(writer, "{} ", StyleSuffix(&style))?;
		}

		write!(writer, "{} ", LevelColoured::new(metadata.level()))?;

		write!(writer, "{}", metadata.target().dimmed())?;

		if let Some(scope) = ctx.event_scope() {
			for position in scope.from_root().with_position().peekable() {
				use itertools::Position::{First, Last, Middle, Only};
				if let First(_) | Only(_) = position {
					write!(writer, "{}", " | ".dimmed())?;
				}

				if let Middle(_) | Last(_) = position {
					// separate spans by arrows
					write!(writer, "{}", "->".dimmed())?;
				}
				let span = position.into_inner();

				write!(writer, "{}", span.metadata().name().bold())?;

				let ext = span.extensions();
				if let Some(fields) = &ext.get::<FormattedFields<TFields>>() {
					if !fields.is_empty() {
						write!(writer, "{}{}{}", "{".bold(), fields, "}".bold())?;
					}
				}
			}
		};

		write!(writer, "{}", " > ".dimmed())?;

		if self.with_filenames {
			if let Some(filename) = metadata.file() {
				write!(
					writer.styled(Style::new().dimmed())?,
					"{}:{}",
					filename,
					metadata
						.line()
						.map_or_else(|| "?".to_string(), |l| l.to_string())
				)?;
			}
		}

		// Write fields on the event
		ctx.field_format().format_fields(writer.by_ref(), event)?;

		writeln!(writer)
	}
}

impl<TSub, TFields> FormatEvent<TSub, TFields> for Web
where
	TSub: Subscriber + for<'a> LookupSpan<'a>,
	TFields: for<'a> FormatFields<'a> + 'static,
{
	fn format_event(
		&self,
		ctx: &FmtContext<'_, TSub, TFields>,
		mut writer: format::Writer<'_>,
		event: &Event<'_>,
	) -> fmt::Result {
		let metadata = event.metadata();

		write!(writer, "{} ", LevelPlain::new(metadata.level()))?;

		write!(writer, "{}", metadata.target())?;

		if let Some(scope) = ctx.event_scope() {
			for position in scope.from_root().with_position().peekable() {
				use itertools::Position::{First, Last, Middle, Only};
				if let First(_) | Only(_) = position {
					write!(writer, " | ")?;
				}

				if let Middle(_) | Last(_) = position {
					// separate spans by arrows
					write!(writer, "->")?;
				}
				let span = position.into_inner();

				write!(writer, "{}", span.metadata().name())?;

				let ext = span.extensions();
				if let Some(fields) = &ext.get::<FormattedFields<TFields>>() {
					if !fields.is_empty() {
						write!(writer, "{{{fields}}}")?;
					}
				}
			}
		};

		writeln!(writer, " >")?;

		if self.with_filenames {
			if let Some(filename) = metadata.file() {
				write!(
					writer,
					"{}:{}",
					filename,
					metadata
						.line()
						.map_or_else(|| "?".to_string(), |l| l.to_string())
				)?;
			}
		}

		// Write fields on the event
		ctx.field_format().format_fields(writer.by_ref(), event)?;

		writeln!(writer)
	}
}

struct LevelPlain<'a> {
	level: &'a Level,
}

impl<'a> LevelPlain<'a> {
	const fn new(level: &'a Level) -> Self {
		Self { level }
	}
}

impl<'a> fmt::Display for LevelPlain<'a> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match *self.level {
			Level::TRACE => write!(f, "TRACE"),
			Level::DEBUG => write!(f, "DEBUG"),
			Level::INFO => write!(f, " INFO"),
			Level::WARN => write!(f, " WARN"),
			Level::ERROR => write!(f, "ERROR"),
		}
	}
}

struct LevelColoured<'a> {
	plain: LevelPlain<'a>,
}

impl<'a> LevelColoured<'a> {
	const fn new(level: &'a Level) -> Self {
		Self {
			plain: LevelPlain::new(level),
		}
	}
}

impl<'a> fmt::Display for LevelColoured<'a> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let mut f = match *self.plain.level {
			Level::TRACE => f.styled(Style::new().purple()),
			Level::DEBUG => f.styled(Style::new().blue()),
			Level::INFO => f.styled(Style::new().green()),
			Level::WARN => f.styled(Style::new().yellow()),
			Level::ERROR => f.styled(Style::new().red()),
		}?;

		write!(f, "{}", self.plain)
	}
}
