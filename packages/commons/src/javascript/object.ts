export const getProperty = <
	K extends PropertyKey,
	T extends Record<K, unknown>,
>(
	obj: T,
	key: K,
) =>
	obj[key] as
		| T[Extract<keyof T, K>]
		| (K extends keyof T ? never : undefined);
