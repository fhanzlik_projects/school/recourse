import { mix } from "polished";
import { assert, error } from "../../debugging";
import tokens from "./material-theme/data/tokens.json";

const elevation = {
	1: 0.05,
	2: 0.08,
	3: 0.11,
	4: 0.12,
	5: 0.14,
};

export const elevate = (
	colour: string,
	tint: string,
	level: keyof typeof elevation,
) => mix(elevation[level], tint, colour);

const mkColors = (theme: string) => {
	const colorNonThemed = (id: string) => {
		const { type, value } =
			tokens.entities.find((e) => e.id === id) ??
			error(`failed to find entity (${id})`);
		assert(type === "color", `only color entities are supported (${id})`);
		assert(value.startsWith("#"), `only hex colors are supported (${id})`);
		return value;
	};
	const color = (id: string) => colorNonThemed(`${id}.${theme}`);

	return {
		transparent: "#00000000",

		background: color("md.sys.color.background"),
		error: color("md.sys.color.error"),
		onBackground: color("md.sys.color.on-background"),
		onError: color("md.sys.color.on-error"),
		onPrimary: color("md.sys.color.on-primary"),
		onSecondaryContainer: color("md.sys.color.on-secondary-container"),
		onSurface: color("md.sys.color.on-surface"),
		onSurfaceVariant: color("md.sys.color.on-surface-variant"),
		outlineVariant: color("md.sys.color.outline-variant"),
		primary: color("md.sys.color.primary"),
		secondaryContainer: color("md.sys.color.secondary-container"),
		surface: color("md.sys.color.surface"),
		surfaceTint: color("md.sys.color.surface-tint"),
		surfaceVariant: color("md.sys.color.surface-variant"),
		surfaceContainerLow: colorNonThemed("md.ref.palette.neutral10"),
		inverseSurface: color("md.sys.color.inverse-surface"),
		inverseOnSurface: color("md.sys.color.inverse-on-surface"),
	};
};

export const colours = {
	light: mkColors("light"),
	dark: mkColors("dark"),
};

export const stateLayerOpacities = {
	hover: 0.08,
	focus: 0.12,
	pressed: 0.12,
	dragged: 0.16,
};
