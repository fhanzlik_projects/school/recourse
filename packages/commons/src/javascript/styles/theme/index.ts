import _ from "lodash";
import { darken, lighten, mix } from "polished";
import { error } from "../../debugging";
import { LeavesOfType } from "../../types";
import { css } from "../mics";
import { colours, elevate, stateLayerOpacities } from "./material";

export type Theme = typeof dark;

const gray0 = `hsl(0, 0%, 10%)`;
const gray1 = `hsl(0, 0%, 20%)`;
const gray2 = `hsl(0, 0%, 30%)`;
const gray3 = `hsl(0, 0%, 40%)`;
const gray4 = `hsl(0, 0%, 50%)`;
const gray5 = `hsl(0, 0%, 60%)`;
const gray6 = `hsl(0, 0%, 70%)`;
const gray7 = `hsl(0, 0%, 80%)`;
const gray8 = `hsl(0, 0%, 90%)`;
const green1 = `hsl(102, 82%, 45%)`;

const blue1 = `#1d90f5`;
const blue0 = darken(0.05, blue1);
const blue2 = lighten(0.05, blue1);
const green0 = `#83ff0f`;
const red0 = `#ff1553`;

const dark = {
	colours: {
		...colours.dark,
		primary: blue1,
		onPrimary: colours.dark.onBackground,
		"surfaceContainerLow.elevated.1": elevate(
			colours.dark.surfaceContainerLow,
			colours.dark.surfaceTint,
			1,
		),
		"inverseSurface.elevated.3": elevate(
			colours.dark.inverseSurface,
			colours.dark.surfaceTint,
			3,
		),

		background: {
			normal: colours.dark.background,
			elevated: {
				0: elevate(colours.dark.surface, colours.dark.surfaceTint, 1),
				1: elevate(colours.dark.surface, colours.dark.surfaceTint, 2),
				2: elevate(colours.dark.surface, colours.dark.surfaceTint, 3),
			},

			primary: blue1,
			secondary: green1,

			highlight: {
				elevated: { 0: gray3, 1: gray4 },
				primary: blue2,
			},
			dim: {
				primary: blue0,
			},
		},
		foreground: {
			get onPrimary() {
				return dark.colours.onPrimary;
			},

			normal: colours.dark.onBackground,
			bright: gray8,
			dim: gray6,
			error: red0,
			success: green0,
			primary: blue1,
			secondary: green1,
			footer: gray0,
		},
		outline: {
			error: red0,
			focus: blue2,
		},
		shadow: {
			normal: "hsla(0, 0%, 0%, 0.7)",
		},
	},
	stateLayerOpacities,
};

export const themes = { dark: dark };

export type ColourId = LeavesOfType<Theme["colours"], string>;
export const colour =
	(id: ColourId) =>
	({ theme = error.unexpectedNullish() }: { theme?: Theme | undefined }) =>
		_.get(theme.colours, id) ?? error.unexpectedNullish();

export type StateLayerOpacityId = keyof Theme["stateLayerOpacities"];
export const stateLayer =
	(base: ColourId, layer: ColourId, opacity: StateLayerOpacityId) =>
	({ theme = error.unexpectedNullish() }: { theme?: Theme | undefined }) =>
		mix(
			theme.stateLayerOpacities[opacity],
			colour(layer)({ theme }),
			colour(base)({ theme }),
		);

export type ForegroundColourId = LeavesOfType<
	Theme["colours"]["foreground"],
	string
>;
export const foreground =
	(color: ForegroundColourId) =>
	({ theme = error.unexpectedNullish() }: { theme?: Theme | undefined }) =>
		css`
			color: ${_.get(theme.colours.foreground, color) ??
			error.unexpectedNullish()};
		`;

export type BackgroundColourId = LeavesOfType<
	Theme["colours"]["background"],
	string
>;
export const background =
	(color: BackgroundColourId) =>
	({ theme = error.unexpectedNullish() }: { theme?: Theme | undefined }) =>
		css`
			background-color: ${(_.get(theme.colours.background, color) as
				| string
				| undefined) ?? error.unexpectedNullish()};
		`;

export type OutlineColourId = LeavesOfType<Theme["colours"]["outline"], string>;
export const outlineColour =
	(color: OutlineColourId) =>
	({ theme = error.unexpectedNullish() }: { theme?: Theme | undefined }) =>
		css`
			outline-color: ${_.get(theme.colours.outline, color) ??
			error.unexpectedNullish()};
		`;

export const focusOutline =
	({
		focusMode = "visible",
	}: {
		focusMode?: "within" | "visible" | undefined;
	} = {}) =>
	({ theme = error.unexpectedNullish() }: { theme?: Theme | undefined }) =>
		css`
			outline: 0.15rem solid transparent;
			&:focus-${focusMode} {
				${outlineColour(`focus`)({ theme })}
			}
		`;
