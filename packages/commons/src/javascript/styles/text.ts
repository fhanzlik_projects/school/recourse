import { css } from "./mics";
import { foreground, ForegroundColourId, Theme } from "./theme";

const families = {
	code: {
		name: `Menlo, Consolas, Monaco, monospace`,
		weights: { regular: 400 },
	},
	montserrat: {
		name: `'Montserrat', sans-serif`,
		weights: { regular: 400, bold: 700 },
	},
	roboto: {
		name: `'Roboto', sans-serif`,
		weights: { regular: 400, bold: 700 },
	},
	quicksand: {
		name: `'Quicksand', sans-serif`,
		weights: { regular: 400 },
	},
};
export type FontFamilyName = keyof typeof families;
export type FontWeight<Family extends FontFamilyName> =
	keyof (typeof families)[Family]["weights"];

export const text =
	<F extends FontFamilyName = "roboto">({
		colour = "normal",
		family = "roboto",
		size,
		weight = "regular",
	}: {
		colour?: ForegroundColourId;
		family?: F | FontFamilyName;
		size?: number;
		weight?: FontWeight<F>;
	} = {}) =>
	({ theme }: { theme?: Theme }) =>
		css`
			text-decoration: none;
			${foreground(colour)({ theme })}
			font-family: ${families[family].name};
			${size === undefined ? `` : `font-size: ${size}rem;`}
			/* the assumption is needed because typescript is unable to track the relationship */
			/* between \`family\` and \`weight\` */
			font-weight: ${(families[family].weights as Record<FontWeight<F>, number>)[
				weight
			].toString()};
		`;
