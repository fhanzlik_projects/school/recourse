import { css } from "./mics";
import { text } from "./text";
import { background, Theme } from "./theme";

export const button =
	({ colour }: { colour: "primary" }) =>
	({ theme }: { theme?: Theme }) =>
		css`
			padding: 0.25rem 2rem;
			${text({ family: `montserrat`, weight: `bold` })({ theme })}
			${background(colour)({ theme })}
			border-radius: 1rem;

			transition: 0.25s background ease-in-out;
			&:hover {
				${background(`highlight.${colour}`)({ theme })}
			}
			&:active {
				${background(`dim.${colour}`)({ theme })}
			}
		`;
