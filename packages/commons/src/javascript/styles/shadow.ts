import { error } from "../debugging";
import { css } from "./mics";
import { Theme } from "./theme";

const bases = {
	normal: ({ theme = error.unexpectedNullish() }: { theme?: Theme }) => css`
		box-shadow: 0 0 0.5rem ${theme.colours.shadow.normal};
	`,
};

export const shadow = (base: keyof typeof bases) => bases[base];
