import { css } from "./mics";
import { Theme } from "./theme";

export const globalStyles = ({ theme }: { theme: Theme }) => css`
	*,
	*::before,
	*::after {
		margin: 0;
		padding: 0;
		border: none;
		font: inherit;
		box-sizing: border-box;
		color: inherit;

		overflow-wrap: break-word;
		hyphens: auto;
	}

	body,
	html {
		display: flex;
		flex-direction: column;
	}

	html {
		overflow-x: hidden;
	}

	#root {
		flex-basis: 100vh;
	}

	body {
		font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto",
			"Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans",
			"Helvetica Neue", sans-serif;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;

		min-width: 100vw;
		min-height: 100vh;

		/* this should hopefully alert everyone to missing text styles */
		color: magenta;
	}

	body {
		line-height: 1.5;
	}
	h1,
	h2,
	h3,
	h4,
	h5,
	h6 {
		line-height: 1.1;
	}
`;
