import { EmptyObj } from "../types";
import { Theme } from "./theme";

export type StyleFn<P = EmptyObj> = (
	props: { theme?: Theme | undefined } & P,
) => string;

export type Style<T = EmptyObj> = string | StyleFn<T>;

/**
 * hack to work around vscode-styled-components' inability to highlight and format arbitrary strings
 *
 * more details at: https://github.com/microsoft/typescript-styled-plugin/issues/161
 */
// TODO: consider expanding this so that it can make working with shared styles which use themes easier
export const css: (
	template: TemplateStringsArray,
	...params: string[]
) => string = String.raw;
