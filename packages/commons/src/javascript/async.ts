export const runBackgroundAsyncTask = (cb: () => Promise<void>) => {
	logAsyncFailure(cb());
};

export const logAsyncFailure = (promise: Promise<void>) => {
	promise.catch(console.error);
};
