export const dbg = <T>(
	value: T,
	formatter: (value: T) => unknown[] = (value) => [value],
) => {
	console.log(...formatter(value));
	return value;
};

/**
 * throws an instance of {@link Error}
 *
 * this function serves as a helper to enable throwing in expression contexts.
 *
 * @example
 *
 * const notNull = mayBeNull ?? error("must not be null!");
 *
 * const aString = typeof anything === "string" ? anything : error("must be a string!");
 *
 * // NOTE:
 * // as of now, typescript ignores calls to never-returning functions such as this one when doing
 * // control-flow analysis, so if you need the call to be used for e.g. type narrowing, just return
 * // the result.
 *
 * if (typeof numberOrString === "number") {
 * 	return error("no number!");
 * }
 *
 * // numberOrString is correctly typed as string here
 */
export const error = (message: string) => {
	throw new Error(message);
};

// known errors
error.unexpectedNullish = () => error(`unexpected nullish value encountered`);

export const assert: (
	condition: boolean,
	message: string,
) => asserts condition = (condition, message) => {
	if (!condition) throw new Error(`AssertionFailed: ${message}`);
};

/**
 * variant of the {@link error} function which also logs the error into console
 *
 * this function is intended for places where you can't (or don't want to) do proper error handling.
 *
 * for example when you are in a synchronous context but want to react to asynchronous rejection:
 * @example
 *
 * asyncFunctionThatICantAwait().catch(errorLogging)
 */
export const errorLogging = (message: string) => {
	console.error(`Error: `, message);
	return error(message);
};

export const logPromiseErrors =
	<T extends unknown[]>(asyncFn: (...args: T) => Promise<unknown>) =>
	(...args: T) =>
		asyncFn(...args).catch(errorLogging);
