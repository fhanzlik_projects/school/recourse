/**
 * check that a value is of certain type at compile-time
 *
 * this is workaround for missing type ascriptions in TypeScript, tracked at: https://github.com/microsoft/TypeScript/issues/7481
 *
 * the main use-case is to be able to check a type of a value without assigning it to an variable.
 * this is mostly useful when working with objects, e.g.:
 * @example
 * const a = {
 * 	b: is<SomeType>({
 * 		x: 0
 * 	})
 * }
 *
 * // !!! WARNING !!!
 * // since TypeScript's functions are bi-variant over their parameters, this function can be potentially unsafe. e.g.:
 *
 * class A {}
 * class B extends A {
 * 	fn() {}
 * }
 *
 * // this compiles without warnings even with `{strict: true}`, but will explode at runtime.
 * const x = is<(a: A) => void>(
 * 	(b: B) => {
 * 		b.fn();
 * 	}
 * );
 * x(new A());
 */
export const is = <T>(value: T) => value;

/**
 * type to be used in place of `{}`, as `{}` means "any non-nullish value", whereas this type is actually object with no properties
 */
export type EmptyObj = Record<string, never>;

type Join<K, P> = K extends string | number
	? P extends string | number
		? `${K}${"" extends P ? "" : "."}${P}`
		: never
	: never;

type Prev = [
	never,
	0,
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	...0[],
];

export type LeavesOfType<TVal, TValType, D extends number = 10> = [D] extends [
	never,
]
	? never
	: TVal extends TValType
	? ""
	: // eslint-disable-next-line @typescript-eslint/no-explicit-any
	TVal extends Record<keyof any, any>
	? {
			[K in keyof TVal]-?: Join<
				K,
				LeavesOfType<TVal[K], TValType, Prev[D]>
			>;
	  }[keyof TVal]
	: never;

export type Tuple<
	T,
	N extends number,
	R extends readonly T[] = [],
> = R["length"] extends N ? R : Tuple<T, N, readonly [T, ...R]>;

export type Brand<T extends keyof any> = Record<T, true>;
