class FetchError extends Error {
	public override readonly name = "FetchError";

	public statusCode: number;
	public body: string;

	constructor(statusCode: number, statusText: string, body: string) {
		super(`Response ${statusCode}: ${statusText}`);
		this.name;
		this.statusCode = statusCode;
		this.body = body;
	}
}

/**
 * wrapper around {@link fetch} that throws if the response is not OK
 */
export const fetchStrict = async (...params: Parameters<typeof fetch>) => {
	const response = await fetch(...params);

	if (!response.ok)
		throw new FetchError(
			response.status,
			response.statusText,
			await response.text(),
		);

	return response;
};
