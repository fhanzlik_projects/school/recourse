import { parse as parseUuid, stringify, v4 as genUuidV4 } from "uuid";
import { error } from "./debugging";
import { Brand } from "./types";

declare const brand_uuid: unique symbol;

export type Uuid = Uint8Array & Brand<typeof brand_uuid>;
export const uuid = (value: Uint8Array) => {
	if (value.length !== 16) error("invalid uuid byte count");
	return value as Uuid;
};
uuid.parse = (value: string) => uuid(parseUuid(value) as Uint8Array);
uuid.debugStringify = (id: Uuid) => stringify(id);
uuid.random = () => {
	const buffer = new Uint8Array(16);
	genUuidV4(undefined, buffer);
	return uuid(buffer);
};

uuid.toKey = (id: Uuid) => new DataView(id.buffer).getUint32(0);
