export const getScrollParent = (element: HTMLElement) => {
	let style = getComputedStyle(element);
	let excludeStaticParent = style.position === "absolute";
	let overflowRegex = /(auto|scroll)/;

	if (style.position === "fixed") return document.body;

	let parent: HTMLElement | null = element;
	while ((parent = parent.parentElement) !== null) {
		style = getComputedStyle(parent);
		if (excludeStaticParent && style.position === "static") {
			continue;
		}
		if (
			overflowRegex.test(
				style.overflow + style.overflowY + style.overflowX,
			)
		)
			return parent;
	}

	return document.body;
};
