#!/bin/bash

set -eu

function create_user_and_database() {
	local database="$1";

	echo "Creating user and database '$database'"

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
		CREATE USER "$database" PASSWORD 'development';
		CREATE DATABASE "$database";
		GRANT ALL PRIVILEGES ON DATABASE "$database" TO "$database";
		EOSQL
}

create_user_and_database 'ory_kratos'
